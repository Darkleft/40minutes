function solonum(event) {
    var val = true
    var letras = " ÁÉÍÓÚÜABCDEFGHIJKLMNÑOPQRSTUVWXYZ\áéíóúüabcdefghijklmnñopqrstuvwxyz+-*/°!,#$%&()=?¡¿'|¬@~`^_:;[]{}¨.";
    var key = event.keyCode || event.which;
    var tecla = String.fromCharCode(key).toLowerCase();
    if (letras.indexOf(tecla) !== -1 || event.which === 34 || event.which === 13) {
        val = false;
    }
    return val;
}

function btndiv(value) {
    $(".btn_div").removeClass("active");
    $(".btn_" + value).addClass("active");
    $(".div_tb").addClass("hide");
    $("." + value).removeClass("hide");
    $.ajax({
        url: "../../site/globals",
        type: 'POST',
        data: {
            ajaxbtndiv: true,
            value: value,
        },
    })
}

function btndivclases(value) {
    $(".btn_div_clases").removeClass("active");
    $(".btn_" + value).addClass("active");
    $(".div_clases").addClass("hide");
    $("." + value).removeClass("hide");
    $.ajax({
        url: "../../site/globals",
        type: 'POST',
        data: {
            ajaxbtnclases: true,
            value: value,
        },
    })
}
$(".btn_div").on('click', function () {
    btndiv(this.value)
});
$(".btn_div_clases").on('click', function () {
    btndivclases(this.value)
});
$("table").removeClass('kv-table-wrap');