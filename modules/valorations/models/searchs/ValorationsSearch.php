<?php

namespace app\modules\valorations\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\valorations\models\Valorations;

/**
 * ValorationsSearch represents the model behind the search form of `app\modules\valorations\models\Valorations`.
 */
class ValorationsSearch extends Valorations {

    public $customer_name;
    public $coach_name;
    public $class_date;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['valoration_id', 'valoration_user', 'valoration_coach', 'valoration_class', 'valoration_metabolism', 'valoration_cervical', 'valoration_age'], 'integer'],
            [['valoration_weight', 'valoration_imc', 'valoration_grease', 'valoration_muscle'], 'number'],
            [['valoration_hability', 'customer_name', 'coach_name', 'class_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Valorations::find()->select('*');
        $query->joinWith('valorationUsuarios');
//        $query->join('join', 'customers', 'customer_user=users.user_id');
//        $query->join('join', 'refereers', 'refereer_user=users.user_id');
        $query->joinWith('valorationCoach');
        $query->joinWith('valorationClass');
        $query->orderBy("valoration_id desc");
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'valoration_id' => $this->valoration_id,
            'valoration_user' => $this->valoration_user,
            'valoration_coach' => $this->valoration_coach,
            'valoration_class' => $this->valoration_class,
            'valoration_weight' => $this->valoration_weight,
            'valoration_imc' => $this->valoration_imc,
            'valoration_grease' => $this->valoration_grease,
            'valoration_muscle' => $this->valoration_muscle,
            'valoration_metabolism' => $this->valoration_metabolism,
            'valoration_cervical' => $this->valoration_cervical,
            'valoration_age' => $this->valoration_age,
        ]);

        $query->andFilterWhere(['like', 'valoration_hability', $this->valoration_hability])
                ->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'coach_name', $this->coach_name])
                ->andFilterWhere(['like', 'coach_name', $this->class_date]);

        return $dataProvider;
    }

    public function searchCustomers($params, $id_customer) {
        $query = Valorations::find()->where("valoration_user = $id_customer")->orderBy("valoration_id desc");
        $query->joinWith('valorationUsuarios');
        //$query->joinWith('customers');
        $query->joinWith('valorationCoach');
        $query->joinWith('valorationClass');
        $query->orderBy("valoration_id desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'valoration_id' => $this->valoration_id,
            'valoration_user' => $this->valoration_user,
            'valoration_coach' => $this->valoration_coach,
            'valoration_class' => $this->valoration_class,
            'valoration_weight' => $this->valoration_weight,
            'valoration_imc' => $this->valoration_imc,
            'valoration_grease' => $this->valoration_grease,
            'valoration_muscle' => $this->valoration_muscle,
            'valoration_metabolism' => $this->valoration_metabolism,
            'valoration_cervical' => $this->valoration_cervical,
            'valoration_age' => $this->valoration_age,
        ]);

        $query->andFilterWhere(['like', 'valoration_hability', $this->valoration_hability])
                ->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'coach_name', $this->coach_name])
                ->andFilterWhere(['like', 'coach_name', $this->class_date]);

        return $dataProvider;
    }

    public function searchCoach($params, $id_coach) {
        $query = Valorations::find()->where("valoration_coach = $id_coach")->orderBy("valoration_id desc");
        $query->joinWith('valorationUsuarios');
        //$query->joinWith('customers');
        $query->joinWith('valorationCoach');
        $query->joinWith('valorationClass');
        $query->orderBy("valoration_id desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'valoration_id' => $this->valoration_id,
            'valoration_user' => $this->valoration_user,
            'valoration_coach' => $this->valoration_coach,
            'valoration_class' => $this->valoration_class,
            'valoration_weight' => $this->valoration_weight,
            'valoration_imc' => $this->valoration_imc,
            'valoration_grease' => $this->valoration_grease,
            'valoration_muscle' => $this->valoration_muscle,
            'valoration_metabolism' => $this->valoration_metabolism,
            'valoration_cervical' => $this->valoration_cervical,
            'valoration_age' => $this->valoration_age,
        ]);

        $query->andFilterWhere(['like', 'valoration_hability', $this->valoration_hability])
                ->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'coach_name', $this->coach_name])
                ->andFilterWhere(['like', 'coach_name', $this->class_date]);

        return $dataProvider;
    }

}
