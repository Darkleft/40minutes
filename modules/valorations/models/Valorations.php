<?php

namespace app\modules\valorations\models;

use Yii;
use app\modules\clases\models\Clases;
use app\modules\coachs\models\Coachs;
use app\modules\customers\models\Customers;
use app\modules\admin\models\Usuarios;
use app\modules\groups\models\Refereers;

/**
 * This is the model class for table "valorations".
 *
 * @property int $valoration_id
 * @property int $valoration_user
 * @property int $valoration_coach
 * @property int $valoration_class
 * @property int $valoration_plan
 * @property string $valoration_weight
 * @property string $valoration_imc
 * @property string $valoration_grease
 * @property string $valoration_muscle
 * @property int $valoration_metabolism
 * @property int $valoration_cervical
 * @property int $valoration_age
 * @property string $valoration_hability
 *
 * @property Coachs $valorationCoach
 * @property Customers $valorationUsuarios
 * @property Class $valorationClass
 */
class Valorations extends \yii\db\ActiveRecord {

    public $customer_name;
    public $coach_name;
    public $class_date;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'valorations';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_name', 'coach_name', 'class_date'], 'safe'],
            [['valoration_user', 'valoration_class'], 'required'],
            [['valoration_user', 'valoration_coach', 'valoration_class',  'valoration_age', 'valoration_plan'], 'integer'],
            [['valoration_weight', 'valoration_imc', 'valoration_grease', 'valoration_metabolism', 'valoration_cervical','valoration_muscle'], 'number'],
            [['valoration_hability'], 'string', 'max' => 1],
            [['valoration_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coachs::className(), 'targetAttribute' => ['valoration_coach' => 'coach_id']],
            [['valoration_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['valoration_user' => 'user_id']],
            [['valoration_class'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['valoration_class' => 'class_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'valoration_id' => 'ID',
            'valoration_user' => 'Cliente',
            'valoration_coach' => 'Entrenador',
            'valoration_class' => 'Clase',
            'valoration_weight' => 'Peso',
            'valoration_imc' => 'IMC',
            'valoration_grease' => 'Grasa',
            'valoration_muscle' => 'Musculo',
            'valoration_metabolism' => 'Metabolismo',
            'valoration_cervical' => 'Grasa Viceral',
            'valoration_age' => 'Edad',
            'valoration_hability' => 'Hability',
            'customer_name'=>'Cliente',
            'coach_name'=>'Entrenador',
            'class_date'=>'Fecha de la Clase',
            'valoration_plan'=>'Plan de usuario asociado'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValorationCoach() {
        return $this->hasOne(Coachs::className(), ['coach_id' => 'valoration_coach']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValorationUsuarios() {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'valoration_user']);
    }
    public function getCustomers() {
        
        $typeuser = Usuarios::findOne($this->valoration_user);
        
        if (!empty($typeuser)) {
            if ($typeuser->type_user == 4) {
                return $this->hasOne(Customers::className(), ['customer_user' => 'valoration_user']);
            }else
            if ($typeuser->type_user == 18) {
                return $this->hasOne(Refereers::className(), ['refereer_user' => 'valoration_user']);
            }
        }
        
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValorationClass() {
        return $this->hasOne(Clases::className(), ['class_id' => 'valoration_class']);
    }

}
