<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\valorations\models\Valorations */

$this->title = 'Update Valorations: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Valorations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->valoration_id, 'url' => ['view', 'id' => $model->valoration_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="valorations-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
