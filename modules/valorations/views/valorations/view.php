<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\valorations\models\Valorations */

$this->title = $model->valoration_id;
$this->params['breadcrumbs'][] = ['label' => 'Valorations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valorations-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->valoration_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->valoration_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'valoration_id',
            'valoration_user',
            'valoration_coach',
            'valoration_class',
            'valoration_weight',
            'valoration_imc',
            'valoration_grease',
            'valoration_muscle',
            'valoration_metabolism',
            'valoration_cervical',
            'valoration_age',
            'valoration_hability',
        ],
    ]) ?>

</div>
