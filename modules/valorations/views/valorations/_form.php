<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\valorations\models\Valorations */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="valorations-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'valoration_user')->textInput() ?>

    <?= $form->field($model, 'valoration_coach')->textInput() ?>

    <?= $form->field($model, 'valoration_class')->textInput() ?>

    <?= $form->field($model, 'valoration_weight')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valoration_imc')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valoration_grease')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valoration_muscle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'valoration_metabolism')->textInput() ?>

    <?= $form->field($model, 'valoration_cervical')->textInput() ?>

    <?= $form->field($model, 'valoration_age')->textInput() ?>

    <?= $form->field($model, 'valoration_hability')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
