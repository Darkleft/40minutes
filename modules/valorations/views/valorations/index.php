<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\Usuarios;
use app\modules\groups\models\Refereers;
use app\modules\customers\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\valorations\models\searchs\ValorationsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Valoraciones';
?>
<div class="valorations-index container">
    <?= Html::a('Atras', ['/site/index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-10 table-responsive">
    <?=
    GridView::widget([
        'tableOptions' => [
            'class'=>'table table-striped table-bordered'
            ],        
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'valoration_id',
            [
                'attribute' => 'customer_name',
                'value' => function($model) {
                    $typeuser = Usuarios::findOne($model->valoration_user);

                    if (!empty($typeuser)) {
                        if ($typeuser->type_user == 4) {
                            return Customers::find()->where("customer_user = $model->valoration_user")->one()->customer_name;
                        } else
                        if ($typeuser->type_user == 18) {
                            $referer = Refereers::find()->where("refereer_user = $model->valoration_user")->one();

                            return empty($referer) ? 'N/A':$referer->refereer_name;
                        } else {
                            return 'N/A';
                        }
                    }
                }
            ],
            [
                'attribute' => 'coach_name',
                'value' => 'valorationCoach.coach_name'
            ],
            [
                'attribute' => 'class_date',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->valorationClass->class_date, ['/clases/clases/view', 'id' => $model->valoration_class]);
                }
            ],
            'valoration_weight',
            'valoration_imc',
            'valoration_grease',
            'valoration_muscle',
            'valoration_metabolism',
            'valoration_cervical',
            'valoration_age',
        //'valoration_hability',
//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    </div>
</div>
