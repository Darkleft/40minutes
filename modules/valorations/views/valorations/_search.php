<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\valorations\models\searchs\ValorationsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="valorations-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'valoration_id') ?>

    <?= $form->field($model, 'valoration_user') ?>

    <?= $form->field($model, 'valoration_coach') ?>

    <?= $form->field($model, 'valoration_class') ?>

    <?= $form->field($model, 'valoration_weight') ?>

    <?php // echo $form->field($model, 'valoration_imc') ?>

    <?php // echo $form->field($model, 'valoration_grease') ?>

    <?php // echo $form->field($model, 'valoration_muscle') ?>

    <?php // echo $form->field($model, 'valoration_metabolism') ?>

    <?php // echo $form->field($model, 'valoration_cervical') ?>

    <?php // echo $form->field($model, 'valoration_age') ?>

    <?php // echo $form->field($model, 'valoration_hability') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
