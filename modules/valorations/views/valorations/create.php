<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\valorations\models\Valorations */

$this->title = 'Create Valorations';
$this->params['breadcrumbs'][] = ['label' => 'Valorations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valorations-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
