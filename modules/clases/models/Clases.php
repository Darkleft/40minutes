<?php

namespace app\modules\clases\models;

use Yii;
use app\modules\coachs\models\Coachs;
use app\modules\customers\models\Customers;
use app\modules\admin\models\Sublistas;
use app\modules\groups\models\Groups;

/**
 * This is the model class for table "class".
 *
 * @property int $class_id
 * @property int $class_type
 * @property int $class_custmer
 * @property int $class_coach
 * @property int $class_group
 * @property string $class_date
 * @property string $class_ini
 * @property string $class_end
 * @property string $class_address
 * @property int $class_assistants
 * @property string $class_observation
 * @property string $class_hability
 *
 * @property Coachs $classCoach
 * @property Customers $classCustmer
 * @property Groups $classGroup
 * @property Sublists $classType
 * @property ClassAsist[] $classAsists
 * @property Ratings[] $ratings
 * @property Valorations[] $valorations
 */
class Clases extends \yii\db\ActiveRecord {

    public $customer;
    public $coach;
    public $typeradio;
    public $typevalue;
    public $horas;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'class';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['class_type', 'class_custmer', 'class_date', 'class_address', 'class_assistants'], 'required'],
            [['class_type', 'class_custmer', 'class_coach', 'class_assistants', 'class_group'], 'integer'],
            [['class_date', 'class_ini', 'class_end'], 'safe'],
            [['class_observation'], 'string'],
            [['class_address', 'customer', 'coach'], 'string', 'max' => 100],
            [['class_hability'], 'string', 'max' => 1],
            [['class_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coachs::className(), 'targetAttribute' => ['class_coach' => 'coach_id']],
            [['class_custmer'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['class_custmer' => 'customer_id']],
            [['class_group'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['class_group' => 'group_id']],
            [['class_type'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['class_type' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'class_id' => 'Class ID',
            'class_type' => 'Tipo de Clase',
            'class_custmer' => 'Usuario',
            'class_coach' => 'Entrenador',
            'class_date' => 'Fecha',
            'class_address' => 'Dirección',
            'class_assistants' => 'Cantidad de Asistentes',
            'class_observation' => 'Observaciones',
            'class_hability' => 'Estado',
            'customer' => 'Cliente',
            'coach' => 'Entrenador',
            'class_group' => 'Grupo',
            'class_ini'=>'Hora de inicio',
            'class_end'=>'Hora de finaización'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassCoach() {
        return $this->hasOne(Coachs::className(), ['coach_id' => 'class_coach']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassCustmer() {
        return $this->hasOne(Customers::className(), ['customer_id' => 'class_custmer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassGroup() {
        return $this->hasOne(Groups::className(), ['group_id' => 'class_group']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassType() {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'class_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassAsists() {
        return $this->hasMany(ClassAsist::className(), ['ca_class' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatings() {
        return $this->hasMany(Ratings::className(), ['rating_class' => 'class_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValorations() {
        return $this->hasMany(Valorations::className(), ['valoration_class' => 'class_id']);
    }
    
    /*

     * Class hability
     * 
     * 0=>Solicitada
     * 1=>Asignada
     * 2=>Cancelada
     * 3=>Iniciada
     * 4=>Finalizada
     * 5=>Calificada
     *     
     */

}
