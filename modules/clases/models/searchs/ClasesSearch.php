<?php

namespace app\modules\clases\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\clases\models\Clases;

/**
 * ClasesSearch represents the model behind the search form of `app\modules\clases\models\Clases`.
 */
class ClasesSearch extends Clases {

    public $customer;
    public $coach;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['class_id', 'class_type', 'class_custmer', 'class_coach', 'class_assistants'], 'integer'],
            [['class_date', 'class_address', 'class_observation', 'class_hability', 'customer', 'coach'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Clases::find()->where('class_hability IN (0, 1)')->orderBy("class_date desc");

        $query->joinWith('classCustmer');
        $query->joinWith('classCoach');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'class_id' => $this->class_id,
            'class_type' => $this->class_type,
            'class_custmer' => $this->class_custmer,
            'class_coach' => $this->class_coach,
            'class_date' => $this->class_date,
            'class_assistants' => $this->class_assistants,
        ]);

        $query->andFilterWhere(['like', 'class_address', $this->class_address])
                ->andFilterWhere(['like', 'class_observation', $this->class_observation])
                ->andFilterWhere(['like', 'class_hability', $this->class_hability])
                ->andFilterWhere(['like', 'customer_name', $this->customer])
                ->andFilterWhere(['like', 'coach_name', $this->coach]);

        return $dataProvider;
    }

    public function searchClasesCoachAsig($params, $hability, $id) {
        $query = Clases::find()->where("class_coach = $id")->andWhere("class_hability IN ('$hability')")->orderBy("class_date desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'class_id' => $this->class_id,
            'class_type' => $this->class_type,
            'class_custmer' => $this->class_custmer,
            'class_coach' => $this->class_coach,
            'class_assistants' => $this->class_assistants,
        ]);

        $query->andFilterWhere(['like', 'class_address', $this->class_address])
                ->andFilterWhere(['like', 'class_observation', $this->class_observation])
                ->andFilterWhere(['like', 'class_hability', $this->class_hability])
                ->andFilterWhere(['like', 'class_date', $this->class_date]);

        return $dataProvider;
    }

    public function searchClasesCustomers($params, $hability, $id) {
        $query = Clases::find()->where("class_custmer = $id")->andWhere("class_hability IN ('$hability')")->orderBy("class_date desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'class_id' => $this->class_id,
            'class_type' => $this->class_type,
            'class_custmer' => $this->class_custmer,
            'class_coach' => $this->class_coach,
            'class_assistants' => $this->class_assistants,
        ]);

        $query->andFilterWhere(['like', 'class_address', $this->class_address])
                ->andFilterWhere(['like', 'class_observation', $this->class_observation])
                ->andFilterWhere(['like', 'class_hability', $this->class_hability])
                ->andFilterWhere(['like', 'class_date', $this->class_date]);

        return $dataProvider;
    }

}
