<?php

namespace app\modules\clases\models;

use Yii;
use app\modules\admin\models\Excercises;

/**
 * This is the model class for table "excercise_class".
 *
 * @property int $ec_id
 * @property int $ec_class
 * @property int $ec_exercise
 * @property string $ec_old
 * @property string $ec_serie
 * @property string $ec_repeat
 * @property string $ec_weight
 * @property string $ec_round
 * @property string $ec_medium
 * @property string $ec_circuit
 * @property string $ec_hability
 *
 * @property Class $ecClass
 * @property Excercises $ecExercise
 */
class Excerciseclass extends \yii\db\ActiveRecord
{
    
    public $musculo;
    public $tipo;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excercise_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ec_class', 'ec_exercise'], 'required'],
            [['ec_class', 'ec_exercise'], 'integer'],
            [['ec_old'], 'string', 'max' => 100],
            [['ec_serie', 'ec_repeat', 'ec_weight', 'ec_round', 'ec_medium', 'ec_circuit'], 'string', 'max' => 45],
            [['ec_hability'], 'string', 'max' => 1],
            [['ec_class'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['ec_class' => 'class_id']],
            [['ec_exercise'], 'exist', 'skipOnError' => true, 'targetClass' => Excercises::className(), 'targetAttribute' => ['ec_exercise' => 'excercise_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ec_id' => 'Ec ID',
            'ec_class' => 'Ec Class',
            'ec_exercise' => 'Ejercicio',
            'ec_old' => 'Ejercicio app Old',
            'ec_serie' => 'Serie',
            'ec_repeat' => 'Repeticiones',
            'ec_weight' => 'Peso',
            'ec_round' => 'Rondas',
            'ec_medium' => 'Medio',
            'ec_circuit' => 'Circuito',
            'ec_hability' => 'Ec Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEcClass()
    {
        return $this->hasOne(Clases::className(), ['class_id' => 'ec_class']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEcExercise()
    {
        return $this->hasOne(Excercises::className(), ['excercise_id' => 'ec_exercise']);
    }
}
