<?php

namespace app\modules\clases\models;

use Yii;

/**
 * This is the model class for table "class_asist".
 *
 * @property int $ca_id
 * @property int $ca_class
 * @property int $ca_user
 * @property string $ca_hability
 *
 * @property Class $caClass
 */
class Classasist extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_asist';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ca_class', 'ca_user'], 'required'],
            [['ca_class', 'ca_user'], 'integer'],
            [['ca_hability'], 'string', 'max' => 1],
            [['ca_class'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['ca_class' => 'class_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ca_id' => 'Ca ID',
            'ca_class' => 'Ca Class',
            'ca_user' => 'Ca User',
            'ca_hability' => 'Ca Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCaClass()
    {
        return $this->hasOne(Clases::className(), ['class_id' => 'ca_class']);
    }
}
