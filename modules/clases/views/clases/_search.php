<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\clases\models\searchs\ClasesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="clases-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'class_id') ?>

    <?= $form->field($model, 'class_type') ?>

    <?= $form->field($model, 'class_custmer') ?>

    <?= $form->field($model, 'class_coach') ?>

    <?= $form->field($model, 'class_date') ?>

    <?php // echo $form->field($model, 'class_address') ?>

    <?php // echo $form->field($model, 'class_assistants') ?>

    <?php // echo $form->field($model, 'class_observation') ?>

    <?php // echo $form->field($model, 'class_hability') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
