<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\clases\models\Clases */

$this->title = "Clase $model->class_id";
?>
<div class="clases-view">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-primary']) ?>
    <legend><h1><?= Html::encode($this->title) ?></h1></legend>

    <div class="col-lg-6">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => 'Tipo de Clase',
                    'attribute' => 'classType.sublist_name',
                ],
                [
                    'label' => 'Entrenador',
                    'attribute' => 'classCoach.coach_name',
                ],
                [
                    'label' => 'Cliente',
                    'attribute' => 'classCustmer.customer_name',
                ],
                'class_date',
            ],
        ])
        ?>
    </div>
    <div class="col-lg-6">
        <?=
        DetailView::widget([
            'model' => $model,
            'attributes' => [
                'class_assistants',
                'class_observation:ntext',
                [
                    'attribute' => 'class_hability',
                    'value' => function($model) {
                        switch ($model->class_hability) {
                            case '0':
                                $ret = "Soicitada";
                                break;
                            case '1':
                                $ret = "Asignada";
                                break;
                            case '2':
                                $ret = "Cancelada";
                                break;
                            case '3':
                                $ret = "Iniciada";
                                break;
                            case '4':
                                $ret = "Finalizada";
                                break;
                            case '5':
                                $ret = "Calificada";
                                break;
                            default :
                                $ret = "N/A";
                                break;
                        }
                        return $ret;
                    }
                ]
            ],
        ])
        ?>
    </div>
    <div class="col-lg-12">
        <legend>Ejercicios Realizados</legend>
        <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'ecExercise.excerciseMuscle.sublist_name',
                'ecExercise.excerciseShape.sublist_name',
                'ecExercise.excercise_description',
                'ec_old',
                'ec_serie',
                'ec_repeat',
                'ec_weight',
                'ec_round',
                'ec_medium',
                'ec_circuit',
            //'ec_hability',
            ],
        ]);
        ?>
        </div>
    </div>
</div>
