<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\clases\models\Clases */

$this->title = 'Modificar Clase';

?>
<div class="clases-update">
 
    <div style="margin-top: 5%">
        <h3> <?= $model->classCustmer->customer_name ?></h3>
    </div>
    <div style="padding-top: 2%">
        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>

</div>
