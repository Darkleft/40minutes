<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\editable\Editable;
use yii\helpers\Url;
use app\modules\admin\models\Sublistas;
use app\modules\coachs\models\Coachs;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\clases\models\searchs\ClasesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clases';
?>


<div class="clases-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);    ?>

    <p>
        <?= Html::a('Agendar Clase', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <br>
    <?= Html::button('Caliendario', ['class' => 'btn btn-info', 'onclick' => '$(".dv-clases").addClass("hide"); $(".calendar").removeClass("hide")']) ?>
    <?= Html::button('Lista', ['class' => 'btn btn-info', 'onclick' => '$(".dv-clases").addClass("hide"); $(".lista").removeClass("hide")']) ?>
    <div class="dv-clases calendar">
        <?php
        $js = <<< JS

        var eventSource=[$events];

        $("#select_name").on('change',function() {

        //get current status of our filters into eventSourceNew
        var eventSourceNew=[$events];

        //remove the old eventSources
        $('#eventFilterCalendar').fullCalendar('removeEventSource', eventSource[0]);
        //attach the new eventSources
        $('#eventFilterCalendar').fullCalendar('addEventSource', eventSourceNew[0]);
        $('#eventFilterCalendar').fullCalendar('refetchEvents');

        //copy to current source 
        eventSource = eventSourceNew;
    });
JS;

        $this->registerJs($js, \yii\web\View::POS_READY);
        ?>

        <?=
        \yii2fullcalendar\yii2fullcalendar::widget([
            //'events'=> $events, 
            'id' => 'event',
            'clientOptions' => [
                'editable' => false,
                'eventSources' => [$events],
                'draggable' => false,
                'droppable' => false,
                'displayEventTime'=>true,
                'displayEventEnd'=>false,
                'timeFormat'=>'h:mm a'
            ],
            'eventClick' => "function(calEvent, jsEvent, view) {

                $(this).css('border-color', 'blue');

                $.get('update',{'id':calEvent.id}, function(data){
                $('#modalmensajes').children().children().css('float','left');
                    $('#modalmensajes').modal()
                    .find('#modalmensajebody')
                    .html(data);
                    $('#modalheader')
                    .html('Clase ' + calEvent.id);
                    $('#btn-ok-modal').addClass('hide');
                })

                $('#calendar').fullCalendar('removeEvents', function (calEvent) {
                    return true;
                });

           }",
                /* $('#event').fullCalendar({
                  eventRender: function(event, element) {
                  if(event.status == "on leave") {
                  element.css('background-color', '#131313');
                  } else if (event.status == "stand by") {
                  element.css('background-color', '#678768');
                  } else if (event.status == "active") {
                  element.css('background-color', '#554455');
                  }
                  },
                  }); */
        ]);
        ?>
    </div>
    <div class="dv-clases lista hide">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                [
                    'attribute' => 'class_type',
                    'value' => function($model) {
                        return Sublistas::getName($model->class_type);
                    }
                ],
                [
                    'attribute' => 'customer',
                    'value' => 'classCustmer.customer_name'
                ],
                [
                    'class' => 'kartik\grid\EditableColumn',
                    'attribute' => 'coach',
                    'value' => 'classCoach.coach_name',
                    'editableOptions' => function ($model, $key, $index) {
                        return [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => Coachs::getList(),
                            'displayValueConfig' => Coachs::getList(),
                            'options' => ['prompt' => 'Seleccione un Entrenador'],
                            'formOptions' => ['action' => Url::to(['editables'])],
                        ];
                    },
                ],
                'class_date',
                'class_address',
                'class_assistants',
                'class_observation:ntext',
                'class_hability',
                ['class' => 'yii\grid\ActionColumn', 'header' => 'Acciones', 'template' => '{view}'],
            ],
        ]);
        ?>
    </div>
</div>
