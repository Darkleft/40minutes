<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\modules\coachs\models\Coachs;
use app\modules\customers\models\Customers;
use app\modules\admin\models\Sublistas;
use yii\jui\AutoComplete;
use yii\web\JsExpression;
use app\modules\admin\models\Usuarios;

/* @var $this yii\web\View */
/* @var $model app\modules\clases\models\Clases */
/* @var $form yii\widgets\ActiveForm */
if ($model->class_hability >= 3 || (!empty($model->class_date) && $model->class_date < date('Y-m-d H'))) {
    $disabled = 'disabled';
} else {
    $disabled = '';
}
$data = Usuarios::find()
        ->select(['username as value', 'username as label', 'username as id'])
        ->asArray()
        ->all();
?>

<div class="clases-form">

    <br>
    <?php $form = ActiveForm::begin(); ?>

    <?php
    if ($model->isNewRecord) {
        ?>
        <div class="col-lg-4">
            <?= Html::radioList('typeradio', '2', ['2' => 'Email'], ['required' => 'required', $disabled]) ?>
            <script>
                $('input:radio[name="typeradio"]').change(
                        function () {
                            $("#typevalue").prop("readonly", false);
                        });
            </script>
            <?=
            AutoComplete::widget([
                'model' => new Usuarios(),
                'attribute' => 'user_id',
                'clientOptions' => [
                    'source' => $data,
                    'minLength' => '3',
//            'autoFill' => true,
//        'select' => new JsExpression("function( event, ui ) {
//			        $('#memberssearch-family_name_id').val(ui.item.id);//#memberssearch-family_name_id is the id of hiddenInput.
//			     }")
                ],
                'options' => ['class' => 'form-control', 'id' => 'typevalue', 'required' => 'required', 'onblur' => "
            $.ajax({
                type:'POST',
                url:'create',
                data:{
                    ajaxtypevalue:true,
                    type: $('input:radio[name=typeradio]:checked').val(),
                    value:this.value,
                },
                success:function(data){
                    var obj = jQuery.parseJSON(data);
                    console.log(obj)
                    if (obj.valid) {
                        $('.addressselect').html(obj.address);
                        $('#clases-class_custmer').val(obj.customer);
                        $('.submit').removeClass('hide');
                    }else{
                     $('#clases-class_custmer').val('');
                        $('.submit').addClass('hide');
                        $('#modalmensajes').modal();
                        $('#modalmensajebody').html(obj.message);
                    }
                }
            })

            ",
                    $disabled]
            ]);
            ?>
            <?= $form->field($model, 'class_custmer')->hiddenInput(['readonly' => 'readonly'])->label(false) ?>

        </div>
    <?php } ?>
    <div class="col-lg-4">
        <?=
        $form->field($model, 'class_date')->widget(DatePicker::className(), [
            'language' => 'es',
            'dateFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                'buttonText' => 'Fecha',
                'showAnim' => 'slide',
                'minDate' => '0'
            ],
            'options' => ['class' => 'form-control', 'readonly' => true, $disabled]
        ])
        ?> 
    </div>
    <div class="col-lg-4">
        <label>Hora</label>
        <?=
        Html::DropDownList('hora', $model->horas, Sublistas::getHoursclass(), ['class' => 'form-control',
            'onchange' => "  
                $.ajax({
                type:'POST',
                url:'create',
                data:{
                    ajaxhours:true,
                    value:$('#clases-class_date').val()+' '+this.value,
                },
                success:function(data){
                    console.log(data);
                    $('.dv_coach').html(data);
                    
                }
            })
               ",
            $disabled
        ])
        ?>
    </div>
    <div class="col-lg-4">
        <b>Dirección</b><br>
        <div class="addressselect" style="margin-top: 5px">
            <?=
                    $form->field($model, 'class_address')
                    ->dropDownList(!empty($model->class_custmer) ? Customers::getAddress($model->class_custmer) : ['' => 'Seleccione'], [$disabled])->label(false)
            ?>
        </div>
    </div>
    <div class="col-lg-4 ">
        <b>Entrenador</b><br>
        <div class="dv_coach" style="margin-top: 5px">
            <?= $form->field($model, 'class_coach')->dropDownList(!empty($model->class_custmer) ? Coachs::getDisponible($model->class_date) : Coachs::getList(), [$disabled])->label(false) ?>
        </div>
    </div>
    <div class="col-lg-4">
        <label>Valoración en la clase</label><br>
        <?= Html::radioList('valoration', '2', ['1' => 'Si', '2' => 'No'], ['required' => 'required', $disabled]) ?>
    </div>
    <div class="clearfix"></div>
    <div style="text-align: center">
        <label> </label><br>
        <?= empty($disabled) ? Html::submitButton($model->isNewRecord ? 'Agendar' : 'Actualizar Clase', ['class' => 'btn btn-success submit']) : '' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
