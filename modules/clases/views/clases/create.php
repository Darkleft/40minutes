<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\clases\models\Clases */

$this->title = 'Agendar Clase';
?>
<div class="clases-create">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-primary']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
