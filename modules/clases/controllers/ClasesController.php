<?php

namespace app\modules\clases\controllers;

use Yii;
use app\modules\clases\models\Clases;
use app\modules\clases\models\searchs\ClasesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\models\User;
use app\modules\valorations\models\Valorations;
use app\modules\coachs\models\Coachs;
use app\modules\clases\models\searchs\ExcerciseclassSearch;
use app\modules\customers\models\Customers;
use app\modules\admin\models\Usuarios;
use app\modules\customers\models\Customersplans;
use app\modules\plans\models\Plans;
use DateTime;
use DateInterval;
use app\modules\coachs\models\CoachsHours;

/**
 * ClasesController implements the CRUD actions for Clases model.
 */
class ClasesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'editables', 'view', 'create', 'update'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Clases models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ClasesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Clases::find()->where('class_hability IN (0, 1)')->orderBy("class_date desc");

        if (is_null($choice) || $choice == 'all') {
//the function should return the same events that you were loading before
            $dbEvents = $query->all();
            $events = $this->loadEvents($dbEvents);
        } else {
//here you need to look up into the data base 
//for the relevant events against your choice
            $dbEvents = $query->where(['=', 'class_id', ':choice'])
                    ->params([':choice' => $choice])
                    ->asArray()
                    ->all();
            $events = $this->loadEvents($dbEvents);
        }

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'events' => $events
        ]);
    }

    /**
     * Displays a single Clases model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $searchModel = new ExcerciseclassSearch();
        $dataProvider = $searchModel->search($id);
        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Clases model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $modeluser = new User();
        $model = new Clases();
        $modelcoach = New Coachs();
        $modelvaloration = new Valorations();
        $valid = true;
        $id = '';
        $data = Usuarios::find()
                ->select(['username as value', 'username as  label', 'username as id'])
                ->asArray()
                ->all();
        if (Yii::$app->request->post('ajaxtypevalue')) {
            if (Yii::$app->request->post('type') == 1) {
                if (is_numeric(Yii::$app->request->post('value'))) {
                    $value = Yii::$app->request->post('value');
                    $customer = Customers::find()->where("customer_identification = $value")->one();
                    if (!empty($customer)) {
                        $id = $customer->customer_id;
                        $return = \yii\helpers\Html::activeDropDownList($model, 'class_address', Customers::getAddress($id), ['class' => 'form-control']);
                    } else {
                        $valid = false;
                        $message = 'El número de cedula no ha sido registrado.';
                    }
                } else {
                    $valid = false;
                    $message = 'La identificación debe ser númerica.';
                }
            } else
            if (Yii::$app->request->post('type') == 2) {
                $value = Yii::$app->request->post('value');
                if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $usuario = Usuarios::find()->where("username = '$value'")->one();
                    if (!empty($usuario)) {
                        $customer = Customers::find()->where("customer_user = $usuario->user_id")->one();
                        if (!empty($customer)) {
                            $id = $customer->customer_id;
                            $return = \yii\helpers\Html::activeDropDownList($model, 'class_address', Customers::getAddress($id), ['class' => 'form-control']);
                        } else {
                            $valid = false;
                            $message = 'El email no está registrado como cliente.';
                        }
                    } else {
                        $valid = false;
                        $message = 'Email no registrado.';
                    }
                } else {
                    $valid = false;
                    $message = 'Email no válido.';
                }
            }

//            print_r(Yii::$app->request->post());
            return json_encode(['valid' => $valid, 'message' => $message, 'address' => $return, 'customer' => $id]);
        } else
        if (Yii::$app->request->post('ajaxhours')) {
//            return Yii::$app->request->post('value');
            return \yii\helpers\Html::activeDropDownList($model, 'class_coach', Coachs::getDisponible(Yii::$app->request->post('value')), ['required' => 'required', 'class' => 'form-control']);
        } else
        if ($model->load(Yii::$app->request->post()) && $model->validate(Yii::$app->request->post())) {
            $save = true;
            $model->typevalue = Yii::$app->request->post('typevalue');
            $model->typeradio = Yii::$app->request->post('typeradio');

            $model->horas = Yii::$app->request->post('hora');
            $fechahora = $model->class_date . " " . $model->horas;
            $validate = $model->find()
                    ->where("class_date BETWEEN DATE_ADD('$fechahora', INTERVAL -2 HOUR) AND DATE_ADD('$fechahora', INTERVAL 2 HOUR)")
                    ->andWhere("class_coach = '" . $model->class_coach . "'")
                    ->one();

            if (!empty($validate)) {
                $save = false;
                $model->addError('class_coach', 'El entrenador ya esta asignado en un rango de dos horas.');
//$out = json_encode(['output' => '0', 'message' => 'El entrenador ya esta asignado en un rango de dos horas.']);
            }
            if (date("H", strtotime($model->horas)) < date("H", strtotime($model->classCoach->coach_range_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($model->classCoach->coach_range_end))) {
                $save = false;
            }
            $validatehours = CoachsHours::find()->where("hour_coach = $model->class_coach")
                            ->andWhere("hour_day = DAYOFWEEK('$fechahora')-1")->andWhere('hour_hability NOT IN ("0")')->all();
            if (!empty($validatehours)) {
                $count = count($validatehours);
                if ($count == 1) {
                    $modelhours1 = CoachsHours::findOne($validatehours[0]['hour_id']);
                    if (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
                if ($count == 2) {
                    $modelhours1 = CoachsHours::findOne($validatehours[0]['hour_id']);
                    $modelhours2 = CoachsHours::findOne($validatehours[1]['hour_id']);
                    if ((date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours2->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours2->hour_end)))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
                if ($count == 3) {
                    if ((date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours2->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours2->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours3->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours3->hour_end)))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
            } else {
                $save = false;
                $model->addError('class_coach', 'El entrenador no tiene Horarios Para este día.');
            }





            if ($save) {
                $modelusuario = Customers::findOne($model->class_custmer);
                $model->class_date = $fechahora;
                $modelusuarioplan = Customersplans::find()
                                ->where("cp_customer = $model->class_custmer")
                                ->andWhere("cp_hability NOT IN ('0')")->one();

                if ($modelusuarioplan->cpPlanCondition->pc_number_user > 1) {
                    $typeclass = 8;
                } else {
                    $typeclass = 7;
                }
                $model->class_type = $typeclass;
                $model->class_assistants = $modelusuarioplan->cpPlanCondition->pc_number_user;
                $model->class_hability = '1';
                if ($model->save()) {
                    if (Yii::$app->request->post('valiratiosn') == 1) {
                        $modelvaloration->valoration_class = $model->class_id;
                        $modelvaloration->valoration_coach = $model->class_coach;
                        $modelvaloration->valoration_user = $modelusuario->customer_user;
                        $modelvaloration->valoration_plan = $modelusuarioplan->cp_id;
                        $modelvaloration->save();
                    }
                    $coachname = $model->classCoach->coach_name;
                    $customername = $model->classCustmer->customer_name;
                    $customeruser = $model->classCustmer->customer_user;
                    $coachuser = $model->classCoach->coach_user;

                    $usercustomer = User::findIdentity($customeruser);
                    $usercoach = User::findIdentity($coachuser);

                    $contentcustomer = "Su clase a sido asignada para $model->class_date con el instructor $coachname.";
                    $contentcoach = "Se le ha asignado una clase para el cliente $customername el día $model->class_date";
                    if (Yii::$app->request->post('valiratiosn') == 1) {
                        $contentcustomer .= "<br><br> <b>NOTA: La cita de valoración quedo asignada para esta clase.</b>";
                        $contentcoach .= "<br><br> <b>NOTA: En esta clase se debe realizar una valoración, por favor tener en cuenta.</b>";
                    }
//$modeluser->mail("40minutes", $contentcustomer, $usercustomer->username, "Clases 40 Minutes");
//$modeluser->mail("40minutes", $contentcoach, $usercoach->username, "Clases 40 Minutes");
                    return $this->redirect(['index']);
                } else {
                    $msn = json_encode($model->errors);
                    Yii::$app->getSession()->setFlash('error', $msn);
                }
            }
//            print_r($model->typeradio);
        }
//        else{

        return $this->render('create', [
                    'model' => $model,
                    'data' => $data,
        ]);
//        }
    }

    /**
     * Updates an existing Clases model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {

        $this->layout = false;
        $model = $this->findModel($id);
        $modelvaloration = new Valorations();
        $save = true;
        if ($model->load(Yii::$app->request->post())) {
            $model->horas = Yii::$app->request->post('hora');
            $fechahora = $model->class_date . " " . $model->horas;
            $validate = $model->find()
                    ->where("class_date BETWEEN DATE_ADD('$fechahora', INTERVAL -2 HOUR) AND DATE_ADD('$fechahora', INTERVAL 2 HOUR)")
                    ->andWhere("class_coach = '" . $model->class_coach . "'")
                    ->one();

            if (!empty($validate)) {
                $save = false;
                $model->addError('class_coach', 'El entrenador ya esta asignado en un rango de dos horas.');
//$out = json_encode(['output' => '0', 'message' => 'El entrenador ya esta asignado en un rango de dos horas.']);
            }
            if (date("H", strtotime($model->horas)) < date("H", strtotime($model->classCoach->coach_range_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($model->classCoach->coach_range_end))) {
                $save = false;
                $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
            }

            if ($save) {
                $modelusuario = Customers::findOne($model->class_custmer);
                $model->class_date = $fechahora;
                $modelusuarioplan = Customersplans::find()
                                ->where("cp_customer = $model->class_custmer")
                                ->andWhere("cp_hability NOT IN ('0')")->one();

                if ($modelusuarioplan->cpPlanCondition->pc_number_user > 1) {
                    $typeclass = 8;
                } else {
                    $typeclass = 7;
                }
                $model->class_type = $typeclass;
                $model->class_assistants = $modelusuarioplan->cpPlanCondition->pc_number_user;
                $model->class_hability = '1';
                if ($model->save()) {
//                    $modelcoach->findOne($model->class_coach);
//                    $modelcoach->coah_hability = "2";
//                    $modelcoach->save();
                    if (Yii::$app->request->post('valoration') == 1) {
                        $validate = $modelvaloration->find()->where("valoration_class = $model->class_id")->one();
                        if (empty($validate)) {
                            $modelvaloration->valoration_class = $model->class_id;
                            $modelvaloration->valoration_coach = $model->class_coach;
                            $modelvaloration->valoration_user = $modelusuario->customer_user;
                            $modelvaloration->valoration_plan = $modelusuarioplan->cp_id;
                            $modelvaloration->save();
                        } else {
                            $validate->valoration_hability = '1';
                            $validate->save();
                        }
                    } else {
                        $validate = $modelvaloration->find()->where("valoration_class = $model->class_id")->one();
                        if (!empty($validate)) {
                            $validate->valoration_hability = '0';
                            $validate->save();
                        }
                    }
                    $coachname = $model->classCoach->coach_name;
                    $customername = $model->classCustmer->customer_name;
                    $customeruser = $model->classCustmer->customer_user;
                    $coachuser = $model->classCoach->coach_user;

                    $usercustomer = User::findIdentity($customeruser);
                    $usercoach = User::findIdentity($coachuser);

                    $contentcustomer = "Su clase a sido asignada para $model->class_date con el instructor $coachname.";
                    $contentcoach = "Se le ha asignado una clase para el cliente $customername el día $model->class_date";
                    if (Yii::$app->request->post('valiratiosn') == 1) {
                        $contentcustomer .= "<br><br> <b>NOTA: La cita de valoración quedo asignada para esta clase.</b>";
                        $contentcoach .= "<br><br> <b>NOTA: En esta clase se debe realizar una valoración, por favor tener en cuenta.</b>";
                    }
//$modeluser->mail("40minutes", $contentcustomer, $usercustomer->username, "Clases 40 Minutes");
//$modeluser->mail("40minutes", $contentcoach, $usercoach->username, "Clases 40 Minutes");
                    return $this->redirect(['index']);
                } else {
                    $msn = json_encode($model->errors);
                    echo $msn;
                    Yii::$app->getSession()->setFlash('error', $msn);
                }
            } else {
                $msn = json_encode($model->errors);
                echo $msn;
            }
        } else {

            $explode = explode(' ', $model->class_date);
            $model->horas = $explode[1];
            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    public function actionEditables() {
        $modeluser = new User();
        $modelvaloration = new Valorations();
        $modelcoach = New Coachs();
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $model = Clases::findOne($id_editable);
            $modelvaloration = Valorations::find()->where("valoration_class = $id_editable")->one();
            $attr = Yii::$app->request->post('editableAttribute');
            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Clases']);
            $post['Clases'] = $posted;
            $save = true;


            if ($attr == 'coach') {
                $validate = $model->find()
                        ->where("class_date BETWEEN DATE_ADD('$model->class_date', INTERVAL -2 HOUR) AND DATE_ADD('$model->class_date', INTERVAL 2 HOUR)")
                        ->andWhere("class_coach = '" . $post['Clases']['coach'] . "'")
                        ->one();
                if (!empty($validate)) {
                    $save = false;
                    $out = json_encode(['output' => '0', 'message' => 'El entrenador ya esta asignado en un rango de dos horas.']);
                } else {
                    $model->class_coach = $post['Clases']['coach'];
                }
            }
            $validatehours = CoachsHours::find()->where("hour_coach = $model->class_coach")
                            ->andWhere("hour_day = DAYOFWEEK('$model->class_date')-1")->andWhere('hour_hability NOT IN ("0")')->all();
            if (!empty($validatehours)) {
                $count = count($validatehours);
                if ($count == 1) {
                    $modelhours1 = CoachsHours::findOne($validatehours[0]['hour_id']);
                    if (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
                if ($count == 2) {
                    $modelhours1 = CoachsHours::findOne($validatehours[0]['hour_id']);
                    $modelhours2 = CoachsHours::findOne($validatehours[1]['hour_id']);
                    if ((date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours2->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours2->hour_end)))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
                if ($count == 3) {
                    if ((date("H", strtotime($model->horas)) < date("H", strtotime($modelhours1->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours1->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours2->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours2->hour_end))) ||
                            (date("H", strtotime($model->horas)) < date("H", strtotime($modelhours3->hour_ini)) || date("H", strtotime($model->horas)) > date("H", strtotime($modelhours3->hour_end)))) {
                        $model->addError('class_coach', 'El entrenador no esta habilitado para este rango de horas.');
                        $save = false;
                    }
                }
            } else {
                $save = false;
                $model->addError('class_coach', 'El entrenador no tiene Horarios Para este día.');
            }
            if ($model->load($post) && $save) {
                $out = json_encode(['output' => '', 'message' => 'El entrenador ha sido asignado.']);
                $model->class_hability = '1';
                if ($model->save()) {
                    $coachname = $model->classCoach->coach_name;
                    $customername = $model->classCustmer->customer_name;
                    $customeruser = $model->classCustmer->customer_user;
                    $coachuser = $model->classCoach->coach_user;

                    $usercustomer = User::findIdentity($customeruser);
                    $usercoach = User::findIdentity($coachuser);

                    $contentcustomer = "Su clase a sido asignada para $model->class_date con el instructor $coachname.";
                    $contentcoach = "Se le ha asignado una clase para el cliente $customername el día $model->class_date";
                    if (!empty($modelvaloration)) {
                        $contentcustomer .= "<br><br> <b>NOTA: La cita de valoración quedo asignada para esta clase.</b>";
                        $contentcoach .= "<br><br> <b>NOTA: En esta clase se debe realizar una valoración, por favor tener en cuenta.</b>";
                        $modelvaloration->valoration_coach = $model->class_coach;
                        $modelvaloration->save();
//                        $modeluser->mail("40minutes", $contentcustomer, $usercustomer->username, "Cita de Valoración 40 Minutes");
                    }
                    $modeluser->mail("40minutes", $contentcustomer, $usercustomer->username, "Clases 40 Minutes");
                    $modeluser->mail("40minutes", $contentcoach, $usercoach->username, "Clases 40 Minutes");
                    echo $out;
                    return;
                } else {
                    $out = json_encode(['output' => '0', 'message' => $model->errors]);
                }
            }
            echo $out;
            return;
        }
    }

    /**
     *
     * @param type $choice
     * @return type
     */
    public function actionFilterEvents($choice = null) {
        
    }

    /**
     * 
     * @param type $dbEvents
     * @return \yii2fullcalendar\models\Event
     */
    private function loadEvents($dbEvents) {
        foreach ($dbEvents AS $event) {
//Testing
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $event->class_id;
            $Event->title = $event->classCustmer->customer_name . " - " . $event->classCoach->coach_name;
            $Event->start = $event->class_date;
            $fecha = new DateTime($event->class_date);
            $fecha->add(new DateInterval('PT45M'));
            $Event->end = $fecha->format('Y-m-d H:i:s');
//            $Event->className = 'btn';
            $Event->backgroundColor = $event->class_hability == 0 ? "#FE642E" : "#5cb85c";
//            $Event->end = date('Y-m-d\TH:i:s\Z', strtotime($event->date_end . ' ' . $event->time_end));
            $events[] = $Event;
        }
        return $events;
    }

    /**
     * Deletes an existing Clases model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionDelete($id) {
//        $this->findModel($id);
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the Clases model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Clases the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Clases::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
