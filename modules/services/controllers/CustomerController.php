<?php

namespace app\modules\services\controllers;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use app\modules\admin\models\Usuarios;
use app\models\User;
use app\modules\customers\models\Customers;
use app\modules\coachs\models\Coachs;
use app\modules\plans\models\Plans;
use app\modules\plans\models\Conditions;
use app\modules\customers\models\Customersplans;
use app\modules\groups\models\Refereers;
use app\modules\groups\models\Groups;
use app\modules\admin\models\Sublistas;

class CustomerController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\admin\models\Usuarios";
    public $message = "Null Or Empty Model";
    public $path = "https://www.40minutes.com.co";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'encrypt' => ['GET', 'HEAD'],
            'decrypt' => ['GET', 'HEAD'],
            'create' => ['GET', 'HEAD'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'information' => ['POST', 'HEAD'],
            'getmodel' => ['POST', 'HEAD'],
            'actualiza' => ['POST', 'HEAD'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }
    public function actionGetmodel(){
        $customerid = Yii::$app->request->post('customer_id');
        $model = Customers::findOne($customerid);
        if (!empty($model)){
            $model->customer_user = $model->customerUser->username;
            return ["valid" => true, 'model' => $model];
        }else{
            return ["valid" => false, 'message' => 'Empty Model'];
        }
    }

    public function actionInformation() {
        $userid = Yii::$app->request->post('user_id');
        $customerid = Yii::$app->request->post('customer_id');
        $modeluser = Usuarios::findOne($userid);
        $model = Customers::findOne($customerid);
        if (Yii::$app->request->post('myprofile')) {
            if (!empty($model)) {
                return ["valid" => true, 'model' => $model];
            }else{
                return ["valid" => false, 'msn' => 'Empty Model'];
            }
        }else
        if (!empty($modeluser)) {
            if (!empty($model)) {
                if ($model->customer_identification == null || $model->customer_identification == 0 || empty($model->customer_identification)) {
                    return ["actualizar" => true, 'model' => $model];
                } else if ($model->customer_gender == null || $model->customer_gender == 0 || empty($model->customer_gender)) {
                    return ["actualizar" => true, 'model' => $model];
                }
                $url = User::get_fcontent("$this->path/40minutes/images/avatars/$model->customer_avatar");
                $avatar = 'data:image/png;base64,' . base64_encode($url[0]);
                $validateplan = Plans::findOne($model->customer_plan_active);
                $group = false;
                $datagroup = false;

                $dataplancative = Customersplans::find()
                        ->where("cp_customer = $model->customer_id")
                        ->andWhere("cp_plan = $model->customer_plan_active")
                        ->andWhere("cp_hability NOT IN (0)")
                        ->one();
                if (($model->customer_plan_active != 1 && $model->customer_plan_active != 0 && empty($dataplancative))) {
                    $model->customer_plan_active = 0;
                    $model->save();
                } else
                if (!empty($dataplancative)) {
                    if ($dataplancative->cp_expire == date('Y-m-d')) {
                        $dataplancative->cp_hability = "0";
                        if ($dataplancative->save()) {
                            $model->customer_plan_active = 0;
                            $model->save();
                        }
                    }
                }
                if (!empty($dataplancative) &&
                        Conditions::findOne($dataplancative->cp_plan_condition)->pc_number_user > 1) {
                    $group = true;
                    $validategroup = Groups::find()
                            ->where("group_customer = $model->customer_id")
                            ->andWhere("group_hability NOT IN ('0')")
                            ->one();
                    if (!empty($validategroup)) {
                        $datagroup = true;
                        $validatereferers = Refereers::find()
                                ->where("refereer_group =$validategroup->group_id")
                                ->andWhere("refereer_hability NOT IN ('0')")
                                ->all();
                        if (empty($validatereferers)) {
                            if ($validatereferers) {
                                
                            }
                            $datagroup = false;
                        }
                    } else {
                        $datagroup = false;
                    }
                }
                $dataplan = [
                    'id'=>$validateplan->plan_id,
                    'name' => $validateplan->plan_name,
                    'type' => Sublistas::getName($validateplan->plan_type),
                    'expire' => empty($dataplancative) ? 'N/A' : $dataplancative->cp_expire,
                    "cantidad" => !empty($dataplancative) ? Conditions::findOne($dataplancative->cp_plan_condition)->pc_number_user : 1,
                ];
                return [
                    'valid' => $group == $datagroup ? true:false,
                    'datacustomer' => $model,
                    'avatar' => $avatar,
                    'dataplan' => $dataplan,
                    'group' => $group,
                    'datagroup' => $datagroup,
                ];
            } else {
                return ['valid' => false, 'msn' => $this->message];
            }
        } else {
            return ['valid' => false, 'msn' => "User " . $this->message];
        }
    }

    public function actionActualiza() {
        $model = new Customers();

        $user = Usuarios::findOne(Yii::$app->request->post('id_usuario'));
        $customer = $model->find()->where("customer_user = $user->user_id")->one();
        $usuarios = Yii::$app->request->post('Version') == 3 ? ['Usuarios'=>json_decode(Yii::$app->request->post('Usuarios'), true)]:Yii::$app->request->post();
        $customers= Yii::$app->request->post('Version') == 3 ?['Customers'=>json_decode(Yii::$app->request->post('Customers'), true)]:Yii::$app->request->post();
        
        if (!empty($user) && $user->load($usuarios)) {
            if ($user->save()) {
                $ret = ['valid' => true, 'message' => 'Datos guardados con éxito.'];
            } else {
                $ret = ['valid' => false,'message' => 'Ocurrió un error durante el proceso.', 'error' => $user->errors];
            }
        }
        if (!empty($customer) && $customer->load($customers)) {
            if ($customer->save()) {
               $ret = ['valid' => true, 'message' => 'Datos guardados con éxito.', ];
            } else {
                $ret = ['valid' => false,'message' => 'Ocurrió un error durante el proceso.', 'error' => $customer->errors];
            }
        } else {
            $ret = ['valid' => false,'message' => 'Ocurrió un error durante el proceso.', 'error' => $customer->errors];
        }
        return $ret;
    }

    public function actionActivateplan() {
        if($_POST['x_cod_response'] == 1 || $_POST['x_cod_response'] == '1'){
            //Customer
            // $_POST['x_extra1']
            //Plan
            //$_POST['x_extra2']
            //Condicion
            //$_POST['x_extra3']     
            $model = Customers::findOne($_POST['x_extra1']);
            $plan = Yii::$app->request->post('Version') == 3 ? ['Customersplans'=>json_decode(Yii::$app->request->post('Customersplans'), true)]:Yii::$app->request->post();
            
            if (!empty($model)) {
                $conditionplan = new Customersplans();
                $conditionplan->cp_plan = $_POST['x_extra2'];
                $conditionplan->cp_plan_condition = $_POST['x_extra3'];                 
                $conditionplan->cp_number_class = 0;
                $conditionplan->cp_customer = $model->customer_id;
                $typeplan = Plans::findOne($conditionplan->cp_plan)->plan_type;
                if ($typeplan == 13) {
                    $expire = date_create(date('Y-m-d'));
                    date_add($expire, date_interval_create_from_date_string("3 MONTH"));
                } else
                if ($typeplan == 14) {
                    $expire = date_create(date('Y-m-d'));
                    date_add($expire, date_interval_create_from_date_string("1 MONTH"));
                }
                $valueplan = Conditions::findOne($conditionplan->cp_plan_condition);
                $conditionplan->cp_value = $valueplan->pc_price;
                $conditionplan->cp_expire = date_format($expire, "Y-m-d");
                if ($conditionplan->save()) {
                    $model->customer_plan_active = $conditionplan->cp_plan;
                    $model->save();
                    if ($valueplan->pc_number_user > 1) {
                        $group = true;
                    } else {
                        $group = false;
                    }
                    return ['valid' => true, 'group' => $group, 'number' => $valueplan->pc_number_user];
                } else {
                    return ['valid' => false, 'msn' => $conditionplan->errors];
                }

        } 

//            $p_cust_id_cliente = '';
//            $p_key = '';
//            $x_ref_payco = $_REQUEST['x_ref_payco'];
//            $x_transaction_id = $_REQUEST['x_transaction_id'];
//            $x_amount = $_REQUEST['x_amount'];
//            $x_currency_code = $_REQUEST['x_currency_code'];
//            $x_signature = $_REQUEST['x_signature'];
//            $signature = hash('sha256', $p_cust_id_cliente . '^' . $p_key . '^' . $x_ref_payco . '^' . $x_transaction_id . '^' . $x_amount . '^' . $x_currency_code);
//            $x_response = $_REQUEST['x_response'];
//            $x_motivo = $_REQUEST['x_response_reason_text'];
//            $x_id_invoice = $_REQUEST['x_id_invoice'];
//            $x_autorizacion = $_REQUEST['x_approval_code'];
////Validamos la firma
//            if ($x_signature == $signature) {
//                /* Si la firma esta bien podemos verificar los estado de la transacción */
//                $x_cod_response = $_REQUEST['x_cod_response'];
//                switch ((int) $x_cod_response) {
//                    case 1:
//                        # code transacción aceptada
//                        //echo "transacción aceptada";
//                        break;
//                    case 2:
//                        # code transacción rechazada
//                        //echo "transacción rechazada";
//                        break;
//                    case 3:
//                        # code transacción pendiente
//                        //echo "transacción pendiente";
//                        break;
//                    case 4:
//                        # code transacción fallida
//                        //echo "transacción fallida";
//                        break;
//                }
//            } else {
//                die("Firma no valida");
//            }
        }
    }
    public function actionPlanActive() {
        $model = Customers::findOne(Yii::$app->request->post('customer_id'));
        if (!empty($model)) {
            $planactive = Customersplans::find()->where("cp_customer = $model->")->andWhere()->one();
        }
    }

}
