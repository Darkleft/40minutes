<?php

namespace app\modules\services\controllers;

class ChatsController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\chats\models\Chats";
    public $modelClassUser = "\app\modules\chats\models\Userschat";
    public $modelClassMessages = "\app\modules\chats\models\Messageschat";
    public $modelPushBots = "\app\models\PushBots";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'newchat' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionNewchat() {
        $model = new $this->modelClassMessages;
        $modelchat = new $this->modelClass;
        if (\Yii::$app->request->post('message_text') && \Yii::$app->request->post('chat_user_admin') && \Yii::$app->request->post('chat_user_find')) {
            if ($modelchat->save()) {
                $modeluseradmin = new $this->modelClassUser;
                $modeluseradmin->uc_user = \Yii::$app->request->post('chat_user_admin');
                $modeluseradmin->uc_chat = $modelchat->chat_id;
                $modeluseradmin->uc_hability = "2";
                if ($modeluseradmin->save()) {
                    $modeluser = new $this->modelClassUser;
                    $modeluser->uc_user = \Yii::$app->request->post('chat_user_find');
                    $modeluser->uc_chat = $modelchat->chat_id;
                    if ($modeluser->save()) {
                        $model->message_user_chat = $modeluseradmin->uc_user;
                        $model->message_text = \Yii::$app->request->post('message_text');
                        if ($model->save()) {
                            //mensajes PUSHBOTS
                            return ['valid' => true];
                        }
                    }
                }
            }
        }
    }

    public function actionConversation() {

        $modelchat = new $this->modelClass;
        $modeluser = new $this->modelClassUser;

        if (\Yii::$app->request->post('chat_id')) {
            $chat = $modelchat->find()->where('chat_id = ' . \Yii::$app->request->post('chat_id'))->andWhere("chat_hability NOT IN ('0', '2')")->one();
            if (!empty($chat)) {
                $validate = $modeluser->find()->where("uc_chat = $chat->chat_id")->andWhere("uc_user = " . \Yii::$app->request->post('user_id'))->one();
                if (!empty($validate)) {
                    $model = new $this->modelClassMessages;
                    $model->message_user_chat = $validate->uc_user;
                    $model->message_text = \Yii::$app->request->post('message_text');
                    if ($model->save()) {
                        //mensajes PUSHBOTS
                        return ['valid' => true];
                    }
                } else {
                    return ['msn' => 'Usuario no chat'];
                }
            }
        }
    }

    public function actionAdduserchat() {

        $modelchat = new $this->modelClass;
        $modeluser = new $this->modelClassUser;

        if (\Yii::$app->request->post('chat_id')) {
            $chat = $modelchat->find()->where('chat_id = ' . \Yii::$app->request->post('chat_id'))->andWhere("chat_hability NOT IN ('0', '2')")->one();
            if (!empty($chat)) {
                $validate = $modeluser->find()->where("uc_chat = $chat->chat_id")->andWhere("uc_user = " . \Yii::$app->request->post('user_id'))->one();
                if (empty($validate)) {
                    $modeluser->uc_chat = $chat->chat_id;
                    $modeluser->uc_user = \Yii::$app->request->post('user_id');
                    if ($modeluser->save()) {
                        //mensajes PUSHBOTS
                        return ['valid' => true];
                    }
                } else {
                    return ['msn' => 'Usuario in chat'];
                }
            }
        }
    }

    public function actionDeleteuserchat() {

        $modelchat = new $this->modelClass;
        $modeluser = new $this->modelClassUser;

        if (\Yii::$app->request->post('chat_id')) {
            $chat = $modelchat->find()->where('chat_id = ' . \Yii::$app->request->post('chat_id'))->andWhere("chat_hability NOT IN ('0', '2')")->one();
            if (!empty($chat)) {
                $validate = $modeluser->find()->where("uc_chat = $chat->chat_id")->andWhere("uc_user = " . \Yii::$app->request->post('user_id'))->one();
                if (!empty($validate)) {
                    $validate->uc_hability = "0";
                    if ($validate->save()) {
                        //mensajes PUSHBOTS
                        return ['valid' => true];
                    } else {
                        return ['msn' => $validate->errors];
                    }
                } else {
                    return ['msn' => 'Usuario in chat'];
                }
            }
        }
    }
    
    public function actionConversacion(){
        return ['usurario'=>'Sergio Florez', 'mensaje'=>\Yii::$app->request->post('message_text')];
    }

}
