<?php

namespace app\modules\services\controllers;

use app\modules\admin\models\Ubications;
use app\modules\admin\models\Usuarios;
use app\modules\clases\models\Clases;

class UbicationsController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\admin\models\Ubications";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'getubication' => ['POST'],
            'setubication' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionSet() {
        $model = new Ubications();
        $usuario = \Yii::$app->request->post('id_user');
        $validateuser = Usuarios::findOne($usuario);
        if (!empty($validateuser)) {
            $validate = Ubications::find()->where("id_user = $usuario")->one();
            if (empty($validate)) {
                $model->id_user = $usuario;
                $model->long = \Yii::$app->request->post('long');
                $model->lat = \Yii::$app->request->post('lat');
                if ($model->save()) {
                    return ['valid' => true];
                } else {
                    return ['valid' => false];
                }
            } else {
                $validate->long = \Yii::$app->request->post('long');
                $validate->lat = \Yii::$app->request->post('lat');
                if ($validate->save()) {
                    return ['valid' => true];
                } else {
                    return ['valid' => false];
                }
            }
        }
    }

    public function actionGet() {
        $model = new Ubications();
        $clase = \Yii::$app->request->post('clase_id');
        $modelclass = Clases::findOne($clase);
        if (!empty($modelclass)) {
            $customeruser = $modelclass->classCustmer->customer_user;
            $coachuser = $modelclass->classCoach->coach_user;
            $ubicacustomer = $model->find()->where("id_user = $customeruser")->one();
            $ubicacoach = $model->find()->where("id_user = $coachuser")->one();
            return ['valid' => true, 'ubicacustomer' => $ubicacustomer, 
                'infocustomer'=>['name'=>$modelclass->classCustmer->customer_name], 'ubicacoach' => $ubicacoach, 'infocoach'=>['name'=>$modelclass->classCoach->coach_name]];
        } else {
            return ['valid' => false, 'message' => $this->message];
        }
    }

}
