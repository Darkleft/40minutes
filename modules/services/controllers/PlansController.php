<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\services\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\plans\models\Plans;
use app\modules\plans\models\Conditions;
use app\modules\admin\models\Sublistas;
use app\modules\plans\models\Cupons;
use app\modules\plans\models\Cuponuser;

class PlansController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\plans\models\Plans";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'encrypt' => ['GET', 'HEAD'],
            'decrypt' => ['GET', 'HEAD'],
            'create' => ['GET', 'HEAD'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'list' => ['POST'],
            'getname' => ['POST'],
            'data' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionList() {
        return ['valid' => true, 'list' => ArrayHelper::map(Plans::find()->where('plan_hability NOT IN ("0")')->andWhere('plan_id NOT IN (0, 1)')->all(), 'plan_id', 'plan_name')];
    }

    public function actionGetname() {
        $model = $this->modelClass->findOne(Yii::$app->request->post('id'));
        if (!empty($model)) {

            return ['valid' => true, 'name' => $model->plan_name];
        } else {
            return ['valid' => false, 'mns' => 'No existe el Plan'];
        }
    }

    public function actionData() {

        if (Yii::$app->request->post('id') != null && Yii::$app->request->post('id') != '') {
            $id = Yii::$app->request->post('id');
            $idcond = Yii::$app->request->post('id_condition');
            $zone = Yii::$app->request->post('zona');
            $number = Yii::$app->request->post('num');
            if (($id == 1 || $id == '0') && Yii::$app->request->post('ajaxdata') == null) {
                $model = Conditions::find()->where("pc_plan IN ($id)")->one();
                if (!empty($model)) {
                    $array = [];
                    $array['id'] = $model->pc_id;
                    $array['name'] = Plans::getName($model->pc_plan);
                    $array['zone'] = Sublistas::getName($model->pc_zone);
                    $array['nuser'] = $model->pc_number_user;
                    $array['price'] = $model->pc_price;
                    $array['info'] = Plans::getData("$model->pc_plan")->plan_description;
                    return ['valid' => true, 'model' => $array, 'nameplan' => Plans::getName($model->pc_plan)];
                } else {
                    return ['valid' => false, 'message' => $this->message];
                }
            } else {
                if ($idcond != null && $idcond != '') {
                    $model = Conditions::find()->where("pc_plan IN ($id)")->andWhere("pc_id IN ($idcond)")->one();
                    if (!empty($model)) {
                        $array = [];
                        $array['id'] = $model->pc_id;
                        $array['name'] = Plans::getName($model->pc_plan);
                        $array['zone'] = Sublistas::getName($model->pc_zone);
                        $array['nuser'] = $model->pc_number_user;
                        $array['price'] = $model->pc_price;
                        $array['info'] = Plans::getData($model->pc_plan)->plan_description;
                        return ['valid' => true, 'model' => $array];
                    } else {
                        return ['valid' => false, 'message' => $this->message];
                    }
                } else {
                    $conditions = Conditions::find()->where("pc_plan IN ($id)");
                    if (!empty($zone) && $zone != null) {
                        $conditions->andWhere("pc_zone IN ($zone)");
                    }
                    if (!empty($number) && $number != null) {
                        $conditions->andWhere("pc_number_user IN ($number)");
                    }
                    if (!empty($conditions->all())) {
                        $arraycondition = [];
                        foreach ($conditions->all() as $condition) {
                            $array = [];
                            $array['id'] = $condition->pc_id;
                            $array['name'] = Plans::getName($condition->pc_plan);
                            $array['zone'] = Sublistas::getName($condition->pc_zone);
                            $array['nuser'] = $condition->pc_number_user;
                            $array['price'] = $condition->pc_price;
                            $array['info'] = Plans::getData($condition->pc_plan)->plan_description;
                            $arraycondition[] = $array;
                        }
                        $zones = Sublistas::getZones();
                        return ['valid' => true, 'zones' => $zones, 'model' => $arraycondition, 'nameplan' => Plans::getName($condition->pc_plan)];
                    } else {
                        return ['valid' => false, 'message' => $this->message];
                    }
                }
                //$model;
            }
        } else {
            return ['valid' => false, 'message' => "Id: " . $this->message];
        }
    }

    public function actionValidatecupon() {
        $cupon = Yii::$app->request->post('cupon');
        $user = Yii::$app->request->post('id_user');
        $value = Yii::$app->request->post('value');
        $modelcupon = new Cupons();
        $modelcuponuser = new Cuponuser();

        $validatecupon = $modelcupon->find()->where("cupon_code= '$cupon'")->andWhere("cupon_hability NOT IN ('0')")->one();
        if (!empty($validatecupon)) {
            $validateuser = $modelcuponuser->find()->where("cu_cupon = $validatecupon->cupon_id")->one();
            if (empty($validateuser)) {
                if ($validatecupon->cupon_type == 34) {
                    $percentaje = $value * $validatecupon->cupon_value / 100;
                    $endvalue = $value - $percentaje;
                } else
                if ($validatecupon->cupon_type == 35) {
                    $endvalue = $value - $validatecupon->cupon_value;
                } else {
                    $endvalue = $value;
                }
                $modelcuponuser->cu_cupon =$validatecupon->cupon_id;
                $modelcuponuser->cu_user = $user;
                $modelcuponuser->save();
                return ['valid' => true, 'endvalue' => $endvalue];
            } else {
                return ['valid' => false, 'message' => 'El usuario ya ha tomado este cupón.'];
            }
        } else {
            return ['valid' => false, 'message' => 'El cupón no existe o ya ha expirado.'];
        }
    }

}
