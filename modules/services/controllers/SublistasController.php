<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\services\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Sublistas;

class SublistasController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\admin\models\Sublistas";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
    return [
         '*',                        // star allows all domains
        //'http://test1.example.com',
        //'http://test2.example.com',
    ];
}
    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'encrypt' => ['GET', 'HEAD'],
            'decrypt' => ['GET', 'HEAD'],
            'create' => ['GET', 'HEAD'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'gender' => ['GET'],
            'getname' => ['POST'],
            'exercisetype' => ['POST'],
            'exercisemuscle' => ['POST'],
            'zones' => ['POST'],
            'hours' => ['GET'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionGender() {
        return ['valid' => true, 'list' => ArrayHelper::map(Sublistas::find()->where('sublist_list  IN (3)')->andWhere('sublist_hability NOT IN ("0")')->all(), 'sublist_id', 'sublist_name')];
    }
    public function actionExercisetype() {
        return ['valid' => true, 'list' => ArrayHelper::map(Sublistas::find()->where('sublist_list  IN (9)')->andWhere('sublist_hability NOT IN ("0")')->all(), 'sublist_id', 'sublist_name')];
    }
    public function actionExercisemuscle() {
        return ['valid' => true, 'list' => ArrayHelper::map(Sublistas::find()->where('sublist_list  IN (8)')->andWhere('sublist_hability NOT IN ("0")')->all(), 'sublist_id', 'sublist_name')];
    }
    public function actionZones() {
        return ['valid' => true, 'list' => ArrayHelper::map(Sublistas::find()->where('sublist_list  IN (5)')->andWhere('sublist_hability NOT IN ("0")')->all(), 'sublist_id', 'sublist_name')];
    }
    public function actionHours() {
        return ['valid' => true, 'list' => Sublistas::getHours()];
    }
    
    
    public function actionGetname() {
        $model = $this->modelClass->findOne(Yii::$app->request->post('id'));
        if (!empty($model)) {
            
            return ['valid' => true, 'name' => $model->sublist_name];
        }else{
            return ['valid' => false, 'mns' => 'No existe el Item'];
        }
    }
}

