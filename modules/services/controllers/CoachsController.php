<?php

namespace app\modules\services\controllers;

use Yii;
use app\modules\clases\models\Clases;
use app\modules\ratings\models\Ratings;
use app\modules\admin\models\Usuarios;
use app\modules\admin\models\Sublistas;
use app\modules\customers\models\Customers;


class CoachsController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\coachs\models\Coachs";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'information' => ['POST'],
            'actualiza' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionInformation() {
        $userid = Yii::$app->request->post('user_id');
        $model = new $this->modelClass;
        $modelclases = new Clases();
        $coach = $model->find()->where("coach_user = $userid")->one();
        if (!empty($coach)) {

           // $searchModel = new ClasesSearch();
            $modelratings = Ratings::find()->join('join', 'class', 'rating_class = class_id')->andWhere("class_coach = $coach->coach_id")->all();
            $puntuality = 0;
            $empathy = 0;
            $presentation = 0;
            $general = 0;
            if (!empty($modelratings)) {
                $i = 0;
                foreach ($modelratings as $modelrating) {
                    $puntuality = ($puntuality + $modelrating->rating_puntuality) ;
                    $empathy = $empathy + ($modelrating->rating_empathy);
                    $presentation = $presentation + ($modelrating->rating_presentation);
                    $i++;
                }
                $presentation = ($presentation/$i);
                $empathy = $empathy/$i;
                $puntuality = $puntuality/$i;
                $general = ($puntuality + $empathy + $presentation) / 3;
            }

            $calification = ['puntuality' =>number_format( $puntuality,2), 'empathy' => number_format($empathy,2), 'presentation' => number_format($presentation,2), 'general' => number_format($general, 2)];
            //usuario
            $user = Usuarios::findOne($coach->coach_user);
            switch ($user->user_hability) {
                case 0:
                    $user->user_hability = 'Registrado';
                    break;
                case 1:
                    $user->user_hability = 'Activo';
                    break;
                case 2:
                    $user->user_hability = 'Inactivo';
                    break;
                default :
                    $user->user_hability = 'Error';
                    break;
            }

            //datacoach
            $coach->coach_user = $coach->coachUser->username;
            $coach->coach_working_day = $coach->coachWorkingDay->sublist_name;

            $clasesasignadas=$modelclases->find()
                    ->where("class_coach = $coach->coach_id")
                    ->andWhere("class_hability IN ('1')")
                    ->andWhere("class_date > DATE_ADD(CURRENT_DATE(), INTERVAL -1 DAY)")
                    ->limit(5)->orderBy("class_date desc")->all();
            
            foreach ($clasesasignadas as $clasesasignada) {
                $clasesasignada->class_type = Sublistas::getName($clasesasignada->class_type);
                $clasesasignada->class_custmer = Customers::getNamecustomer($clasesasignada->class_custmer);
            }
            
            $clasesiniciadas=$modelclases->find()->where("class_coach = $coach->coach_id")->andWhere("class_hability IN ('3')")->orderBy("class_date desc")->all();
            
            foreach ($clasesiniciadas as $clasesiniciada) {
                $clasesiniciada->class_type = Sublistas::getName($clasesiniciada->class_type);
                $clasesiniciada->class_custmer = Customers::getNamecustomer($clasesiniciada->class_custmer);
            }


            return [
                'valid' => true,
                'user' => $user,
                'datacoach' => $coach,
                'clasesasignadas' => $clasesasignadas,
                'clasesiniciadas'=>$clasesiniciadas,
                //'clasesfinalizadas' => $searchModel->searchClasesCoachAsig(NULL, 4, $coach->coach_id)->query->all(),
                //'clasescanceladas' => $searchModel->searchClasesCoachAsig(NULL, 2, $coach->coach_id)->query->all(),
                'calification' => $calification
            ];
        } else {
            return ['valid' => false, 'msn' => $this->message];
        }
    }

    public function actionActualiza() {
        $model = new $this->modelClass;

        $user = Usuarios::findOne(Yii::$app->request->post('user_id'));
        if (!empty($user)) {
            $coach = $model->find()->where("coach_user = $user->user_id")->one();
            if ($user->load(Yii::$app->request->post())) {
                if ($user->save()) {
                    $ret = ['valid' => true, 'msn' => 'Datos guardados con éxito.'];
                } else {
                    $ret = ['valid' => false,'msn' => 'Ocurrió un error durante el proceso.', 'error' => $user->errors];
                }
            } else {
                $ret = ['valid' => false,'msn' => 'Ocurrió un error durante el proceso.', 'error' => $user->errors];
            }
            if ($coach->load(Yii::$app->request->post())) {
                if ($coach->save()) {
                    $ret = ['valid' => true, 'msn' => 'Datos guardados con éxito.', 'model' => $coach, 'email' => $user->username];
                } else {
                    $ret = ['valid' => false,'msn' => 'Ocurrió un error durante el proceso.', 'error' => $coach->errors];
                }
            } else {
                $ret = ['valid' => false,'msn' => 'Ocurrió un error durante el proceso.', 'error' => $coach->errors];
            }
        } else {
            $ret = ['valid' => false,'msn' => "$this->message"];
        }
        return $ret;
    }

}
