<?php

namespace app\modules\services\controllers;

use Yii;
use app\modules\clases\models\Clases;
use app\modules\customers\models\Customersplans;
use app\modules\plans\models\Plans;
use app\modules\coachs\models\Coachs;
use app\models\User;
use app\modules\customers\models\Customers;
use app\modules\clases\models\Excerciseclass;
use app\modules\admin\models\Excercises;
use app\modules\admin\models\Sublistas;
use app\modules\admin\models\Usuarios;
use app\modules\clases\models\Classasist;
use app\modules\valorations\models\Valorations;
use app\modules\ratings\models\Ratings;
use app\modules\chats\models\Chats;
use app\modules\groups\models\Groups;
use app\models\PushBots;
use app\modules\groups\models\Refereers;

class ClasesController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\clases\models\Clases";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'countclass' => ['POST'],
            'newclass' => ['POST'],
            'iniclass' => ['POST'],
            'endclass' => ['POST'],
            'viewclass' => ['POST'],
            'cancelclass' => ['POST'],
            'excercisesclass' => ['POST'],
            'assiteclass' => ['POST'],
            'infoclass' => ['POST'],
            'calificaclass' => ['POST'],
            'chat' => ['POST']
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionCountclass() {
        $id_customer = \Yii::$app->request->post('id_customer');
        if ($id_customer == !null && !empty($id_customer)) {
            $modelcustomer = Customers::findOne($id_customer);
            if (!empty($modelcustomer)) {
                if ($modelcustomer->customer_plan_active == 0) {
                    return ['valid' => false, "message" => "No tiene un plan activo."];
                } else
                if ($modelcustomer->customer_plan_active == 1) {
                    return ['valid' => true, "faltan" => 1, 'utiliza' => 0];
                } else {
                    $plancuntomer = Customersplans::find()->where("cp_customer = $id_customer")->andWhere('cp_hability NOT IN ("0")')->one();
                    if (!empty($plancuntomer)) {
                        $plan = Plans::findOne($plancuntomer->cp_plan);
                        return ['valid' => true, "faltan" => ($plan->plan_number_class - $plancuntomer->cp_number_class), 'utiliza' => $plancuntomer->cp_number_class];
                    } else {
                        return ['valid' => false, "message" => "No hay cuenta de plan"];
                    }
                }
            } else {
                return ['valid' => false, "message" => $this->message];
            }
        } else {
            return ['valid' => false, "message" => "Vacio"];
        }
    }

    public function actionClassuser() {
        date_default_timezone_set('America/Bogota');
        $id_customer = \Yii::$app->request->post('id_customer');
        $id_referer = \Yii::$app->request->post('id_referer');
        if ($id_customer == !null && !empty($id_customer)) {
            $modelcustomer = Customers::findOne($id_customer);
            if (!empty($modelcustomer)) {
                $hability = \Yii::$app->request->post('hability');
                if ($hability == 4) {
                    $hability = "'4','5'";
                }
                $clases = Clases::find()->where("class_custmer = $id_customer")->andWhere("class_hability IN($hability)")->orderBy("class_date desc")->all();
                if (empty($clases)) {
                    switch ($hability) {
                        case '0':
                            $ret = 'Solicitadas';
                            break;
                        case '1':
                            $ret = 'Asignadas';
                            break;
                        case '2':
                            $ret = 'Canceladas';
                            break;
                        case '3':
                            $ret = 'Iniciadas';
                            break;
                        case '4':
                            $ret = 'Finalizadas';
                            break;
                        case '5':
                            $ret = 'Calificadas';
                            break;
                        default:
                            $ret = 'N/A';
                    }
                    return ['valid' => false, "message" => "No tienes clases $ret", 'estado' => $ret];
                }
                foreach ($clases as $class) {
                    switch ($class->class_hability) {
                        case '0':
                            $ret = 'Solicitadas';
                            break;
                        case '1':
                            $ret = 'Asignadas';
                            break;
                        case '2':
                            $ret = 'Canceladas';
                            break;
                        case '3':
                            $ret = 'Iniciadas';
                            break;
                        case '4':
                            $ret = 'Finalizadas';
                            break;
                        case '5':
                            $ret = 'Calificadas';
                            break;
                        default:
                            $ret = 'N/A';
                    }
                    $class->class_group = date('g:i a', strtotime($class->class_date));
                    $class->class_coach = empty($class->class_coach) ? null : Coachs::getNamecoach($class->class_coach);
                    $class->class_hability = $ret;
                }

                return ['valid' => true, "model" => $clases, 'estado' => $class->class_hability];
            } else {
                return ['valid' => false, "message" => $this->message];
            }
        } else
        if ($id_referer == !null && !empty($id_referer)) {
            $modelreferer = Refereers::findOne($id_referer);
            if (!empty($modelreferer)) {
                $grupo = Groups::findOne($modelreferer->refereer_group);
                $modelcustomer = Customers::findOne($grupo->group_customer);
                if (!empty($modelcustomer)) {
                    $hability = \Yii::$app->request->post('hability');
                    if ($hability == 4) {
                        $hability = "'4','5'";
                    }
                    $clases = Clases::find()->where("class_custmer = $grupo->group_customer")->andWhere("class_hability IN($hability)")->orderBy("class_date desc")->all();
                    if (empty($clases)) {

                        return ['valid' => false, "message" => "No tienes clases Asignadas", 'estado' => 'Asignadas'];
                    }
                    foreach ($clases as $class) {
                        $class->class_coach = empty($class->class_coach) ? null : Coachs::getNamecoach($class->class_coach);
                        $class->class_hability = 'Asignadas';
                    }

                    return ['valid' => true, "model" => $clases, 'estado' => $class->class_hability];
                } else {
                    return ['valid' => false, "message" => $this->message];
                }
            } else {
                return ['valid' => false, "message" => $this->message];
            }
        } else {
            return ['valid' => false, "message" => "Vacio"];
        }
    }

    public function actionNewclass() {
        $model = new Clases();
        $modelcoachs = new Coachs();
        $modeluser = new User();
        $modelcustomer = new Customers();
        $class = Yii::$app->request->post();

        if ($model->load($class)) {
            $customer = $modelcustomer->findOne($model->class_custmer);
            $user = Usuarios::findOne($customer->customer_user);
            $explode = explode(" ", $model->class_date);
            $fecha = $explode[0];
            $hora = $explode[1];
            $save = true;
            $message = "";
            if ($customer->customer_plan_active == '0') {
                $save = false;
                return ['valid' => false, 'message' => "No tiene ningun plan activo."];
            }
            $validatenumberclass = Customersplans::find()
                            ->where("cp_customer = $model->class_custmer")
                            ->andWhere('cp_hability NOT IN ("0", "2")')->one();
            if (!empty($validatenumberclass)) {
                if ($validatenumberclass->cpPlanCondition->pc_number_user > 1) {
                    $typeclass = 8;
                } else {
                    $typeclass = 7;
                }
                $model->class_type = $typeclass; //validar segun el plan
                $model->class_assistants = $validatenumberclass->cpPlanCondition->pc_number_user;
                $plan = Plans::findOne($validatenumberclass->cp_plan);
                if ($validatenumberclass->cp_expire != '0000-00-00' && $validatenumberclass->cp_expire < date('Y-m-d')) {
                    $save = false;
                    $message = "Su plan a expirado";
                }
                $vaidateclassuser = Clases::find()
                                ->where("class_custmer = $model->class_custmer")
                                ->andWhere("class_date > '$modelcustplan->cp_activate'")
                                ->andWhere("class_hability NOT IN ('2')")->count();
                if ($validatenumberclass->cp_number_class >= $plan->plan_number_class || $vaidateclassuser >= $plan->plan_number_class) {
                    $save = false;
                    $message = "Ya ha tomado la cantidad de clases adquiridas en el plan $plan->plan_name";
                }
                if (date("H") >= Sublistas::getHoursolicit() && date("H", strtotime($model->class_date)) <= 12 && date("d", strtotime($model->class_date)) == (date("d") + 1)) {
                    $save = false;
                    $message = "Debe solicitar las calses con mas tiempo de anterioridad.";
                }
                if (date("H", strtotime($model->class_date)) <= (date('H') + 2) && date("d", strtotime($model->class_date)) <= date("d")) {
                    $save = false;
                    $message = "Debe solicitar las calses en una fecha posterior y/o con dos horas de diferencia a la hora actual.";
                }
                if ($save) {
                    $validateday = Clases::find()->where('class_custmer = ' . $model->class_custmer)
                                    ->andWhere("class_date LIKE '$fecha%'")->andWhere("class_hability NOT IN ('2')")->one();
                    if (!empty($validateday)) {
                        /* if ($validateday->cp_plan == 15) {

                          } else */
                        $save = false;
                        $message = "Ya se ha solicitado una clase para este día.";
                        //}
                    } else {
                        $coachs = $modelcoachs->find()->where("'$hora' BETWEEN coach_range_ini AND coach_range_end")->all();
                        if (empty($coachs)) {
                            $save = false;
                            $message = "No hay entrenadores disponibles para esta clase. $hora";
                        }
                        if ($save) {
                            if ($model->save()) {
                                $message = 'Clase Solicitada';
                                $modeluser->mail("40minutes", "Solicitud de asignación de Clase para $customer->customer_name el día $model->class_date", \Yii::$app->params['adminEmail'], "Clases 40 Minutes");
                            } else {
                                $save = false;
                                $message = "$model->errors";
                            }
                        }
                    }
                }
            } else if ($customer->customer_plan_active == '1') {
                $modelcustplan = new Customersplans();
                $modelcustplan->cp_customer = $customer->customer_id;
                $modelcustplan->cp_plan = 1;
                $modelcustplan->cp_number_class = 0;
                $modelcustplan->cp_plan_condition = 53;
                $modelcustplan->cp_expire = $user->create_at = date('Y-m-d', strtotime("$user->create_at +7 days"));
                $modelcustplan->cp_valorations = '0';
                $modelcustplan->cp_value = '0';
                if ($modelcustplan->save()) {
                    if ($modelcustplan->cpPlanCondition->pc_number_user > 1) {
                        $typeclass = 8;
                    } else {
                        $typeclass = 7;
                    }
                    $model->class_type = $typeclass; //validar segun el plan
                    $model->class_assistants = $validatenumberclass->cpPlanCondition->pc_number_user;
                    $plan = Plans::findOne($validatenumberclass->cp_plan);
                    if ($validatenumberclass->cp_expire != '0000-00-00' && $validatenumberclass->cp_expire < date('Y-m-d')) {
                        $save = false;
                        $message = "Su plan a expirado";
                    }
                    $vaidateclassuser = Clases::find()
                                    ->where("class_custmer = $model->class_custmer")
                                    ->andWhere("class_date > '$modelcustplan->cp_activate'")
                                    ->andWhere("class_hability NOT IN ('2')")->count();
                    if ($validatenumberclass->cp_number_class >= $plan->plan_number_class || $vaidateclassuser >= $plan->plan_number_class) {
                        $save = false;
                        $message = "Ya ha tomado la cantidad de clases adquiridas en el plan $plan->plan_name";
                    }
                    if (date("H") >= Sublistas::getHoursolicit() && date("H", strtotime($model->class_date)) <= 12 && date("d", strtotime($model->class_date)) == (date("d") + 1)) {
                        $save = false;
                        $message = "Debe solicitar las calses con mas tiempo de anterioridad.";
                    }
                    if (date("H", strtotime($model->class_date)) <= (date('H') + 2) && date("d", strtotime($model->class_date)) <= date("d")) {
                        $save = false;
                        $message = "Debe solicitar las calses en una fecha posterior y/o con dos horas de diferencia a la hora actual.";
                    }
                    if ($save) {
                        if ($model->save()) {
                            $message = 'Clase Solicitada';
                            $modeluser->mail("40minutes", "Solicitud de asignación de Clase para $customer->customer_name el día $model->class_date", \Yii::$app->params['adminEmail'], "Clases 40 Minutes");
                        } else {
                            $save = false;
                            $message = "$model->errors";
                        }
                    }
                } else {
                    $save = false;
                    $message = json_encode($modelcustplan->errors);
                }
            } else {
                $save = false;
                $message = "No tiene ningun plan activo.";
            }
            return ['valid' => $save, 'message' => $message, 'clase_id' => $save ? $model->class_id : 'NO', 'cp_id' => $validatenumberclass->cp_id];
        } else {
            return ['valid' => false, 'message' => $this->message];
        }
    }

    public function actionAsisteclass() {
        $model = new Classasist();
        $model->load(\Yii::$app->request->post());
        $validate = $model->find()->where("ca_class = $model->ca_class")->andWhere("ca_user=$model->ca_user")->one();
        if (empty($validate)) {
            $model->save();
        } else {
            $validate->ca_hability = \Yii::$app->request->post('status');
            $validate->save();
        }
    }

    public function actionIniclass() {
        $model = Clases::findOne(\Yii::$app->request->post('id_clase'));
        if (!empty($model)) {
            $fecha1 = new \DateTime($model->class_date);

            $fecha2 = new \DateTime(date("Y-m-d H:i:s"));
            $intervalo = $fecha1->diff($fecha2);
            $dia = date('d', strtotime($model->class_date));
            $hora = $intervalo->format('%H');
            $minuto = $intervalo->format('%i');
            if ($dia == date('d') && $hora == '0' && $minuto < '45' && $model->class_hability == '1') {
                $model->class_assistants = '1';
                $model->class_hability = '3';
                $model->class_ini = date('H:i:s');
                if ($model->save()) {
                    $modelcoach = Coachs::findOne($model->class_coach);
                    $modelcoach->coah_hability = "2";
                    $modelcoach->save();
                }
                return ['valid' => true, 'inicio' => date('H:i') . " Hrs"];
            } else {
                return ['valid' => false, 'message' => "La clase no pudo ser iniciada por diferencia en la hora establecida."];
            }
        }
    }

    public function actionExcercisesclass() {
        $model = new Excerciseclass();
        if ($model->load(\Yii::$app->request->post())) {
            if (!$model->save()) {
                return ['valid' => false, 'message' => 'Ocurrió un error durante el proceso. ' . $model->errors];
            } else {
                return ['valid' => true, 'message' => 'Datos guardados.'];
            }
        } else {
            return ['valid' => false, 'message' => $this->message];
        }
    }

    public function actionEndclass() {
        $modeluser = new User();
        $model = Clases::findOne(\Yii::$app->request->post('class_id'));
        if (!empty($model)) {
            $modelexcercise = Excerciseclass::find()->where("ec_class = $model->class_id")->all();
            $modelvaloration = Valorations::find()->where("valoration_class = $model->class_id")->andWhere("valoration_hability IN (1)")->all();
            if (!empty($modelvaloration)) {
                return ['valid' => false, 'message' => 'Esta clase necesita Valoraciones de los clientes.'];
            }
            if (!empty($modelexcercise)) {
                $modelcoach = Coachs::findOne($model->class_coach);
                $modelcustplan = Customersplans::find()
                                ->where("cp_customer = $model->class_custmer")
                                ->andWhere("cp_hability IN ('1')")->one();
                $model->class_hability = '4';
                $model->class_end = date('H:i:s');
                $modelcoach->coah_hability = "1";
                $modelcustplan->cp_number_class = $modelcustplan->cp_number_class + 1;

                if ($model->save()) {
                    $message = 'Clase Finalizada.';
                    if ($modelcustplan->cp_number_class == $modelcustplan->cpPlan->plan_number_class) {
                        $modelcustomer = Customers::findOne($model->class_custmer);
                        $modelcustplan->cp_hability = '0';
                        $modelcustomer->customer_plan_active = '0';
                        $modelcustomer->save();
                        $validategroup = $model = Groups::find()->where("group_customer = $model->class_custmer")->andWhere("group_hability NOT IN (0)")->one();
                        if (!empty($validategroup)) {
                            $group = Groups::findOne($validategroup->group_id);
                            $group->group_hability = '0';
                            if ($group->save()) {
                                $referers = Refereers::find()->where("refereer_group = $group->group_id")->andWhere("refereer_hability NOT IN ('0')")->all();

                                if (!empty($referers)) {
                                    foreach ($referers as $key => $value) {
                                        $newcustomer = new Customers;
                                        $newcustomer->customer_user = $value->refereer_user;
                                        $newcustomer->customer_identification = $value->refereer_identification;
                                        $newcustomer->customer_name = $value->refereer_name;
                                        $newcustomer->customer_plan_active = '0';

                                        if($newcustomer->save()) {
                                            $user = Usuarios::findOne($value->refereer_user);
                                            $user->type_user = 4;
                                            $user->save();
                                        }
                                    }
                                }
                            }
                        }
                        $modeluser->mail("40minutes", "Apreciado Cliente $modelcustomer->customer_name, el día de hoy ha culminado las clases asugnadas, a partir de la fecha su plan queda inactivo.", $modelcustomer->customerUser->username, "Clases 40 Minutes");
                    }
                    if ($modelcoach->save() && $modelcustplan->save()) {
                        
                    }
                    return ['valid' => true, 'message' => $message];
                } else {
                    return ['valid' => false, 'message' => 'Ocurrión un error durante el proceso. ' . $model->errors . " ó " . $modelcoach->errors . " ó " . $modelcustplan->errors];
                }
            } else {
                return ['valid' => false, 'message' => 'No se ha ingresado ninguna Rutina a la clase.'];
            }
        } else {
            return ['valid' => false, 'message' => 'Clase no existe.'];
        }
    }

    public function actionCancelclass() {
        $model = Clases::findOne(\Yii::$app->request->post('class_id'));
        $modeluser = new User();
        if (!empty($model)) {
            $hoy = date('d');
            $hora = date('H');
            $clase = date('d', strtotime($model->class_date));
            if (($hoy >= $clase || (($hoy + 1) == $clase && $hora >= Sublistas::getHourcancel())) && !empty($model->class_coach)) {
                return ['valid' => false, 'message' => 'La clase ya no puede ser cancelada.'];
            }
            if ($hoy < $clase) {
                $model->class_hability = "2";
                if ($model->save()) {
                    $modeluser->mail("40minutes", "Se ha cancelado la Clase para " . $model->classCustmer->customer_name . " del día $model->class_date", $model->classCoach->coachUser->username, "Cancelación de clase");
                    $modeluser->mail("40minutes", "Se ha cancelado su Clase del día $model->class_date", $model->classCustmer->customerUser->username, "Cancelación de clase");
                    return ['valid' => true, 'message' => 'Clase Cancelada.'];
                } else {
                    return ['valid' => false, 'message' => 'Ocurrió un error durante el proceso. ' . $model->errors];
                }
            } else {
                return ['valid' => false, 'message' => "La clase no puedo ser cancelada."];
            }
        } else {
            return ['valid' => false, 'message' => 'Clase no existe.'];
        }
    }

    public function actionViewclass() {
        $model = Clases::findOne(\Yii::$app->request->post('class_id'));
        if (!empty($model)) {
            switch ($model->class_hability) {
                case '0':
                    $ret = 'Solicitada';
                    break;
                case '1':
                    $ret = 'Asignada';
                    break;
                case '2':
                    $ret = 'Cancelada';
                    break;
                case '3':
                    $ret = 'Iniciada';
                    break;
                case '4':
                    $ret = 'Finalizada';
                    break;
                case '5':
                    $ret = 'Calificada';
                    break;
                default:
                    $ret = 'N/A';
            }
            $model->class_coach = $model->class_coach != null && $model->class_coach != '' ? Coachs::getNamecoach($model->class_coach) : 'No Asignado';
            $model->class_hability = $ret;
            $modelexcercise = Excerciseclass::find()->where("ec_class = $model->class_id")->all();
            foreach ($modelexcercise as $excercise) {
                $shape = $excercise->ecExercise->excercise_shape;
                $muscle = $excercise->ecExercise->excercise_muscle;
                $excercise->ec_exercise = $excercise->ecExercise->excercise_description == 'N/A' ? $excercise->ec_old : $excercise->ecExercise->excercise_description;
                $excercise->ec_old = Sublistas::getName($shape);
                $excercise->ec_hability = Sublistas::getName($muscle);
            }
            return ['valid' => true, 'model' => $model, 'modelexcercise' => $modelexcercise];
        } else {
            return ['valid' => false, 'message' => 'Clase no existe. id' . \Yii::$app->request->post('class_id')];
        }
    }

    public function actionInfoclass() {
        $model = Clases::findOne(\Yii::$app->request->post('id_clase'));

        if (!empty($model)) {
            $modelexcercise = Excerciseclass::find()->where("ec_class = $model->class_id")->all();
            if (!empty($modelexcercise)) {
                foreach ($modelexcercise as $excercise) {
                    $shape = $excercise->ecExercise->excercise_shape;
                    $muscle = $excercise->ecExercise->excercise_muscle;
                    $excercise->ec_exercise = $excercise->ecExercise->excercise_description == 'N/A' ? $excercise->ec_old : $excercise->ecExercise->excercise_description;
                    $excercise->ec_old = Sublistas::getName($shape);
                    $excercise->ec_hability = Sublistas::getName($muscle);
                }
            }
            $time = 0;
            $modelasist = Classasist::find()->where("ca_class = $model->class_id")->andWhere('ca_hability NOT IN ("0")')->all();
            if ($model->class_hability == '3') {
                $fecha1 = new \DateTime($model->class_ini);

                $fecha2 = new \DateTime(date("H:i:s"));
                $intervalo = $fecha1->diff($fecha2);
                $hora = $intervalo->format('%H');
                $minuto = $intervalo->format('%i');
                $segundo = $intervalo->format('%s');
                $time = ['hora' => $hora, 'minuto' => $minuto, 'segundo' => $segundo];
            }
            $validatevaloration = Valorations::find()->where("valoration_class = $model->class_id")->one();
            if (!empty($validatevaloration)) {
                $valoration = $validatevaloration->valoration_id;
            } else {
                $valoration = 0;
            }
            if (\Yii::$app->request->post('info')) {
                return ['model' => $model, 'asist' => $modelasist, 'time' => $time, 'countrutinas' => count($modelexcercise), 'valoration' => $valoration];
            } else
            if (\Yii::$app->request->post('rutinas')) {
                if (!empty($modelexcercise)) {
                    return ['valid' => true, 'rutinas' => $modelexcercise];
                } else {
                    return ['valid' => false, 'message' => "Clase sin rutinas."];
                }
            }
        }
    }

    public function actionClassinit() {
        if (\Yii::$app->request->post('coach_id')) {
            $coach = \Yii::$app->request->post('coach_id');
            $model = Clases::find()->where("class_coach = $coach")->andWhere("class_hability IN ('3')")->one();
            if (!empty($model)) {
                $modelasist = Classasist::find()->where("ca_class = $model->class_id")->andWhere('ca_hability NOT IN ("0")')->all();
                return ['valid' => false, 'model' => $model, 'asist' => $modelasist];
            } else {
                return ['valid' => true];
            }
        } else
        if (\Yii::$app->request->post('customer_id')) {
            $customer = \Yii::$app->request->post('customer_id');
            $model = Clases::find()->where("class_custmer = $customer")->andWhere("class_hability IN ('4')")->one();
            if (!empty($model)) {
                return ['value' => '1', 'validate' => false, 'model' => $model, 'message' => 'Por favor realiza la calificación de tu última clase.'];
            }
            $model = Clases::find()->where("class_custmer = $customer")->andWhere("class_hability IN ('3')")->one();
            if (!empty($model)) {
                return ['value' => '2', 'valid' => false, 'model' => $model, 'message' => 'Estas en una clase, si la clase ya termino por favor comunicate con el instructor para que la finalice.'];
            }
            return ['valid' => true,];
        } else
        if (\Yii::$app->request->post('referer_id')) {
//            $referer = \Yii::$app->request->post('referer_id');
//            $model = Clases::find()->where("class_custmer = $customer")->andWhere("class_hability IN ('4')")->one();
//            if (!empty($model)) {
//                return ['value' => '1', 'validate' => false, 'model' => $model, 'message' => 'Por favor realiza la calificación de tu última clase.'];
//            }
//            $model = Clases::find()->where("class_custmer = $customer")->andWhere("class_hability IN ('3')")->one();
//            if (!empty($model)) {
//                return ['value' => '2', 'valid' => false, 'model' => $model, 'message' => 'Estas en una clase, si la clase ya termino por favor comunicate con el instructor para que la finalice.'];
//            }
            return ['valid' => true,];
        }
    }

    public function actionCalificaclass() {
        $model = new Ratings();
        if ($model->load(\Yii::$app->request->post())) {
            $var = false;
            $message = '';
            if (is_numeric($model->rating_puntuality) && $model->rating_puntuality >= 0 && $model->rating_puntuality <= 5) {
                $var = true;
            } else {
                $var = false;
                $message = 'El valor de Puntualidad debe ser númerico y menor o igual a 5.';
                return ['validate' => $var, 'message' => $message];
            }
            if (is_numeric($model->rating_presentation) && $model->rating_presentation >= 0 && $model->rating_presentation <= 5) {
                $var = true;
            } else {
                $var = false;
                $message = 'El valor de Presentación debe ser númerico y menor o igual a 5.';
                return ['validate' => $var, 'message' => $message];
            }
            if (is_numeric($model->rating_empathy) && $model->rating_empathy >= 0 && $model->rating_empathy <= 5) {
                $var = true;
            } else {
                $var = false;
                $message = 'El valor de Empatía debe ser númerico y menor o igual a 5.';
                return ['validate' => $var, 'message' => $message];
            }
            if (empty($model->rating_observations) || $model->rating_observations == '' || $model->rating_observations == null) {
                $var = false;
                $message = 'Por favor dejanos tus observaciones.';
                return ['validate' => $var, 'message' => $message];
            }
            if ($var) {
                if ($model->save()) {
                    $modelclass = Clases::findOne($model->rating_class);
                    $modelclass->class_hability = '5';
                    if ($modelclass->save()) {
                        $message = 'Calificación guardada';
                    }
                } else {
                    $var = false;
                    $message = \Yii::$app->request->post();
                }
            }

            return ['valid' => $var, 'message' => $message];
        }
    }

    public function actionChat() {
        $model = new Chats();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                $modelclass = Clases::findOne($model->chat_class);
                $modelgroup = Groups::find()->where("group_customer = $modelclass->class_custmer")
                                ->andWhere('group_hability NOT IN ("0")')->one();
                $group = [];
                if (!empty($modelgroup)) {
                    $group[] = [
                        'user' => $modelgroup->groupCustomer->customer_user,
                        'name' => $modelgroup->groupCustomer->customer_name
                    ];
                    $modelreferers = Refereers::find()->where("refereer_group = $modelgroup->group_id ")
                                    ->andWhere("refereer_hability NOT IN ('0','2')")->all();
                    if (!empty($modelreferers)) {
                        foreach ($modelreferers as $modelreferer) {
                            $group[] = [
                                'user' => $modelreferer->refereer_user,
                                'name' => $modelreferer->refereer_name
                            ];
                        }
                    }
                } else {
                    $group[] = [
                        'user' => $modelclass->classCustmer->customer_user,
                        'name' => $modelclass->classCustmer->customer_name
                    ];
                }
                $group[] = [
                    'user' => $modelclass->classCoach->coach_user,
                    'name' => $modelclass->classCoach->coach_name
                ];
                $pb = new PushBots();

                $tags = [];

                foreach ($group as $value) {
                    if ($value['user'] != $model->chat_user) {
                        $tags[] = $value['user'];
                    }
                }
                // Application ID
                $appID = '5bb7d76369b5ee14c2182d19';
                // Application Secret
                $appSecret = '8c5759086691aec735ec19843c3db64a';

                $pb->App($appID, $appSecret);

                // Notification Settings

                $msg = $model->chat_message;
                $nombre = Usuarios::getName($model->chat_user);
                // Custom fields - payload data
                $customfields = array(
                    "customNotificationTitle" => "Mensaje de $nombre",
                    "sound" => "default",
                    "url" => 'chat',
                    "nextActivity" => "com.ionicframework.minutes402441341",
                    "sIco" => "icon_notification",
                    'accColor' => '000000',
                    "lIco" => "icon_notification",
                    'accColor' => '099a8c',
                );

                if (!empty($tags)) {
                    $pb->Tags($tags);
                }

                $pb->Payload($customfields);
                $pb->Alert($msg);
                $pb->Platform(array("0", "1"));


                $pb->Push();
                return ['valid' => true, 'gorup' => $group];
            } else {
                return ['valid' => false, 'message' => $model->errors];
            }
        }
    }

}
