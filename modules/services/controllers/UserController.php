<?php

namespace app\modules\services\controllers;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;
use app\modules\admin\models\Usuarios;
use app\models\User;
use app\modules\customers\models\Customers;
use app\modules\coachs\models\Coachs;
use app\modules\plans\models\Plans;
use app\modules\plans\models\Conditions;
use app\modules\customers\models\Customersplans;
use app\modules\groups\models\Refereers;
use app\modules\groups\models\Groups;
use app\modules\admin\models\Sublistas;
use app\modules\admins\models\Admins;

class UserController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\admin\models\Usuarios";
    public $message = "Null Or Empty Model";
    public $path = "https://www.40minutes.com.co";

    //public $path = "http://localhost";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'encrypt' => ['GET', 'HEAD'],
            'decrypt' => ['GET', 'HEAD'],
            'create' => ['GET', 'HEAD'],
            'update' => ['PUT', 'PATCH'],
            'delete' => ['DELETE'],
            'validatetouser' => ['POST'],
            'usermodel' => ['POST'],
            'encriptarpass' => ['POST'],
            'updatephoto' => ['POST'],
            'forgotpass' => ['POST'],
            'validemail' => ['POST'],
            'valididentification' => ['POST'],
            'registercustomers' => ['POST'],
            'userphoto' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionValididentification() {
        $identification = Yii::$app->request->post('identification');
        $valid = true;
        $message = '';
        $validate = Customers::find()->where(['customer_identification' => $identification])->all();
        if (!is_numeric($identification)) {
            $valid = false;
            $message = 'La identifiación debe ser númerica.';
        } else
        if (strlen($identification) > 12 || strlen($identification) < 6) {
            $valid = false;
            $message = 'La identifiación debe tener como minímo 6 digítos y maxímo 12 digítos.';
        } else if (!empty($validate)) {
            $valid = false;
            $message = "El número de Identificación $identification ya se encuentra registrado.";
        } else {
            $valid = true;
            $message = '';
        }
        return ['valid' => $valid, 'message' => $message];
    }

    public function actionValidemail() {
        $email = Yii::$app->request->post('username');
        $valid = true;
        $message = '';
        $model = Usuarios::find()->where("username = '$email'")->one();
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $valid = false;
            $message = 'Email no válido.';
        } else
        if (!empty($model)) {
            $valid = false;
            $message = 'Email ya registrado.';
        }
        return ['valid' => $valid, 'message' => $message];
    }

    public function actionValidemailone() {
        $email = Yii::$app->request->post('email');
        $valid = true;
        $message = '';
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $valid = false;
            $message = 'Email no válido.';
        }
        return ['valid' => $valid, 'message' => $message];
    }

    public function actionValidatetouser() {
        date_default_timezone_set("America/bogota");
        $modeluser = new User();
        if (!empty(Yii::$app->request->post())) {
            $user = Yii::$app->request->post('username');
            $pass = $modeluser->encriptarPass(Yii::$app->request->post('password'));
            $model = Usuarios::find()->where("username = '$user'")->one();

            if (empty($model)) {
                return ["message" => "Usuario no registrado."];
            } else {
                if ($model->password != $pass) {
                    return ["message" => "Contraseña Invalida."];
                } else if ($model->user_hability == 2) {
                    return ["message" => "Usuario Inactivo."];
                } else {
                    $modelcustomer = Customers::find()->where("customer_user = $model->user_id")->one();
                    $modelcoach = Coachs::find()->where("coach_user = $model->user_id")->one();
                    $modelreferrer = Refereers::find()->where("refereer_user = $model->user_id")->one();
                    if (!empty($modelcustomer)) {

                        $model->last_login = date('Y-m-d H:i:s');
                        $model->save();
                        return [
                            "name" => $modelcustomer->customer_name,
                            "id_usuario" => $model->user_id,
                            "id_customer" => $modelcustomer->customer_id,
                            "valid" => true,
                            "type" => $model->type_user,
                        ];
                    } else
                    if (!empty($modelcoach)) {

                        //esto va en inicio
                        if ($modelcoach->coach_identification == null || $modelcoach->coach_identification == 0 || empty($modelcoach->coach_identification)) {
                            return ["actualizar" => '1', 'model' => $modelcoach];
                        } else if ($modelcoach->coach_celphone == null || $modelcoach->coach_celphone == 0 || empty($modelcoach->coach_celphone)) {
                            return ["actualizar" => '1', 'model' => $modelcoach];
                        }
                        $model->last_login = date('Y-m-d H:i:s');
                        $model->save();
                        $url = User::get_fcontent("$this->path/40minutes/images/avatars/" . $model->user_id . "_t.png");
                        $avatar = 'data:image/png;base64,' . base64_encode($url[0]);
                        return [
                            "name" => $modelcoach->coach_name,
                            "id_usuario" => $model->user_id,
                            "id_coach" => $modelcoach->coach_id,
                            "valid" => true,
                            "type" => $model->type_user,
                            'avatar' => $avatar
                        ];
                    } else
                    if (!empty($modelreferrer)) {
                        $model->last_login = date('Y-m-d H:i:s');
                        $model->save();
                        $url = User::get_fcontent("$this->path/40minutes/images/avatars/" . $model->user_id . "_t.png");
                        $avatar = 'data:image/png;base64,' . base64_encode($url[0]);
                        return [
                            "name" => $modelreferrer->refereer_name,
                            "id_usuario" => $model->user_id,
                            "id_referer" => $modelreferrer->refereer_id,
                            "valid" => true,
                            "type" => $model->type_user,
                            'avatar' => $avatar
                        ];
                    } else {
                        return ["message" => "User $this->message"];
                    }
                }
            }
        } else {
            return ["message" => "POST $this->message"];
        }
    }

    public function actionRegistercustomers() {
        $modeluser = new User();
        $modelusuario = new Usuarios();
        $modelcustomer = new Customers();
        $usuarios = Yii::$app->request->post('Version') == 3 ? ['Usuarios' => json_decode(Yii::$app->request->post('Usuarios'), true)] : Yii::$app->request->post();
        $customers = Yii::$app->request->post('Version') == 3 ? ['Customers' => json_decode(Yii::$app->request->post('Customers'), true)] : Yii::$app->request->post();
        $modelcustomer->load($customers);
        $modelusuario->load($usuarios);
        if (!empty(Yii::$app->request->post('Usuarios')) && !empty(Yii::$app->request->post('Customers'))) {
            $validateuser = $modelusuario->find()->where("username ='$modelusuario->username'")->one();
            if (empty($validateuser)) {

                $validateidentification = $modelcustomer->find()->where("customer_identification = $modelcustomer->customer_identification")->one();
                if (empty($validateidentification)) {
                    $modelusuario->password = $modelusuario->password != '' ? $modeluser->encriptarPass($modelusuario->password) : $modelusuario->password;
                    $modelusuario->type_user = 4;
                    if ($modelcustomer->customer_identification != '' &&
                            $modelcustomer->customer_name != '' &&
                            $modelcustomer->customer_address_one != '' &&
                            $modelusuario->save()) {

                        $modelcustomer->customer_user = $modelusuario->user_id;
                        $modelcustomer->customer_avatar = 'F01.png';
                        $modelcustomer->customer_plan_active = 1;
                        if ($modelcustomer->save()) {
//                            $content = "Hola " . $modelcustomer->customer_name . "<br><br> Bienvenido a 40minutes <br><br> Para activiar tu usuario por favor da click en el siguiente enlace:<br>";
//                            $user = $modeluser->encriptarPass($modelusuario->user_id);
//                            $content .= \yii\helpers\Html::a("Activación de usuario", "http://localhost/40minutes/customers/customers/activate?key=$user", ['class' => 'btn btn-primary']);
//                            $modeluser->mail("40minutes", $content, $modelusuario->username, "Registro 40 Minutes");
                            $url = User::get_fcontent("$this->path/40minutes/images/avatars/$modelcustomer->customer_gender/$modelcustomer->customer_avatar");
                            $avatar = 'data:image/png;base64,' . base64_encode($url[0]);
                            return ["message" => "Bienvenido a 40 Minutes.", "id_usuario" => $modelusuario->user_id, "id_customer" => $modelcustomer->customer_id, "valid" => true, 'avatar' => $avatar];
                        } else {
                            $auto = $modelusuario->user_id - 1;
                            $modelusuario->getTableSchema()->canSetProperty("AUTO_INCREMENT= $auto");
                            $modelusuario->findOne($modelusuario->user_id)->delete();
                            return ["message" => "Ocurrió un error durante el proceso, por favor intente nuevamente." . json_encode($modelcustomer->errors)];
                        }
                    } else {
                        return ["message" => "Ocurrió un error durante el proceso, por favor intente nuevamente." . json_encode($modelusuario->errors)];
                    }
                } else {
                    return ["message" => "El número de identificación ya ha sido registrado."];
                }
            } else {
                return ["message" => "El correo ya ha sido registrado."];
            }
        } else {
            return ["message" => "$this->message"];
        }
    }

    public function actionForgotpass() {
        $modeluser = new User();
        $id = Yii::$app->request->post('email');
        $modelusuario = Usuarios::find()->where("username = '$id'")->andWhere('user_hability NOT IN ("0")')->one();
        if (!empty($modelusuario)) {
            $valid = false;
            switch ($modelusuario->type_user) {
                case 2:
                    $model = Admins::find()->where("admin_user = $modelusuario->user_id")->one();
                    if (!empty($model)) {
                        $valid = true;
                        $nombre = $model->admin_name;
                    }
                    break;
                case 3;
                    $model = Coachs::find()->where("coach_user = $modelusuario->user_id")->one();
                    if (!empty($model)) {
                        $valid = true;
                        $nombre = $model->coach_name;
                    }
                    break;
                case 4:
                    $model = Customers::find()->where("customer_user = $modelusuario->user_id")->one();
                    if (!empty($model)) {
                        $valid = true;
                        $nombre = $model->customer_name;
                    }
                    break;
                case 18:
                    $model = Refereers::find()->where("refereer_user = $modelusuario->user_id")->one();
                    if (!empty($model)) {
                        $valid = true;
                        $nombre = $model->refereer_name;
                    }
                    break;
                default:
                    $valid = false;
            }
            if ($valid) {
                $pass = $modeluser->desencriptarPass($modelusuario->password);
                $content = "Hola " . $nombre . "<br><br> 40 Minutes te informa que tus datos de acceso actuales son: <br><br>
                
                 Usuario: $modelusuario->username<br>
                 Contraseña: $pass  
                  ";
                $modeluser->mail("40minutes", $content, Yii::$app->request->post('email'), "Forgot Your Password");
                return ["valid" => true, "message" => "Se a enviado un mensaje al correo registrado con la información de los datos de acceso del usuario."];
            } else {
                return ['valid' => false, 'message' => 'Correo no registrado. '];
            }
        } else {
            return ['valid' => false, 'message' => 'Correo no registrado.'];
        }
    }

    public function actionUsermodel() {
        if (!empty(Yii::$app->request->post())) {
            $user = Yii::$app->request->post('username');
            $modelusuario = Usuarios::find()->where("username = '$user'")->one();
            if (!empty($modelusuario)) {
                switch ($modelusuario->type_user) {
                    case 3:
                        $model = Coachs::find()->where("coach_user = $modelusuario->user_id")->one();
                        break;
                    case 4:
                        $model = Customers::find()->where("customer_user = $modelusuario->user_id")->one();
                        break;
                    default:
                        $model = null;
                        break;
                }
                if ($model == null || empty($modelusuario)) {
                    return ['model' => null, "message" => "$this->message"];
                } else {
                    return ['modeluser' => $modelusuario, 'model' => $model];
                }
            } else {
                return ["message" => "$this->message"];
            }
        } else {
            return ["message" => "$this->message"];
        }
    }

    public function actionEncriptarpass() {
        $pass = Yii::$app->request->post('pass');
        if (!empty($pass)) {
            $cryptKey = '40M1NUT3SW3B4pp';
            $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $pass, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
            return ['valid' => true, 'encode' => $qEncoded];
        } else {
            return ['valid' => false, 'message' => 'Vacio'];
        }
    }

    public function actionUpdatephoto() {
        ini_set('memory_limit', '512M');
        $user = Yii::$app->request->post('id_usuario');
        if (!empty($user)) {
            $model = Usuarios::findOne($user);
            if (!empty($model)) {
                $avatar = Yii::$app->request->post('avatar');
                // open the output file for writing
                $output_file = Yii::$app->basePath . "/images/avatars/$user.png";
                if (!is_file($output_file)) {
                    $output_file = Yii::$app->basePath . "/images/avatars/F01.png";
                }
                $output_file_t = Yii::$app->basePath . "/images/avatars/" . $user . "_t.png";


                $ifp = fopen($output_file, 'wbra+');
                $data = explode(',', $avatar);
                // we could add validation here with ensuring count( $data ) > 1
                fwrite($ifp, base64_decode($data[1]));
                // clean up the file resource
                fclose($ifp);

                $imagine = Image::getImagine()
                        ->open($output_file)
                        ->thumbnail(new Box(200, 200))
                        ->save($output_file_t, ['quality' => 90]);
                return ['valid' => true, 'message' => 'Foto cargada con éxito.'];
            } else {
                return ['valid' => false, 'message' => $this->message];
            }
        } else {
            return ['valid' => false, 'message' => 'Vacio'];
        }
    }

    public function actionUserphoto() {
        $user = Yii::$app->request->post('user');
        $url = User::get_fcontent("$this->path/40minutes/images/avatars/" . $user . "_t.png");
        $avatar = 'data:image/png;base64,' . base64_encode($url[0]);
        return $avatar;
    }

    public function actionContact() {

        $modeluser = new User();
        $content = 'Nombre de Usuario: ' . Yii::$app->request->post('nombre') . '<br><br>'
                . 'Email: ' . Yii::$app->request->post('email') . '<br><br>'
                . 'Télefono: ' . Yii::$app->request->post('telefono') . '<br><br>'
                . 'Ciudad: ' . Yii::$app->request->post('ciudad') . '<br><br>'
                . 'Comentarios: ' . Yii::$app->request->post('comentarios');
        $modeluser->mail("40minutes", $content, \Yii::$app->params['adminEmail'], "Contacto desde la app");
        return ['valid'=>true];
    }

    /* public function actionDesencriptarPass() {
      $pass = Yii::$app->request->post('pass');
      $cryptKey = '40M1NUT3SW3B4pp';
      $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($pass), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
      return( $qDecoded );
      } */
    
    public function actionAutocomplete(){
        $value = Yii::$app->request->post('value');
        $user= Yii::$app->request->post('user');
        $modeluser = new Usuarios();
        $array = [];
        foreach ($modeluser->find()->select('username')->where("type_user IN(3,18)")->andWhere("user_id NOT IN ($user)")->all() as $key => $value) {
            $array[] = $value['username'];
        }
        
        return $array;
    }
}
