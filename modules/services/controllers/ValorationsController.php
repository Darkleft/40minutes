<?php

namespace app\modules\services\controllers;

use Yii;
use app\modules\valorations\models\Valorations;
use app\modules\customers\models\Customers;
use app\modules\customers\models\Customersplans;
use app\modules\clases\models\Clases;
use app\models\User;

class ValorationsController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\valorations\models\Valorations";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'cita' => ['POST'],
            'myvalorations' => ['POST'],
            'save' => ['POST'],
            'validate' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionCita() {
        $model = new Valorations();

        $modeluser = new User();
        $val = false;
        if (!empty(Yii::$app->request->post('validate'))) {
            $customer = Yii::$app->request->post('id_customer');
            $validate = Customersplans::find()->where("cp_customer = $customer")->andWhere("cp_hability NOT IN (0)")->one();
            if (!empty($validate)) {
                $countvalorations = Valorations::find()->where("valoration_user = $customer")->andWhere("valoration_plan = $validate->cp_id")->all();
                if (!empty($countvalorations)) {
                    $count = count($countvalorations);
                    if ($count == $validate->cp_valorations) {
                        return ["valoration" => 'NO', "message" => "Valoración realizada en otra clase."];
                    } else {
                        return ["valoration" => 'SI', "message" => "Valoración no realizada."];
                    }
                } else {
                    return ["valoration" => 'SI', "message" => "Valoración no realizada."];
                }
            } else {
                return ["valoration" => 'NO', "message" => 'Para solicitar la valoración debe tener un plan activo.'];
            }
        } else
        if ($model->load(Yii::$app->request->post())) {
            $modelcustomers = Customers::findOne($model->valoration_user);
            $modelclass = Clases::find()->where("class_id = $model->valoration_class")
                            ->andWhere("class_custmer = $model->valoration_user")->one();
            if (empty($modelcustomers)) {
                return ["valoration" => 'NO', "message" => "Usuario no existe"];
            } else if (empty($modelclass)) {
                return ["valoration" => 'NO', "message" => "Clase no existe"];
            } else {
                if ($model->save()) {
                    $content = 'Su cita a sido solicitada.';
                    $user = User::findIdentity($modelcustomers->customer_user);
                    $modeluser->mail("40minutes", $content, $user->username, "Cita de Valoración 40 Minutes");
                    return ["valoration" => 'SI', "message" => "Cita de valoración guardada."];
                } else {
                    return ["valoration" => 'NO', "message" => "$model->errors"];
                }
            }
        } else {
            return ["valoration" => 'NO', "message" => $this->message];
        }
    }

    public function actionMyvalorations() {
        $iduser = Yii::$app->request->post('id_user');
        $model = Valorations::find()->where("valoration_user = $iduser")
                        ->andWhere("valoration_hability NOT IN ('0')")
                        ->orderBy("valoration_class desc")->all();
        if (!empty($model)) {
            $array = [];
            foreach ($model as $value) {
                $value->valoration_coach = $value->valorationCoach->coach_name;
                $value->valoration_class = $value->valorationClass->class_date;
            }


            return ['valid' => true, 'model' => $model, 'array' => $array];
        } else {
            return ['valid' => false, 'message' => 'No hay valoraciones para mostrar.'];
        }
    }

    public function actionSave() {
        $model = New Valorations();
        if ($model->load(Yii::$app->request->post())) {
            $cita = $model->find()->where("valoration_user = $model->valoration_user")->andWhere("valoration_class = $model->valoration_class")->one();
            if (!empty($cita)) {
                $cita->load(Yii::$app->request->post());
                $cita->valoration_hability = '2';
                if ($cita->save()) {
                    return ['valid' => true, 'model' => $cita, 'message' => 'Valoración guardada.' ];
                } else {
                    return ['valid' => false, 'message' => $cita->errors];
                }
            } else {
                $model->valoration_hability = '2';
                if ($model->save()) {
                    return ['valid' => true, 'model' => $model, 'message' => 'Valoración guardada.'];
                } else {
                    return ['valid' => false, 'message' => $model->errors];
                }
            }
        } else {
            return ['valid' => false, 'message' => 'No hay valoraciones para mostrar.'];
        }
    }
    public function actionValidate() {
        $model = New Valorations();
        $user = Yii::$app->request->post('id_user');
        $class = Yii::$app->request->post('id_class');
        $validate = $model->find()->where("valoration_user = $user")->andWhere("valoration_class = $class")->one();
        if (!empty($validate)) {
            return ['valid' => true, 'model' => $validate];
        } else {
            return ['valid' => false, 'message' => 'No hay valoraciones para mostrar.'];
        }
    }

}
