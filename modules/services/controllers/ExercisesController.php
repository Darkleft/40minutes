<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\services\controllers;

use app\modules\admin\models\Excercises;
use yii\helpers\ArrayHelper;

/**
 * Description of ExcercicesController
 *
 * @author jorge.izquierdo
 */
class ExercisesController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\admin\models\Excercises";
    public $message = "Null Or Empty Model";
    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'getexcercises' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionGetexcercises() {
        $muscle = \Yii::$app->request->post('muscle');
        $type = \Yii::$app->request->post('type');
        return ['valid'=>true, 'list'=>ArrayHelper::map(Excercises::find()
                ->where("excercise_muscle IN ($muscle)")
                ->andWhere("excercise_shape IN ($type)")
                ->andWhere("excercises_hability NOT IN ('0')")
                ->all(), "excercise_id", "excercise_description")]; 
    }

}
