<?php

namespace app\modules\services\controllers;

use Yii;
use app\modules\customers\models\Customers;
use app\modules\groups\models\Refereers;
use app\models\User;
use app\modules\admin\models\Usuarios;
use app\modules\groups\models\Groups;
use app\modules\customers\models\Customersplans;
use app\modules\plans\models\Plans;
use app\modules\admin\models\Sublistas;
use app\modules\clases\models\Clases;

class GroupsController extends \yii\rest\ActiveController {

    public $modelClass = "\app\modules\groups\models\Groups";
    public $message = "Null Or Empty Model";

    public static function allowedDomains() {
        return [
            '*', // star allows all domains
                //'http://test1.example.com',
                //'http://test2.example.com',
        ];
    }

    public function behaviors() {
        return array_merge(parent::behaviors(), [
            // For cross-domain AJAX request
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to domains:
                    'Origin' => static::allowedDomains(),
                    'Access-Control-Request-Method' => ['POST'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age' => 3600, // Cache (seconds)
                ],
            ],
        ]);
    }

    public function actions() {
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['view']);
        unset($actions['index']);
        unset($actions['delete']);
        return $actions;
    }

    protected function verbs() {
        return [
            'index' => ['PUT'],
            'new' => ['POST'],
            'finalizar' => ['POST'],
            'mygroup' => ['POST'],
        ];
    }

    public function actionIndex() {
        return ["message" => "$this->message"];
    }

    public function actionNew() {
        $modeluser = new User();
        $model = new Groups();
        $modelreferers = new Refereers();
        $message = '';
        $save = false;
        $modelcustomeradmin = Customers::findOne(Yii::$app->request->post("id_customer"));
        $referrers = Yii::$app->request->post('Version') == 3 ? ['Refereers' => json_decode(Yii::$app->request->post('Refereers'), true)] : Yii::$app->request->post();

        if ($modelreferers->load($referrers)) {
            if (!is_numeric($modelreferers->refereer_identification)) {
                return ['valid' => $save, 'message' => 'La identificación debe ser númerica', 'group' => $save ? $model->group_id : 0];
            }
            $save = true;
            $model->group_customer = Yii::$app->request->post("id_customer");
            $validategroup = Groups::find()->where("group_customer = $model->group_customer")->one();
            if (!empty($validategroup)) {
                $model = $validategroup;
            }
            $model->group_hability = '1';

            $user = Usuarios::find()->where("username ='$modelreferers->refereer_email'")->one();
            if (empty($user)) {
                $user = new Usuarios();
                $user->type_user = 18;
                $user->username = $modelreferers->refereer_email;
                $user->password = $modeluser->encriptarPass($modelreferers->refereer_identification);
                $user->user_hability = '1';
                if ($user->save()) {
                    $modelreferers->refereer_user = $user->user_id;
                }
            } else {
                if ($user->type_user == 2 || $user->type_user == 3) {
                    return ['valid' => false, 'message' => 'Este Correo no puede ser registrado como cliente'];
                }
                $modelreferers->refereer_user = $user->user_id;
            }

            $validatereferresr = Refereers::find()->where("refereer_identification = $modelreferers->refereer_identification")->andWhere("refereer_hability NOT IN ('0')")->one();
            if (!empty($validatereferresr)) {
                $modelreferers->refereer_hability = '1';
                $modelreferers->refereer_user = $validatereferresr->refereer_user;
                $modelreferers->refereer_email = $validatereferresr->refereerUser->username;
            }

            $modelcustomer = Customers::find()->where("customer_identification = $modelreferers->refereer_identification")->one();
            if (!empty($modelcustomer)) {
                $modelreferers->refereer_user = $modelcustomer->customer_user;
                $modelreferers->refereer_email = $modelcustomer->customerUser->username;
                $user->type_user = 18;
                if ($user->save()) {
                    
                }
            }
            $content = "Hola " . $modelreferers->refereer_name . "<br><br>Bienvenido a 40minutes <br><br>
                            Te han agregado a un nuevo grupo de 40 Minutes <br><br>Usuario:$newuser->username<br>Password:$modelreferers->refereer_identification";
//                                    $value = $modeluser->encriptarPass(date('Y-m-d H:i:s'));
//                                    $usuario = $modeluser->encriptarPass($newuser->user_id);
//                                    $content .= \yii\helpers\Html::a("Activación de usuario", "http://localhost/40minutes/activations/activate?key=$usuario&value=$value", ['class' => 'btn btn-primary']);

            if ($model->save()) {
                $modelreferers->refereer_group = $model->group_id;
                if ($modelreferers->save()) {
                    $save = true;
                    $modeluser->mail("40minutes", $content, $newuser->username, "Registro 40 Minutes");
                    $message = "Se a registrado el usuario.";
                } else {
                    $message = $modelreferers->errors;
                }
            } else {
                $message = $model->errors;
            }
        } else {
            $message = $this->message;
        }
        return ['valid' => $save, 'message' => $message, 'group' => $save ? $model->group_id : 0];
    }

    public function actionFinalizar() {
        $group_id = Yii::$app->request->post("id_group");
        $limit = Yii::$app->request->post("cantidad") - 1;
        $model = Groups::findOne($group_id);
        if (!empty($model)) {
            $model->group_hability = '1';
            if ($model->save()) {
                $modelreferers = Refereers::find()->where("refereer_group = $group_id ")
                                ->orderBy("refereer_id desc")->limit($limit)->all();
                foreach ($modelreferers as $modelreferer) {
                    $referer = Refereers::findOne($modelreferer->refereer_id);
                    $referer->refereer_hability = '1';
                    if ($referer->save()) {
                        $modeluser = Usuarios::findOne($referer->refereer_user);
                        $modeluser->user_hability = '1';
                        if ($modeluser->save()) {
                            $valid = true;
                            $message = "Registro Exitoso";
                        } else {
                            $valid = false;
                            $message = $modeluser->errors;
                        }
                    } else {
                        $valid = false;
                        $message = $referer->errors;
                    }
                }
            } else {
                $valid = false;
                $message = $model->errors;
            }
        } else {
            $valid = false;
            $message = $this->message;
        }
        return [
            "valid" => $valid,
            'message' => $message
        ];
    }

    public function actionMygroup() {
        if (Yii::$app->request->post("id_customer")) {
            $customer = Yii::$app->request->post("id_customer");
            $model = Groups::find()->where("group_customer = $customer")->one();
            $valid = false;
            $message = $this->message;
            if (!empty($model)) {
                $modelreferers = Refereers::find()->where("refereer_group = $model->group_id ")->andWhere("refereer_hability NOT IN ('0','2')")->all();
                if (!empty($modelreferers)) {
                    $message = $modelreferers;
                    $valid = true;
                }
            }
            return [
                "valid" => $valid,
                'message' => $message
            ];
        } else
        if (Yii::$app->request->post("id_referrer")) {
            $referrer = Yii::$app->request->post("id_user");
            $model = Refereers::find()->where("refereer_user = $referrer ")->one();
            if (!empty($model)) {

                $modelcustomer = Groups::findOne($model->refereer_group);
                $group[] = $modelcustomer->groupCustomer->customer_name;
                $modelreferers = Refereers::find()->where("refereer_group = $model->refereer_group ")->andWhere("refereer_hability NOT IN ('0','2')")->all();

                if (!empty($modelreferers)) {
                    foreach ($modelreferers as $modelreferer) {
                        $group[] = $modelreferer->refereer_name;
                    }
                }
                $valid = true;
                $message = $group;
            } else {
                $valid = false;
                $message = $this->message;
            }
            return [
                "valid" => $valid,
                'message' => $message
            ];
        } else
        if (Yii::$app->request->post("id_group")) {
            $modelgroup = Groups::findOne(Yii::$app->request->post("id_group"));
            $modelreferers = Refereers::find()->where("refereer_group = $modelgroup->group_id")->andWhere("refereer_hability NOT IN ('0','2')")->all();
            $group[] = $modelgroup->groupCustomer->customer_name;
            if (!empty($modelreferers)) {
                foreach ($modelreferers as $modelreferer) {
                    $group[] = $modelreferer->refereer_name;
                }
            }
            return [
                "valid" => true,
                'message' => $group
            ];
        }
    }

    public function actionIntegrantes() {
        $id_clase = Yii::$app->request->post("id_clase");
        $modelclass = Clases::findOne($id_clase);
        $model = Groups::find()->where("group_customer = $modelclass->class_custmer")
                        ->andWhere('group_hability NOT IN ("0")')->one();
        $group = [];
        if (!empty($model)) {
            $group[] = [
                'user' => $model->groupCustomer->customer_user,
                'name' => $model->groupCustomer->customer_name
            ];
            $modelreferers = Refereers::find()->where("refereer_group = $model->group_id ")
                            ->andWhere("refereer_hability NOT IN ('0','2')")->all();
            if (!empty($modelreferers)) {
                foreach ($modelreferers as $modelreferer) {
                    $group[] = [
                        'user' => $modelreferer->refereer_user,
                        'name' => $modelreferer->refereer_name
                    ];
                }
            }
            $valid = true;
            $message = $group;
        } else {
            $group[] = [
                'user' => $modelclass->classCustmer->customer_user,
                'name' => $modelclass->classCustmer->customer_name
            ];
            $valid = true;
            $message = $group;
        }
        return [
            "valid" => $valid,
            'message' => $message
        ];
    }

}
