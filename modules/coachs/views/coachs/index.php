<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use app\modules\admin\models\Sublistas;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\coachs\models\searchs\CoachsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entrenadores';
?>
<div class="coachs-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nuevo Entrenador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'export' => false,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'username',
                'value' => 'coachUser.username',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'pass',
                'value' => function($model) {
                    return '*******';
                },
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
                'filter' => false
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'coach_name',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'coach_identification',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'options' => ['maxlength' => 12],
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'coach_celphone',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'options' => ['maxlength' => 10],
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
            ],
//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'attribute' => 'coach_working_day',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
//                        'data' => Sublistas::getJornadas(),
//                        'displayValueConfig' => Sublistas::getJornadas(),
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
//                'filter' => Sublistas::getJornadas(),
//            ],
//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'attribute' => 'coach_range_ini',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
//                        'data' => Sublistas::getHours(),
//                        'displayValueConfig' => Sublistas::getHours(),
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
//                'filter' => Sublistas::getJornadas(),
//            ],
//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'attribute' => 'coach_range_end',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
//                        'data' => Sublistas::getHours(),
//                        'displayValueConfig' => Sublistas::getHours(),
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
//                'filter' => Sublistas::getJornadas(),
//            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'hability',
                'value' => function ($model) {
                    switch ($model->coachUser->user_hability) {
                        case 0:
                            $estado = 'Registrado';
                            break;
                        case 1:
                            $estado = 'Activo';
                            break;
                        case 2:
                            $estado = 'Inactivo';
                            break;
                        default :
                            $estado = 'Error';
                            break;
                    }
                    return $estado;
                },
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo'],
                        'displayValueConfig' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo'],
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
                'filter' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo']
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'header' => 'Acciones',
            ],
        ],
    ]);
    ?>
</div>
