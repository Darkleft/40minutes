<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coachs\models\Coachs */

$this->title = strtoupper($model->coach_name);
?>
<div class="coachs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'modeluser' => $modeluser,
        'modelhours1' => $modelhours1,
        'modelhours2' => $modelhours2,
        'modelhours3' => $modelhours3
    ])
    ?>

</div>
