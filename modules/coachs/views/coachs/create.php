<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\coachs\models\Coachs */

$this->title = 'Nuevo Entrenador';
?>
<div class="coachs-create">
    <?= Html::a('Atrás', Yii::$app->request->referrer, ['class'=>'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'modeluser' => $modeluser
    ])
    ?>

</div>
