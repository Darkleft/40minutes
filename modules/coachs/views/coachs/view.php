<?php

use yii\web\View;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\modules\admin\models\Sublistas;
use app\modules\admin\models\Usuarios;
use yii\widgets\ActiveForm;
use app\modules\coachs\models\CoachsHours;

/* @var $this yii\web\View */
/* @var $model app\modules\coachs\models\Coachs */

$this->title = $model->coach_name;
?>
<div class="coachs-view">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>


    <p>
        <?= Html::button('Información', ['class' => 'btn btn-primary active btn_div btn_info', 'value' => 'info']) ?>
        <?= Html::button('Horarios', ['class' => 'btn btn-primary btn_div btn_hours', 'value' => 'hours']) ?>
        <?= Html::button('Clases', ['class' => 'btn btn-primary btn_div btn_clases', 'value' => 'clases']) ?>
        <?= !empty($valoration) ? Html::button('Valoraciones', ['class' => 'btn btn-primary btn_div btn_valorations', 'value' => 'valorations']) : '' ?>
    </p>
    <br>
    <div class="div_tb info">
        <div class="col-lg-5">
            <fieldset>
                <legend>Datos</legend>
                <?=
                DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'coachUser.username',
                        'coach_name',
                        'coach_identification',
                        'coach_celphone',
                        [
                            'label' => 'Horario',
                            'value' => $model->coachWorkingDay->sublist_name,
                        ],
                        [
                            'attribute' => 'hability',
                            'value' => function ($model) {
                                switch ($model->coachUser->user_hability) {
                                    case 0:
                                        $estado = 'Registrado';
                                        break;
                                    case 1:
                                        $estado = 'Activo';
                                        break;
                                    case 2:
                                        $estado = 'Inactivo';
                                        break;
                                    default :
                                        $estado = 'Error';
                                        break;
                                }
                                return $estado;
                            },
                        ]
                    ],
                ])
                ?>
            </fieldset>
        </div>
        <div class="col-lg-5">
            <fieldset>
                <legend>Calificación</legend>
                <?=
                DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'label' => 'Puntualidad',
                            'value' => $calification['puntuality'] . " pts.",
                        ],
                        [
                            'label' => 'Empatía',
                            'value' => $calification['empathy'] . " pts.",
                        ],
                        [
                            'label' => 'Presentación',
                            'value' => $calification['presentation'] . " pts.",
                        ],
                        [
                            'label' => 'Total',
                            'format' => 'raw',
                            'value' => "<b>" . $calification['general'] . " pts.</b>",
                        ],
                    ],
                ])
                ?>
            </fieldset>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="div_tb hours">
        <legend>Horarios</legend>
        <div class="coachs-hours-form">

            <?php
            $dias = [0 => 'Domingo', 1 => 'Lunes', 2 => 'Martes', 3 => 'Miercoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sabado'];

            foreach ($dias as $key => $value) {
                ?>
                <div class="clearfix"></div>
                <h4><?= $value ?></h4>
                <?php
                foreach ([1, 2] as $number) {
                    $modelhours1 = CoachsHours::find()->where("hour_day = $key")->andWhere("hour_number = $number")->One();
                    if (empty($modelhours1)) {
                        $modelhours1 = new CoachsHours();
                    }
                    $modelhours1->hour_day = $key;
                    $modelhours1->hour_number = $number;
                    ?>
                    <?php
                    $form1 = ActiveForm::begin(['action' => $modelhours1->isNewRecord ?
                                ['createhorarios', 'id' => $model->coach_id, 'key' => $key] :
                                ["updatehorarios", 'id' => $modelhours1->hour_id, 'key' => $key, 'number' => $number]]);
                    ?>
                    <div class="col-lg-2">
                        <?= Html::activeHiddenInput($modelhours1, 'hour_day') ?>
                        <?= Html::activeHiddenInput($modelhours1, 'hour_number') ?>
                        <?= $form1->field($modelhours1, 'hour_ini')->dropDownList(Sublistas::getHours(), ['prompt' => 'Seleccione..']) ?>
                    </div>
                    <div class="col-lg-2">
                        <?= $form1->field($modelhours1, 'hour_end')->dropDownList(Sublistas::getHours(), ['prompt' => 'Seleccione..']) ?>
                    </div>
                    <div class="col-lg-2" style="padding-top: 2%">
                        <?= Html::activeCheckbox($modelhours1, 'hour_hability', [1 => 'Activo', 0 => 'Inactivo']) ?>
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>

                    <?php
                }
            }
            ?>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="div_tb clases hide">
        <legend>Clases</legend>
        <p>
            <?= Html::button('Asignadas', ['class' => 'btn btn-warning active btn_div_clases btn_asig', 'value' => 'asig']) ?>
            <?= Html::button('Finalizadas', ['class' => 'btn btn-warning btn_div_clases btn_end', 'value' => 'end']) ?>
            <?= Html::button('Canceladas', ['class' => 'btn btn-warning btn_div_clases btn_cancel', 'value' => 'cancel']) ?>
        </p>
        <div class="div_clases asig">
            <?=
            GridView::widget([
                'id' => 'grid_asig',
                'dataProvider' => $searchModel->searchClasesCoachAsig(Yii::$app->request->queryParams, "1", $model->coach_id),
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'customer',
                        'value' => 'classCustmer.customer_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases end hide">
            <?=
            GridView::widget([
                'id' => 'grid_end',
                'dataProvider' => $searchModel->searchClasesCoachAsig(Yii::$app->request->queryParams, "4, 5", $model->coach_id),
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'customer',
                        'value' => 'classCustmer.customer_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases cancel hide">
            <?=
            GridView::widget([
                'id' => 'grid_close',
                'dataProvider' => $searchModel->searchClasesCoachAsig(Yii::$app->request->queryParams, "2", $model->coach_id),
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'customer',
                        'value' => 'classCustmer.customer_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="div_tb valorations hide">
        <?php
        if (!empty($valoration)) {
            ?>
            <legend>Valoraciones</legend>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvidervaloratins,
                'filterModel' => $searchModelvalorations,
                'columns' => [
                    [
                        'attribute' => 'customer_name',
                        'value' => function($model) {
                            return Usuarios::getName($model->valoration_user);
                        }
                    ],
                    [
                        'attribute' => 'class_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->valorationClass->class_date, ['/clases/clases/view', 'id' => $model->valoration_class]);
                        }
                    ],
                    'valoration_weight',
                    'valoration_imc',
                    'valoration_grease',
                    'valoration_muscle',
                    'valoration_metabolism',
                    'valoration_cervical',
                    'valoration_age',
                //'valoration_hability',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        }
        ?>
    </div>
</div>
<?php
$this->registerJs("btndiv('" . Yii::$app->session->get('btn_div') . "');btndivclases('" . Yii::$app->session->get('btn_clases') . "')", View::POS_READY);
?>
