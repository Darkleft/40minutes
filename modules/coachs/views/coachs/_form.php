<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Sublistas;
use app\modules\coachs\models\CoachsHours;

/* @var $this yii\web\View */
/* @var $model app\modules\coachs\models\Coachs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coachs-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-2"><?= $form->field($modeluser, 'username')->textInput(['maxlength' => true]) ?> </div>
    <div class="col-lg-2"><?= $form->field($modeluser, 'password')->passwordInput(['maxlength' => true]) ?> </div>
    <div class="col-lg-2"><?= $form->field($model, 'coach_name')->textInput(['maxlength' => true]) ?> </div>
    <div class="col-lg-2"><?= $form->field($model, 'coach_identification')->textInput(['maxlength' => 12, 'onkeypress' => 'return solonum(event)']) ?> </div>
    <div class="col-lg-2"><?= $form->field($model, 'coach_celphone')->textInput(['maxlength' => 10, 'onkeypress' => 'return solonum(event)']) ?> </div>
    <!--<div class="col-lg-3"><?= $form->field($model, 'coach_working_day')->dropDownList(Sublistas::getJornadas(), ['prompt' => 'Seleccione una Jornada']) ?> </div>-->
    <div class="col-lg-2"><?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?> </div>
    <?php ActiveForm::end(); ?>
</div>