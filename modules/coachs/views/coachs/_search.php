<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\coachs\models\searchs\CoachsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coachs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'coach_id') ?>

    <?= $form->field($model, 'coach_user') ?>

    <?= $form->field($model, 'coach_name') ?>

    <?= $form->field($model, 'coach_identification') ?>

    <?= $form->field($model, 'coach_celphone') ?>

    <?php // echo $form->field($model, 'coach_working_day') ?>

    <?php // echo $form->field($model, 'coah_hability') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
