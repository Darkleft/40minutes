<?php

namespace app\modules\coachs\controllers;

use Yii;
use app\modules\coachs\models\Coachs;
use app\modules\coachs\models\searchs\CoachsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\admin\models\Usuarios;
use app\models\User;
use app\modules\clases\models\searchs\ClasesSearch;
use app\modules\ratings\models\Ratings;
use app\modules\valorations\models\Valorations;
use app\modules\valorations\models\searchs\ValorationsSearch;
use app\modules\coachs\models\CoachsHours;

/**
 * CoachsController implements the CRUD actions for Coachs model.
 */
class CoachsController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'editables', 'editableuser', 'delete', 'view', 'createhorarios', 'updatehorarios'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Coachs models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CoachsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('btn_div', 'info');
        Yii::$app->session->set('btn_clases', 'asig');
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Coachs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $model = $this->findModel($id);
        $modeluser = Usuarios::findOne($model->coach_user);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->coach_id]);
        }
        $searchModel = new ClasesSearch();
        $modelratings = Ratings::find()->join('join', 'class', 'rating_class = class_id')->andWhere("class_coach = $id")->all();
        $puntuality = 0;
        $empathy = 0;
        $presentation = 0;
        $general = 0;
        if (!empty($modelratings)) {
            foreach ($modelratings as $modelrating) {
                $puntuality = $puntuality + ($modelrating->rating_puntuality / 5);
                $empathy = $empathy + ($modelrating->rating_empathy / 5);
                $presentation = $presentation + ($modelrating->rating_presentation / 5);
            }
            $general = ($puntuality + $empathy + $presentation) / 3;
        }
        $searchModelvalorations = new ValorationsSearch();
        $dataProvidervaloratins = $searchModelvalorations->searchCoach(Yii::$app->request->queryParams, $id);

        $valoration = Valorations::find()->where("valoration_coach = $id")->orderBy("valoration_class desc")->limit(1)->one();
        $calification = ['puntuality' => $puntuality, 'empathy' => $empathy, 'presentation' => $presentation, 'general' => number_format($general, 2)];
        return $this->render('view', [
                    'model' => $model,
                    'modeluser' => $modeluser,
                    'searchModel' => $searchModel,
                    'calification' => $calification,
                    'valoration' => $valoration,
                    'dataProvidervaloratins' => $dataProvidervaloratins,
                    'searchModelvalorations' => $searchModelvalorations
        ]);
    }

    /**
     * Creates a new Coachs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Coachs();
        $modeluser = new Usuarios;
        $usermodel = new User();

        if ($model->load(Yii::$app->request->post()) && $modeluser->load(Yii::$app->request->post())) {
            $validate = Usuarios::find()->where("username = '$modeluser->username'")->one();
            if (empty($validate)) {
                $modeluser->type_user = 3;
                $modeluser->password = $usermodel->encriptarPass($modeluser->password);
                if ($modeluser->save()) {
                    $model->coach_user = $modeluser->user_id;
                    $model->coach_working_day = '1';
                    if ($model->save()) {
//                        $content = "Hola " . $model->coach_name . "<br><br> Bienvenido a 40minutes <br><br> Para activiar tu usuario por favor da click en el siguiente enlace:<br>";
//                        $user = $usermodel->encriptarPass($modeluser->user_id);
//                        $content .= \yii\helpers\Html::a("Activación de usuario", "http://localhost/40minutes/coachs/coachs/activate?key=$user", ['class' => 'btn btn-primary']);
//                        $validatemail = $usermodel->mail("40minutes", $content, Yii::$app->request->post('email'), "Registro 40 Minutes");
//                        if ($validatemail) {
//                            Yii::$app->getSession()->setFlash('success', "Se ha enviado un mensaje al correo $modeluser->username para la activación del usuario.");
//                        } else {
//                            Yii::$app->getSession()->setFlash('error', "El usuario ha sido registrado, sin embargo el correo $modeluser->username no es valido.");
//                        }
                        Yii::$app->getSession()->setFlash('warning', "Por favor registrar los Horarios.");
                        Yii::$app->session->set('btn_div', 'hours');
                        return $this->redirect(['view', 'id' => $model->coach_id]);
                    } else {
                        Yii::$app->getSession()->setFlash('error', "Ocurrió un error durante el proceso. " . json_encode($model->errors));
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', "Ocurrió un error durante el proceso. " . json_encode($modeluser->errors));
                }
            } else {
                Yii::$app->getSession()->setFlash('warning', "El correo $modeluser->username Ya está registrado.");
            }
        }

        return $this->render('create', [
                    'model' => $model,
                    'modeluser' => $modeluser
        ]);
    }

    /**
     * Updates an existing Coachs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionUpdate($id) {
//        $model = $this->findModel($id);
//        $modeluser = Usuarios::findOne($model->coach_user);
//        $modelhours1 = new CoachsHours();
//        $modelhours2 = new CoachsHours();
//        $modelhours3 = new CoachsHours();
//        $modelhours = CoachsHours::find()->where("hour_coach = $model->coach_id")->all();
//        if (!empty($modelhours)) {
//            $count = count($modelhours);
//            if ($count == 1) {
//                $modelhours1 = CoachsHours::findOne($modelhours[0]['hour_id']);
//            }
//            if ($count == 2) {
//                $modelhours1 = CoachsHours::findOne($modelhours[0]['hour_id']);
//                $modelhours2 = CoachsHours::findOne($modelhours[1]['hour_id']);
//            }
//            if ($count == 3) {
//                $modelhours1 = CoachsHours::findOne($modelhours[0]['hour_id']);
//                $modelhours2 = CoachsHours::findOne($modelhours[1]['hour_id']);
//                $modelhours3 = CoachsHours::findOne($modelhours[2]['hour_id']);
//            }
//        }
//
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['update', 'id' => $model->coach_id]);
//        }
//
//        return $this->render('update', [
//                    'model' => $model,
//                    'modeluser' => $modeluser,
//                    'modelhours1' => $modelhours1,
//                    'modelhours2' => $modelhours2,
//                    'modelhours3' => $modelhours3
//        ]);
//    }

    public function actionCreatehorarios($id, $key) {

        $modelhours = new CoachsHours();
        $save = true;
        if ($modelhours->load(Yii::$app->request->post())) {
        $modelhours->hour_coach = $id;
            $validate = CoachsHours::find()->where("hour_coach = $id")->andWhere("hour_day = $key")->all();
            if (!empty($validate)) {
                $count = count($validate);
                if ($count == 1) {
                    $modelhours1 = CoachsHours::findOne($validate[0]['hour_id']);
                    if ($modelhours->hour_ini <= $modelhours1->hour_end) {
                        Yii::$app->getSession()->setFlash('error', "Ya hay un horario con esta hora. ");
                        $save = false;
                    }
                }
                if ($count == 2) {
                    $modelhours1 = CoachsHours::findOne($validate[0]['hour_id']);
                    $modelhours2 = CoachsHours::findOne($validate[1]['hour_id']);
                    if ($modelhours->hour_ini <= $modelhours1->hour_end || $modelhours->hour_ini <= $modelhours2->hour_end) {
                        Yii::$app->getSession()->setFlash('error', "Ya hay un horario con la hora de inicio o fin. ");
                        $save = false;
                    }
                }
                if ($count == 3) {
                    Yii::$app->getSession()->setFlash('error', "Excede el número de horarios permitidos. ");
                    $save = false;
                }
            }
            if ($modelhours->hour_ini >= $modelhours->hour_end) {
                Yii::$app->getSession()->setFlash('error', "la hora de inicio debe ser menor que la hora de fin. ");
                $save = false;
            }
            if ($save) {
                Yii::$app->getSession()->setFlash('success', "Horario Actualizado. ");
                $modelhours->save();
            }
        }
        return $this->redirect(['view', 'id' => $modelhours->hour_coach]);
    }

    public function actionUpdatehorarios($id, $key, $number) {

        $modelhours = CoachsHours::findOne($id);
        $save = true;
        if ($modelhours->load(Yii::$app->request->post())) {
            $validate = CoachsHours::find()->where("hour_coach = $modelhours->hour_coach")->andWhere("hour_day = $key")->all();
            if (!empty($validate)) {
               echo  $count = count($validate);
                if ($count == 1) {
                    $modelhours1 = CoachsHours::findOne($validate[0]['hour_id']);
                    if ($modelhours->hour_ini <= $modelhours1->hour_end) {
                        Yii::$app->getSession()->setFlash('error', "Ya hay un horario con esta hora. ");
                        $save = false;
                    }
                }
                if ($count == 2) {
                    $modelhours1 = CoachsHours::findOne($validate[0]['hour_id']);
                    $modelhours2 = CoachsHours::findOne($validate[1]['hour_id']);
                    if (($modelhours->hour_ini <= $modelhours1->hour_end && $modelhours1->hour_id != $modelhours->hour_id) || ($modelhours->hour_ini <= $modelhours2->hour_end && $modelhours2->hour_id != $modelhours->hour_id)) {
                        Yii::$app->getSession()->setFlash('error', "Ya hay un horario con la hora de inicio o fin. ");
                        $save = false;
                    }
                }
                if ($count == 3) {
                    Yii::$app->getSession()->setFlash('error', "Excede el número de horarios permitidos. ");
                    $save = false;
                }
            }
            if ($modelhours->hour_ini >= $modelhours->hour_end) {
                Yii::$app->getSession()->setFlash('error', "la hora de inicio debe ser menor que la hora de fin. ");
                $save = false;
            }
            if ($save) {
                Yii::$app->getSession()->setFlash('success', "Horario Actualizado. ");
                $modelhours->save();
            }
        }
        return $this->redirect(['view', 'id' => $modelhours->hour_coach]);
    }

    /**
     * Deletes an existing Coachs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->coah_hability = "0";
        if ($model->save()) {
            $modeluser = Usuarios::findOne($model->coach_user);
            $modeluser->user_hability = "0";
            $modeluser->save();
        }

        return $this->redirect(['index']);
    }

    public function actionActivate($key) {
        $modeluser = new User();
        $key = str_replace(' ', '+', $key);
        $id = $modeluser->desencriptarPass($key);

        $model = Usuarios::findOne($id);
        $model->user_hability = "1";
        $model->save();
        Yii::$app->getSession()->setFlash('success', "Usuario $modeluser->username Activado.");

        return $this->redirect(['/site/index']);
    }

    /**
     * Finds the Coachs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Coachs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Coachs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionEditables() {
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $model = Coachs::findOne($id_editable);
            $attr = Yii::$app->request->post('editableAttribute');
            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Coachs']);
            $post['Coachs'] = $posted;
            $save = true;
            if ($attr == 'coach_range_ini') {
                $validate = date("H", strtotime($post['Coachs']['coach_range_ini'])) >= date("H", strtotime($model->coach_range_end));
                if ($validate) {
                    if (date("H", strtotime($post['Coachs']['coach_range_ini'])) != 21) {
                        $horafin = date("H", strtotime($post['Coachs']['coach_range_ini'])) + 1;
                        $model->coach_range_end = "$horafin:00:00";
                        $out = json_encode(['output' => '', 'message' => 'La hora de inicio no puede ser igual ni mayor a la hora final']);
                    } else {
                        $save = false;
                        $out = json_encode(['output' => '0', 'message' => 'La hora de inicio no puede ser igual ni mayor a la hora final']);
                    }
                }
            }
            if ($attr == 'coach_range_end') {
                $validate = date("H", strtotime($post['Coachs']['coach_range_end'])) <= date("H", strtotime($model->coach_range_ini));
                if ($validate) {
                    if (date("H", strtotime($post['Coachs']['coach_range_end'])) != 04) {
                        $horaini = date("H", strtotime($post['Coachs']['coach_range_end'])) - 1;
                        $model->coach_range_ini = "$horaini:00:00";
                        $out = json_encode(['output' => '', 'message' => 'La hora de fin no puede ser igual ni menor a la hora inicial']);
                    } else {
                        $save = false;
                        $out = json_encode(['output' => '0', 'message' => 'La hora de fin no puede ser igual ni menor a la hora inicial']);
                    }
                }
            }
            if ($attr == 'coach_identification') {
                $validate = $model->find()->where("coach_identification = '" . $post['Coachs']['coach_identification'] . "'")->one();
                if (!empty($validate)) {
                    $save = false;
                    $out = json_encode(['output' => '0', 'message' => 'El número de identificación ya esta registrado.']);
                }
            }
            if ($model->load($post) && $save) {
                if (!$model->save()) {
                    $out = json_encode(['output' => '0', 'message' => $model->errors]);
                }
            }
            echo $out;
            return;
        }
    }

    public function actionEditableuser() {
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $user = new User();
            $model = Coachs::findOne($id_editable);
            $modelUser = Usuarios::findOne($model->coach_user);
            $attr = Yii::$app->request->post('editableAttribute');

            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Coachs']);
            $post['Coachs'] = $posted;
            $save = true;
            if ($attr == 'pass') {
                $modelUser->password = $user->encriptarPass($post['Coachs']['pass']);
            }
            if ($attr == 'username') {
                $validate = $modelUser->find()->where("username = '" . $post['Coachs']['username'] . "'")->one();
                if (!empty($validate)) {
                    $save = false;
                    $out = json_encode(['output' => '0', 'message' => 'El correo ya esta registrado.']);
                } else {
                    $modelUser->username = $post['Coachs']['username'];
                    $content = "Hola $model->coach_name,<br><br> Bienvenido a 40minutes <br><br> Para activiar tu usuario por favor da click en el siguiente enlace:<br>";
                    $usuario = $user->encriptarPass($modelUser->user_id);
                    $content .= \yii\helpers\Html::a("Activación de usuario", "http://localhost/40minutes/customers/customers/activate?key=$usuario", ['class' => 'btn btn-primary']);
                    $user->mail("40minutes", $content, Yii::$app->request->post('email'), "Registro 40 Minutes");
                }
            }
            if ($save) {
                if (!$modelUser->save()) {
                    $out = json_encode(['output' => '0', 'message' => $modelUser->errors]);
                }
            }
//            print_r();

            echo $out;
            return;
        }
    }

}
