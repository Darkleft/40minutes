<?php

namespace app\modules\coachs\models;

use Yii;

/**
 * This is the model class for table "coachs_hours".
 *
 * @property int $hour_id
 * @property int $hour_coach
 * @property string $hour_day
 * @property string $hour_ini
 * @property string $hour_end
 * @property string $hour_hability
 * @property string $hour_number
 *
 * @property Coachs $hourCoach
 */
class CoachsHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'coachs_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hour_coach', 'hour_ini', 'hour_end', 'hour_day', 'hour_number'], 'required'],
            [['hour_coach'], 'integer'],
            [['hour_ini', 'hour_end'], 'safe'],
            [['hour_hability','hour_day', 'hour_number'], 'string', 'max' => 1],
            [['hour_coach'], 'exist', 'skipOnError' => true, 'targetClass' => Coachs::className(), 'targetAttribute' => ['hour_coach' => 'coach_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'hour_id' => 'Hour ID',
            'hour_coach' => 'Entrenador',
            'hour_day' => 'Día',
            'hour_ini' => 'Hora de Inicio',
            'hour_end' => 'Hora Fin',
            'hour_hability' => 'Activo',
            'hour_number'=>'Número de Horario'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHourCoach()
    {
        return $this->hasOne(Coachs::className(), ['coach_id' => 'hour_coach']);
    }
}
