<?php

namespace app\modules\coachs\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Usuarios;
use app\modules\admin\models\Sublistas;

/**
 * This is the model class for table "coachs".
 *
 * @property int $coach_id
 * @property int $coach_user
 * @property string $coach_name
 * @property string $coach_identification
 * @property string $coach_celphone
 * @property int $coach_working_day
 * @property string $coach_range_ini
 * @property string $coach_range_end
 * @property string $coah_hability
 *
 * @property Class[] $classes
 * @property Sublists $coachWorkingDay
 * @property Users $coachUser
 * @property Valorations[] $valorations
 */
class Coachs extends \yii\db\ActiveRecord {

    public $username;
    public $pass;
    public $hability;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'coachs';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['coach_user', 'coach_name', 'coach_working_day'], 'required'],
            [['coach_user', 'coach_identification', 'coach_working_day'], 'integer'],
            [['coach_range_ini', 'coach_range_end'], 'safe'],
            [['coach_name'], 'string', 'max' => 100],
            [['coach_celphone'], 'string', 'max' => 45],
            [['coah_hability'], 'string', 'max' => 1],
            [['coach_working_day'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['coach_working_day' => 'sublist_id']],
            [['coach_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['coach_user' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'coach_id' => 'ID',
            'coach_user' => 'Usuario',
            'coach_name' => 'Nombre',
            'coach_identification' => 'Identificación',
            'coach_celphone' => 'Celular',
            'coach_working_day' => 'Horario',
            'coach_range_ini' => 'Rango de Inicio',
            'coach_range_end' => 'Rango Final',
            'coah_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses() {
        return $this->hasMany(Clases::className(), ['class_coach' => 'coach_id']);
    }

    public function getCoachUser() {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'coach_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachWorkingDay() {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'coach_working_day']);
    }

    public function getList() {
        return ArrayHelper::map(Coachs::find()->where("coah_hability NOT IN ('0')")->all(), 'coach_id', 'coach_name');
    }

    public function getName($user) {
        return Coachs::find()->where(['coach_user' => $user])->one()->coach_name;
    }

    public function getNamecoach($id) {
        return Coachs::findOne($id)->coach_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValorations() {
        return $this->hasMany(Valorations::className(), ['valoration_coach' => 'coach_id']);
    }

    public function getDisponible($date) {
        $explode = explode(' ', $date);
        $fecha = $explode[0];
        $hora = $explode[1];
        $query = Coachs::find()->where("coah_hability NOT IN ('0')")
                ->join('join','coachs_hours', 'hour_coach = coach_id')
                ->andOncondition("hour_day = DAYOFWEEK('$fecha')-1")
                ->andOncondition('hour_hability NOT IN ("0")')
                ->andOncondition("'$hora' BETWEEN hour_ini AND hour_end")
                ->join('join', 'class', 'coach_id=class_coach')->andOncondition("class_date NOT IN  ('$date')")
                ->all();
        return ArrayHelper::map($query, 'coach_id', 'coach_name');
    }

}
