<?php

namespace app\modules\coachs\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\coachs\models\Coachs;
use app\modules\clases\models\Clases;

/**
 * CoachsSearch represents the model behind the search form of `app\modules\coachs\models\Coachs`.
 */
class CoachsSearch extends Coachs
{
    
    public $username;
    public $pass;
    public $hability;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coach_id', 'coach_user', 'coach_identification', 'coach_working_day'], 'integer'],
            [['coach_name', 'coach_celphone', 'coah_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Coachs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'coach_id' => $this->coach_id,
            'coach_user' => $this->coach_user,
            'coach_identification' => $this->coach_identification,
            'coach_working_day' => $this->coach_working_day,
        ]);

        $query->andFilterWhere(['like', 'coach_name', $this->coach_name])
            ->andFilterWhere(['like', 'coach_celphone', $this->coach_celphone])
            ->andFilterWhere(['like', 'coah_hability', $this->coah_hability]);

        return $dataProvider;
    }
    
   
}
