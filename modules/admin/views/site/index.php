<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
s?>

<div class="admin-default-index text-center">
    <h1>Bienvenido al Administrador</h1>


    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/admins.png", ['style' => 'border-radius: 5px']), ['/admins/admins/index']) ?><br>
        <span><b>Administradores</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/hours.png", ['style' => 'border-radius: 5px']), ['horarios']) ?><br>
        <span><b>Horario Clases</b></span>
    </div>
</div>
