<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="col-lg-6 text-center">
    <h3>Hora para Solicitud de Clase</h3>

    <?php $form = ActiveForm::begin(['action' => ['solicitaclass', 'id' => $modelsolicit->sublist_id]]); ?>

    <?=
    Html::ActiveDropDownList($modelsolicit, 'sublist_name', $modelsolicit->getHours2(), ['class' => 'form-control'])
    ?>

    <div style="text-align: center">
        <label> </label><br>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<div class="col-lg-6 text-center">
    <h3>Hora para Cancelación de Clase</h3>
    <?php $form2 = ActiveForm::begin(['action' => ['cancelclass', 'id' => $modelcancel->sublist_id]]); ?>

    <?=
    Html::ActiveDropDownList($modelcancel, 'sublist_name', $modelcancel->getHours2(), ['class' => 'form-control'])
    ?>

    <div style="text-align: center">
        <label> </label><br>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success submit']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>