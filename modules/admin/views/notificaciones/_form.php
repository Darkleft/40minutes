<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\models\Sublistas;

/* @var $this yii\web\View */
/* @var $model app\models\Notificaciones */
/* @var $form yii\widgets\ActiveForm */


$activities = array(
    'dashboard' => 'Inicio',
);
?>

<div class="notificaciones-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-lg-4 col-xs-6"><?= $form->field($model, 'id_tipo')->dropDownList(Sublistas::getNotificaciones(), ['prompt'=>'Seleccione']);?></div>
    <div class="col-lg-4 col-xs-6"> <?= $form->field($model, 'url')->dropDownList(
            $activities,           // Flat array ('id'=>'label')
            ['prompt'=>'Seleccione']    // options
        );?></div>
    <div class="clearfix"></div>
    
    <div class="col-lg-12">
        <?= $form->field($model, 'mensaje')->textarea(['rows' => 6]) ?>
    </div>
   
    <div class="col-lg-12">
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Crear' : 'Actualizar', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
</div>
    <?php ActiveForm::end(); ?>

</div>
