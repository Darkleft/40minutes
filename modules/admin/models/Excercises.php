<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\Sublistas;

/**
 * This is the model class for table "excercises".
 *
 * @property int $excercise_id
 * @property int $excercise_muscle musculo a ejercitar
 * @property int $excercise_shape Forma del ejercicio
 * @property string $excercise_description
 * @property string $excercises_hability
 *
 * @property ExcerciseClass[] $excerciseClasses
 * @property Sublists $excerciseMuscle
 * @property Sublists $excerciseShape
 */
class Excercises extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'excercises';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['excercise_muscle', 'excercise_shape', 'excercise_description'], 'required'],
            [['excercise_muscle', 'excercise_shape'], 'integer'],
            [['excercise_description'], 'string'],
            [['excercises_hability'], 'string', 'max' => 1],
            [['excercise_muscle'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['excercise_muscle' => 'sublist_id']],
            [['excercise_shape'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['excercise_shape' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'excercise_id' => 'Excercise ID',
            'excercise_muscle' => 'Musculo Trabajado',
            'excercise_shape' => 'Tipo de Ejercicio',
            'excercise_description' => 'Ejercicio',
            'excercises_hability' => 'Excercises Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExcerciseClasses()
    {
        return $this->hasMany(Excerciseclass::className(), ['ec_exercise' => 'excercise_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExcerciseMuscle()
    {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'excercise_muscle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExcerciseShape()
    {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'excercise_shape']);
    }
}
