<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\customers\models\Customers;
use app\modules\coachs\models\Coachs;
use app\modules\groups\models\Refereers;
use app\modules\admins\models\Admins;

/**
 * This is the model class for table "users".
 *
 * @property int $user_id
 * @property int $type_user
 * @property string $username
 * @property string $password
 * @property string $create_at
 * @property string $last_login
 * @property string $ip_loged
 * @property string $user_hability
 *
 * @property Admin[] $admins
 * @property Coachs[] $coachs
 * @property Customers[] $customers
 * @property Sublists $typeUser
 */
class Usuarios extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['type_user', 'username', 'password'], 'required'],
            [['type_user'], 'integer'],
            [['create_at', 'last_login'], 'safe'],
            [['username', 'password'], 'string', 'max' => 100],
            [['ip_loged'], 'string', 'max' => 45],
            [['user_hability'], 'string', 'max' => 1],
            [['username'], 'unique'],
            [['username'], 'email'],
            [['type_user'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['type_user' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'user_id' => 'User ID',
            'type_user' => 'Type User',
            'username' => 'Username',
            'password' => 'Password',
            'create_at' => 'Create At',
            'last_login' => 'Last Login',
            'ip_loged' => 'Ip Loged',
            'user_hability' => 'User Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins() {
        return $this->hasMany(Admin::className(), ['admin_user' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoachs() {
        return $this->hasMany(Coachs::className(), ['coach_user' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers() {
        return $this->hasMany(Customers::className(), ['customer_user' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeUser() {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'type_user']);
    }

    public function getData($id = null) {

        if ($id == NULL) {
            $data = Usuarios::find()->where('user_hability NOT IN ("0")')->all();
        } else {
            $data = Usuarios::findOne($id);
        }
        return $data;
    }
    public function getName($id) {
        
        $typeuser = Usuarios::findOne($id);
        
        if (!empty($typeuser)) {
            if ($typeuser->type_user == 2) {
                return Admins::find()->where("admin_user = $id")->one()->admin_name;
            }else
            if ($typeuser->type_user == 3) {
                return Coachs::find()->where("coach_user = $id")->one()->coach_name;
            }else
            if ($typeuser->type_user == 4) {
                return Customers::find()->where("customer_user = $id")->one()->customer_name;
            }else
            if ($typeuser->type_user == 18) {
                return Refereers::find()->where("refereer_user")->one()->refereer_name;
            }
        }
        
    }

}
