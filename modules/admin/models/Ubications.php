<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "ubications".
 *
 * @property int $id
 * @property int $id_user
 * @property string $long
 * @property string $lat
 *
 * @property Users $user
 */
class Ubications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ubications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'long', 'lat'], 'required'],
            [['id_user'], 'integer'],
            [['long', 'lat'], 'string', 'max' => 45],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['id_user' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'long' => 'Long',
            'lat' => 'Lat',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'id_user']);
    }
}
