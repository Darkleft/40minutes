<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "notificaciones".
 *
 * @property int $id
 * @property int $id_tipo
 * @property string $mensaje
 * @property string $url
 * @property int $target
 */
class Notificaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notificaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tipo', 'mensaje', 'url'], 'required'],
            [['id_tipo', 'target'], 'integer'],
            [['mensaje'], 'string'],
            [['url'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_tipo' => 'Tipo de Notificación',
            'mensaje' => 'Mensaje',
            'url' => 'Url',
            'target' => 'Target',
        ];
    }
}
