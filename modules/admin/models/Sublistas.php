<?php

namespace app\modules\admin\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "sublists".
 *
 * @property int $sublist_id
 * @property int $sublist_list
 * @property string $sublist_name
 * @property string $sublist_hability
 *
 * @property Customers[] $customers
 * @property Lists $sublistList
 * @property Users[] $users
 */
class Sublistas extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'sublists';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['sublist_list', 'sublist_name'], 'required'],
            [['sublist_list'], 'integer'],
            [['sublist_name'], 'string', 'max' => 100],
            [['sublist_hability'], 'string', 'max' => 1],
            [['sublist_list'], 'exist', 'skipOnError' => true, 'targetClass' => Listas::className(), 'targetAttribute' => ['sublist_list' => 'list_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'sublist_id' => 'Sublist ID',
            'sublist_list' => 'Sublist List',
            'sublist_name' => 'Sublist Name',
            'sublist_hability' => 'Sublist Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers() {
        return $this->hasMany(Customers::className(), ['customer_gender' => 'sublist_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSublistList() {
        return $this->hasOne(Lists::className(), ['list_id' => 'sublist_list']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers() {
        return $this->hasMany(Users::className(), ['type_user' => 'sublist_id']);
    }

    public function getName($id) {
        return Sublistas::findOne($id)->sublist_name;
    }

    public function getTypeuser() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (1,2)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getGender() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (1,3)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getJornadas() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (7)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getTypeclass() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (1,4)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getTypeplan() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (1,6)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getZones() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (5)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getNotificaciones() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (11)")->all(), 'sublist_id', 'sublist_name');
    }

    public function getHours($minus = null) {
        $hours = [
            ["id" => "04:00:00", "value" => "4:00am"],
            ["id" => "05:00:00", "value" => "5:00am"],
            ["id" => "06:00:00", "value" => "6:00am"],
            ["id" => "07:00:00", "value" => "7:00am"],
            ["id" => "08:00:00", "value" => "8:00am"],
            ["id" => "09:00:00", "value" => "9:00am"],
            ["id" => "10:00:00", "value" => "10:00am"],
            ["id" => "11:00:00", "value" => "11:00am"],
            ["id" => "12:00:00", "value" => "12:00m"],
            ["id" => "13:00:00", "value" => "01:00pm"],
            ["id" => "14:00:00", "value" => "02:00pm"],
            ["id" => "15:00:00", "value" => "03:00pm"],
            ["id" => "16:00:00", "value" => "04:00pm"],
            ["id" => "17:00:00", "value" => "05:00pm"],
            ["id" => "18:00:00", "value" => "06:00pm"],
            ["id" => "19:00:00", "value" => "07:00pm"],
            ["id" => "20:00:00", "value" => "08:00pm"],
            ["id" => "21:00:00", "value" => "09:00pm"]
        ];
        return ArrayHelper::map($hours, 'id', 'value');
    }
    public function getHours2() {
        $hours = [
            ["id" => "04", "value" => "4:00am"],
            ["id" => "05", "value" => "5:00am"],
            ["id" => "06", "value" => "6:00am"],
            ["id" => "07", "value" => "7:00am"],
            ["id" => "08", "value" => "8:00am"],
            ["id" => "09", "value" => "9:00am"],
            ["id" => "10", "value" => "10:00am"],
            ["id" => "11", "value" => "11:00am"],
            ["id" => "12", "value" => "12:00am"],
            ["id" => "13", "value" => "01:00pm"],
            ["id" => "14", "value" => "02:00pm"],
            ["id" => "15", "value" => "03:00pm"],
            ["id" => "16", "value" => "04:00pm"],
            ["id" => "17", "value" => "05:00pm"],
            ["id" => "18", "value" => "06:00pm"],
            ["id" => "19", "value" => "07:00pm"],
            ["id" => "20", "value" => "08:00pm"],
            ["id" => "21", "value" => "09:00pm"],
            ["id" => "22", "value" => "10:00pm"],
            ["id" => "23", "value" => "11:00pm"]
        ];
        return ArrayHelper::map($hours, 'id', 'value');
    }

    public function getHoursclass() {
        $hours = [
        ["id" => "04:00:00", "value" => "4:00am a 4:45am"],
        ["id" => "04:30:00", "value" => "4:30am a 5:15am"],
        ["id" => "05:00:00", "value" => "5:00am a 5:45am"],
        ["id" => "05:30:00", "value" => "5:30am a 6:15am"],
        ["id" => "06:00:00", "value" => "6:00am a 6:45am"],
        ["id" => "06:30:00", "value" => "6:30am a 7:15am"],
        ["id" => "07:00:00", "value" => "7:00am a 7:45am"],
        ["id" => "07:30:00", "value" => "7:30am a 8:15am"],
        ["id" => "08:00:00", "value" => "8:00am a 8:45am"],
        ["id" => "08:30:00", "value" => "8:30am a 9:15am"],
        ["id" => "09:00:00", "value" => "9:00am a 9:45am"],
        ["id" => "09:30:00", "value" => "9:30am a 10:15am"],
        ["id" => "10:00:00", "value" => "10:00am a 10:45am"],
        ["id" => "15:00:00", "value" => "3:00pm a 3:45pm"],
        ["id" => "15:30:00", "value" => "3:30pm a 4:15pm"],
        ["id" => "16:00:00", "value" => "4:00pm a 4:45pm"],
        ["id" => "16:30:00", "value" => "4:30pm a 5:15pm"],
        ["id" => "17:00:00", "value" => "5:00pm a 5:45pm"],
        ["id" => "17:30:00", "value" => "5:30pm a 6:15pm"],
        ["id" => "18:00:00", "value" => "6:00pm a 6:45pm"],
        ["id" => "18:30:00", "value" => "6:30pm a 7:15pm"],
        ["id" => "19:00:00", "value" => "7:00pm a 7:45pm"],
        ["id" => "19:30:00", "value" => "7:30pm a 8:15pm"],
        ["id" => "20:00:00", "value" => "8:00pm a 8:45pm"],
        ["id" => "20:30:00", "value" => "8:30pm a 9:15pm"],
        ["id" => "21:00:00", "value" => "9:00pm a 9:45pm"]];
        return ArrayHelper::map($hours, 'id', 'value');
    }
    
     public function getCupons() {
        return ArrayHelper::map(Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (12)")->all(), 'sublist_id', 'sublist_name');
    }
    
    public static function getHourcancel(){
        return Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (13)")->one()->sublist_name;
    }
    public static function getHoursolicit(){
        return Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (14)")->one()->sublist_name;
    }

}
