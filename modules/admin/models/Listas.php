<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "lists".
 *
 * @property int $list_id
 * @property string $list_name
 * @property string $list_hability
 *
 * @property Sublists[] $sublists
 */
class Listas extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lists';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['list_name'], 'required'],
            [['list_name'], 'string', 'max' => 100],
            [['list_hability'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'list_id' => 'List ID',
            'list_name' => 'List Name',
            'list_hability' => 'List Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSublists()
    {
        return $this->hasMany(Sublists::className(), ['sublist_list' => 'list_id']);
    }
    
    public function getName($id) {
        return Listas::findOne($id)->list_name;
    }
}
