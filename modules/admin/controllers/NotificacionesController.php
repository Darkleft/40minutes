<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\Notificaciones;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\PushBots;
use yii\filters\AccessControl;

/**
 * NotificacionesController implements the CRUD actions for Notificaciones model.
 */
class NotificacionesController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout', 'create'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (\Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = ['/site/index'];
                    }
                    \Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Creates a new Notificaciones model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Notificaciones();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {


            //Yii::import('', true);

            $pb = new PushBots();

            $tags = [];

            if ($model->id_tipo == 31) {
                $tags[] = "entrenador";
            } else if ($model->id_tipo == 32) {
                $tags[] = "cliente";
            }

            // Application ID
            $appID = '5bb7d76369b5ee14c2182d19';
            // Application Secret
            $appSecret = '8c5759086691aec735ec19843c3db64a';

            $pb->App($appID, $appSecret);

            // Notification Settings

            $msg = $model->mensaje;

            // Custom fields - payload data
            $customfields = array(
                "customNotificationTitle" => "40minutes",
                "sound" => "default",
                "url" => $model->url,
                "nextActivity" => "com.ionicframework.minutes402441341",
                'accColor' => '099a8c',
            );

            if (!empty($tags)) {
                $pb->Tags($tags);
            }

            $pb->Payload($customfields);
            $pb->Alert($msg);
            $pb->Platform(array("0", "1"));


            $pb->Push();
            \Yii::$app->getSession()->setFlash('success', 'Notificación enviada.');
            return $this->redirect(['create']);
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Finds the Notificaciones model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Notificaciones the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Notificaciones::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
