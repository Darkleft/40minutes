<?php

namespace app\modules\admin\controllers;

use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use app\modules\admin\models\Sublistas;

/**
 * Default controller for the `admin` module
 */
class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'horarios', 'cancelclass', 'solicitaclass'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = ['/site/index'];
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionHorarios() {
        $modelcancel = Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (13)")->one();
        $modelsolicit = Sublistas::find()->where('sublist_hability NOT IN ("0")')->andWhere("sublist_list IN (14)")->one();

        return $this->render('horarios', [
                    'modelcancel' => $modelcancel,
                    'modelsolicit' => $modelsolicit
        ]);
    }
    
    public function  actionCancelclass($id){
        $model = Sublistas::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Hora para cancelación Guardada.');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($model->errors));
            }
        }
        return $this->redirect(['horarios']);
    }
    public function  actionSolicitaclass($id){
        $model = Sublistas::findOne($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->getSession()->setFlash('success', 'Hora para solicitud Guardada.');
            } else {
                Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($model->errors));
            }
        }
        return $this->redirect(['horarios']);
    }

}
