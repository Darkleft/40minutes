<?php

namespace app\modules\plans\controllers;

use Yii;
use app\modules\plans\models\Cupons;
use app\modules\plans\models\searchs\CuponsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * CuponsController implements the CRUD actions for Cupons model.
 */
class CuponsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cupons models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CuponsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cupons model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cupons model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Cupons();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->cupon_value == 0) {
                Yii::$app->getSession()->setFlash('error', "Debe Ingresar un valor diferente a 0.");
            } else {
                $valiate = $model->find()->where("cupon_code = '$model->cupon_code'")->one();
                if (empty($valiate)) {
                    if ($model->cupon_type == 34 && $model->cupon_value > 100) {
                        Yii::$app->getSession()->setFlash('error', "El porcentaje debe ser menor o igual que 100.");
                    } else {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('success', "Nuevo cupón creado.");
                            return $this->redirect(['index']);
                        } else {
                            Yii::$app->getSession()->setFlash('error', "Ocurrió un error durante el proceso. " . json_encode($model->errors));
                        }
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', "Ya existe un cupón con el código $model->cupon_code");
                }
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cupons model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->cupon_value == 0) {
                Yii::$app->getSession()->setFlash('error', "Debe Ingresar un valor diferente a 0.");
            } else {
                $valiate = $model->find()->where("cupon_code = '$model->cupon_code'")->andWhere("cupon_id NOT IN ($model->cupon_id)")->one();
                if (empty($valiate)) {
                    if ($model->cupon_type == 34 && $model->cupon_value > 100) {
                        Yii::$app->getSession()->setFlash('error', "El porcentaje debe ser menor o igual que 100.");
                    } else {
                        if ($model->save()) {
                            Yii::$app->getSession()->setFlash('success', "Nuevo cupón creado.");
                            return $this->redirect(['index']);
                        } else {
                            Yii::$app->getSession()->setFlash('error', "Ocurrió un error durante el proceso. " . json_encode($model->errors));
                        }
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', "Ya existe un cupón con el código $model->cupon_code");
                }
            }
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cupons model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->cupon_hability = '0';
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cupons model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cupons the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Cupons::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
