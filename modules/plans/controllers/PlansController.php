<?php

namespace app\modules\plans\controllers;

use Yii;
use app\modules\plans\models\Plans;
use app\modules\plans\models\searchs\PlansSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\plans\models\Conditions;
use app\modules\plans\models\searchs\ConditionsSearch;

/**
 * PlansController implements the CRUD actions for Plans model.
 */
class PlansController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'activate', 'editableplancondition', 'deletecondition', 'activatecondition'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Plans models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new PlansSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Plans model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Plans model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Plans();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                \Yii::$app->getSession()->setFlash('success', 'Por favor indique las condiciones. ');
                $this->redirect(['update', 'id' => $model->plan_id]);
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Plans model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modelconditions = new Conditions();
        $searchModel = new ConditionsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $id);




        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->plan_id]);
        }
        if ($modelconditions->load(Yii::$app->request->post()) && $modelconditions->save()) {
            return $this->redirect(['update', 'id' => $model->plan_id]);
        }

        return $this->render('update', [
                    'model' => $model,
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'modelconditions' => $modelconditions
        ]);
    }

    /**
     * Deletes an existing Plans model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->plan_hability = "0";
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionActivate($id) {
        $model = $this->findModel($id);
        $model->plan_hability = "1";
        $model->save();

        return $this->redirect(['index']);
    }
    public function actionDeletecondition($id) {
        $model = Conditions::findOne($id);
        $model->pc_hability = "0";
        $model->save();

        return $this->redirect(\Yii::$app->request->referrer);
    }

    public function actionActivatecondition($id) {
        $model = Conditions::findOne($id);
        $model->pc_hability = "1";
        $model->save();

        return $this->redirect(\Yii::$app->request->referrer);
    }
    public function actionEditableplancondition() {
         if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $model = Conditions::findOne($id_editable);
            $attr = Yii::$app->request->post('editableAttribute');
            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Conditions']);
            $post['Conditions'] = $posted;
            $save = true;
            if ($model->load($post) && $save) {
                if (!$model->save()) {
                    $out = json_encode(['output' => '0', 'message' => $model->errors]);
                }
            }
            echo $out;
            return $this->redirect(\Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Plans model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Plans the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Plans::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
