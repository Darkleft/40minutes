<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Cupons */

$this->title = 'Nuevo Cupon';
?>
<div class="cupons-create">
    <p>
        <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <br><br>
    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
