<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\Sublistas;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\plans\models\searchs\CuponsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cupones';
?>
<div class="cupons-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]);   ?>

    <p>
        <?= Html::a('Nuevo Cupon', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'cupon_code',
            [
                'attribute' => 'cupon_type',
                'value' => function ($model) {
                    return Sublistas::getName($model->cupon_type);
                },
                'filter' => Sublistas::getCupons(),
            ],
            [
                'attribute' => 'cupon_value',
                'value' => function ($model) {
                    if ($model->cupon_type == 34) {
                        $ret = "$model->cupon_value %";
                    } else
                    if ($model->cupon_type == 35) {
                        $ret = "$ " . number_format($model->cupon_value, 0, ',', '.');
                    }
                    return $ret;
                }
            ],
            'cupon_date_ini',
            'cupon_date_end',
            [
                'attribute' => 'cupon_hability',
                'value' => function($model) {
                    if ($model->cupon_hability == '0') {
                        $ret = 'Inactivo';
                    } else
                    if ($model->cupon_hability == '1') {
                        $ret = 'Activo';
                    }
                    return $ret;
                },
                'filter' => ['0' => 'Inactivo', '1' => 'Activo']
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}',
                'header' => 'Acciones',
                'buttons' => [
                    'delete' =>
                    function ($url, $model) {
                        $icono = '<span class="glyphicon glyphicon-trash"></span>';
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea borrar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Eliminar')];
                        return Yii::$app->user->can('admin') && $model->cupon_hability=='1' ? Html::a($icono, $url, $titulo) : '';
                    }
                ],
            ],
        ],
    ]);
    ?>
</div>
