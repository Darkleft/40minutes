<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Cupons */

$this->title = $model->cupon_id;
?>
<div class="cupons-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cupon_id',
            'cupon_code',
            'cupon_type',
            'cupon_percentaje',
            'cupon_value',
            'cupon_date_ini',
            'cupon_date_end',
            'cupon_hability',
        ],
    ]) ?>

</div>
