<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Sublistas;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Cupons */
/* @var $form yii\widgets\ActiveForm */
if ($model->cupon_type == 34 || empty($model->cupon_type)) {
    $hideval = 'hide';
    $hideper = '';
} else
if ($model->cupon_type == 35) {
    $hideval = '';
    $hideper = 'hide';
}
?>

<div class="cupons-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-2">
        <?=
        $form->field($model, 'cupon_type')->dropDownList(Sublistas::getCupons())
        ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'cupon_code')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-2 ">
        <?= $form->field($model, 'cupon_value')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?=
        $form->field($model, 'cupon_date_end')->widget(DatePicker::className(), [
            'language' => 'es',
            'dateFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                'buttonText' => 'Fecha',
                'showAnim' => 'slide',
                'minDate' => '1'
            ],
            'options' => ['class' => 'form-control', 'readonly' => true]
        ])
        ?>
    </div>
    <div class="col-lg-2">
        <?= $model->cupon_hability == '1' || $model->isNewRecord? Html::submitButton('Guardar', ['class' => 'btn btn-success']):'' ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
