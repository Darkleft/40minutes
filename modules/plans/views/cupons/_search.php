<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\searchs\CuponsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cupons-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'cupon_id') ?>

    <?= $form->field($model, 'cumpon_code') ?>

    <?= $form->field($model, 'cupon_type') ?>

    <?= $form->field($model, 'cupon_percentaje') ?>

    <?= $form->field($model, 'cupon_value') ?>

    <?php // echo $form->field($model, 'cupon_date_ini') ?>

    <?php // echo $form->field($model, 'cupons_date_end') ?>

    <?php // echo $form->field($model, 'cupon_hability') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
