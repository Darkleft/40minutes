<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Cupons */

$this->title = 'Modificar Cupon: ' . $model->cupon_code;
?>
<div class="cupons-update">
    <p>
        <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
