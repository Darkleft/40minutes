<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Plans */

$this->title = 'Nuevo Plan';
?>
<div class="plans-create">
<?= Html::a('Atras', ['index'],['class'=>'btn btn-success'])?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
