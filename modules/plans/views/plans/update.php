<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Plans */

$this->title = 'Modificar Plan';
?>
<div class="plans-update">

    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'modelconditions' => $modelconditions
    ])
    ?>

</div>
