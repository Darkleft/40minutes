<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\grid\GridView;
use app\modules\admin\models\Sublistas;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\plans\models\Plans */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="plans-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-4">
        <?= $form->field($model, 'plan_type')->dropDownList(Sublistas::getTypeplan()) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'plan_name')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-4">
        <?= $form->field($model, 'plan_number_class')->textInput(['maxlength' => 3]) ?>
    </div>
    <div class="col-lg-6">
        <?= $form->field($model, 'plan_description')->textarea(['rows' => 6]) ?>
    </div>
    <div class="clearfix"></div>

    <div class="form-group ">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
if (!$model->isNewRecord) {
    ?>
    <div class="conditions-index">

        <h1><?= Html::encode('Condiciones') ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?php $form = ActiveForm::begin(); ?>
        <div class="hide"><?= $form->field($modelconditions, 'pc_plan')->hiddenInput(['value' => $model->plan_id]) ?></div>
        <div class="col-lg-3"> <?= $form->field($modelconditions, 'pc_zone')->dropDownList(Sublistas::getZones()) ?></div>
        <div class="col-lg-3"><?= $form->field($modelconditions, 'pc_number_user')->textInput() ?></div>
        <div class="col-lg-3"><?= $form->field($modelconditions, 'pc_price')->textInput() ?></div>
        <div class="col-lg-3"><?= Html::submitButton('Adicionar', ['class' => 'btn btn-success']) ?></div>


        <?php ActiveForm::end(); ?>
    </p>
    <div class="clearfix"></div>
    <div class="table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//                    'pc_id',
//                    'pc_plan',
            [
                'attribute' => 'pc_zone',
                'value' => function ($model) {
                    return Sublistas::getName($model->pc_zone);
                },
                'filter' => Sublistas::getZones(),
            ],
            [
                'attribute' => 'pc_number_user',
                'value' => function ($model) {
                    return "$model->pc_number_user Persona(s)";
                }
            ],
//            [
//                'attribute' => 'pc_price',
//                'value' => function ($model) {
//                    return "$ " . number_format($model->pc_price, 2, ',', '.');
//                },
//            ],
                        
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'pc_price',
                'value' => function($model) {
                    return "$ " . number_format($model->pc_price, 2, ',', '.');
                },
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editableplancondition'])],
                    ];
                },
                'filter' => false
            ],
            //'pc_hability',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {activar}',
                'header' => 'Acciones',
                'buttons' => [
                    'delete' =>
                    function ($url, $model, $key) {
                    $url = \yii\helpers\Url::toRoute(['deletecondition', 'id' => $key]);
                        $icono = '<span class="glyphicon glyphicon-trash"></span>';
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea borrar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Eliminar')];
                        return Yii::$app->user->can('admin') && $model->pc_hability == 1 ? Html::a($icono, $url, $titulo) : '';
                    },
                    'activar' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['activatecondition', 'id' => $key]);
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea Activar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Activar')];
                        return Yii::$app->user->can('admin') && $model->pc_hability == 0 ? Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, $titulo) : '';
                    }
                ],
            ],
        ],
    ]);
    ?>
    </div>
    </div>
    <?php
}
?>

