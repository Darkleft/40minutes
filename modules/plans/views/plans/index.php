<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\Sublistas;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\plans\models\searchs\PlansSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planes';
?>
<div class="plans-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nuevo Plan', ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Cupones de descuento', ['/plans/cupons/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'plan_type',
                'value' => function ($model) {
                    return Sublistas::getName($model->plan_type);
                },
                'filter' => Sublistas::getTypeplan(),
            ],
            'plan_name',
            'plan_number_class',
            'plan_description:ntext',
            //'plan_hability',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {activar}',
                'header' => 'Acciones',
                'buttons' => [
                    'delete' =>
                    function ($url, $model) {
                        $icono = '<span class="glyphicon glyphicon-trash"></span>';
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea borrar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Eliminar')];
                        return Yii::$app->user->can('admin') && $model->plan_hability == 1 ? Html::a($icono, $url, $titulo) : '';
                    },
                    'activar' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['activar', 'id' => $key]);
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea Activar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Activar')];
                        return Yii::$app->user->can('admin') && $model->plan_hability == 0 ? Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, $titulo) : '';
                    }
                ],
            ],
        ],
    ]);
    ?>
    </div>
</div>
