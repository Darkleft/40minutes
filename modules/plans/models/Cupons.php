<?php

namespace app\modules\plans\models;

use Yii;
use app\modules\admin\models\Sublistas;

/**
 * This is the model class for table "cupons".
 *
 * @property int $cupon_id
 * @property string $cupon_code
 * @property int $cupon_type
 * @property int $cupon_value
 * @property string $cupon_date_ini
 * @property string $cupon_date_end
 * @property string $cupon_hability
 *
 * @property Sublists $cuponType
 */
class Cupons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cupons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cupon_code', 'cupon_type', 'cupon_value'], 'required'],
            [['cupon_type', 'cupon_value'], 'integer'],
            [['cupon_date_ini', 'cupon_date_end'], 'safe'],
            [['cupon_code'], 'string', 'max' => 45],
            [['cupon_hability'], 'string', 'max' => 1],
            [['cupon_type'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['cupon_type' => 'sublist_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cupon_id' => 'Id',
            'cupon_code' => 'Código',
            'cupon_type' => 'Tipo',
            'cupon_value' => 'Valor/Porcentaje',
            'cupon_date_ini' => 'Fecha de Inicio',
            'cupon_date_end' => 'Fecha de Caducidad',
            'cupon_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuponType()
    {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'cupon_type']);
    }
}
