<?php

namespace app\modules\plans\models;

use Yii;
use app\modules\admin\models\Sublistas;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "plans_conditions".
 *
 * @property int $pc_id
 * @property int $pc_plan
 * @property int $pc_zone
 * @property int $pc_number_user
 * @property int $pc_price
 * @property string $pc_hability
 *
 * @property Plans $pcPlan
 * @property Sublists $pcZone
 */
class Conditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pc_plan', 'pc_zone', 'pc_number_user', 'pc_price'], 'required'],
            [['pc_plan', 'pc_zone', 'pc_number_user', 'pc_price'], 'integer'],
            [['pc_hability'], 'string', 'max' => 1],
            [['pc_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['pc_plan' => 'plan_id']],
            [['pc_zone'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['pc_zone' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pc_id' => 'Id',
            'pc_plan' => 'Plan',
            'pc_zone' => 'Zona',
            'pc_number_user' => 'Número de usuarios',
            'pc_price' => 'Precio',
            'pc_hability' => 'Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPlan()
    {
        return $this->hasOne(Plans::className(), ['plan_id' => 'pc_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcZone()
    {
        return $this->hasOne(Sublists::className(), ['sublist_id' => 'pc_zone']);
    }
    
    public function getList($idplan, $zone = null, $num = null)
    {
        $query = Conditions::find()->where("pc_plan = $idplan");
        if ($zone != null) {
           $query->andWhere("pc_zone = $zone"); 
        }
        if ($num != null) {
           $query->andWhere("pc_number_user = $num"); 
        }
        $query->andWhere("pc_hability NOT IN ('0')");
        return ArrayHelper::map($query->all(), 'pc_id', 'pc_price');
    }
}
