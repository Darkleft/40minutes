<?php

namespace app\modules\plans\models;

use Yii;
use app\modules\admin\models\Usuarios;

/**
 * This is the model class for table "cupon_user".
 *
 * @property int $id_cu
 * @property string $cu_date
 * @property int $cu_cupon
 * @property int $cu_user
 *
 * @property Cupons $cuCupon
 * @property Users $cuUser
 */
class Cuponuser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cupon_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cu_date'], 'safe'],
            [['cu_cupon', 'cu_user'], 'required'],
            [['cu_cupon', 'cu_user'], 'integer'],
            [['cu_cupon'], 'exist', 'skipOnError' => true, 'targetClass' => Cupons::className(), 'targetAttribute' => ['cu_cupon' => 'cupon_id']],
            [['cu_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['cu_user' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_cu' => 'Id Cu',
            'cu_date' => 'Cu Date',
            'cu_cupon' => 'Cu Cupon',
            'cu_user' => 'Cu User',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuCupon()
    {
        return $this->hasOne(Cupons::className(), ['cupon_id' => 'cu_cupon']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCuUser()
    {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'cu_user']);
    }
}
