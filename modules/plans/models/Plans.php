<?php

namespace app\modules\plans\models;

use Yii;
use app\modules\admin\models\Sublistas;
use app\modules\customers\models\Customers;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "plans".
 *
 * @property int $plan_id
 * @property int $plan_type
 * @property string $plan_name
 * @property string $plan_number_class
 * @property string $plan_description
 * @property string $plan_hability
 *
 * @property Customers[] $customers
 * @property CustomersPlans[] $customersPlans
 * @property Sublists $planType
 * @property PlansConditions[] $plansConditions
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_type'], 'integer'],
            [['plan_name', 'plan_number_class', 'plan_description'], 'required'],
            [['plan_description'], 'string'],
            [['plan_name', 'plan_number_class'], 'string', 'max' => 45],
            [['plan_hability'], 'string', 'max' => 1],
            [['plan_type'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['plan_type' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'ID',
            'plan_type' => 'Tipo de Plan',
            'plan_name' => 'Nombre',
            'plan_number_class' => 'Número de Clases',
            'plan_description' => 'Descripción',
            'plan_hability' => 'Plan Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['customer_plan_active' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomersPlans()
    {
        return $this->hasMany(Customersplans::className(), ['cp_plan' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanType()
    {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'plan_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlansConditions()
    {
        return $this->hasMany(Conditions::className(), ['pc_plan' => 'plan_id']);
    }
    
     public function getName($id) {
        return Plans::findOne($id)->plan_name;
    }

    public function getList() {
        return ArrayHelper::map(Plans::find()->where('plan_hability NOT IN ("0")')->all(), 'plan_id', 'plan_name');
    }
    public function getList2() {
        return ArrayHelper::map(Plans::find()->where('plan_id NOT IN (0,1)')->andWhere('plan_hability NOT IN ("0")')->all(), 'plan_id', 'plan_name');
    }
    public function getListAll() {
        return ArrayHelper::map(Plans::find()->all(), 'plan_id', 'plan_name');
    }

    public function getData($id = null) {

        if ($id != null) {
            $data = Plans::findOne($id);
        } else {
            $data = Plans::find()->where('plan_hability NOT IN ("0")')->all();
        }
        return $data;
    }
}
