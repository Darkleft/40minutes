<?php

namespace app\modules\plans\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\plans\models\Plans;

/**
 * PlansSearch represents the model behind the search form of `app\modules\plans\models\Plans`.
 */
class PlansSearch extends Plans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_id', 'plan_type'], 'integer'],
            [['plan_name', 'plan_number_class', 'plan_description', 'plan_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Plans::find()->orderBy("plan_hability desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'plan_id' => $this->plan_id,
            'plan_type' => $this->plan_type,
        ]);

        $query->andFilterWhere(['like', 'plan_name', $this->plan_name])
            ->andFilterWhere(['like', 'plan_number_class', $this->plan_number_class])
            ->andFilterWhere(['like', 'plan_description', $this->plan_description])
            ->andFilterWhere(['like', 'plan_hability', $this->plan_hability]);

        return $dataProvider;
    }
}
