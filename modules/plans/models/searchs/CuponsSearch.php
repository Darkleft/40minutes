<?php

namespace app\modules\plans\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\plans\models\Cupons;

/**
 * CuponsSearch represents the model behind the search form of `app\modules\plans\models\Cupons`.
 */
class CuponsSearch extends Cupons
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cupon_id', 'cupon_type', 'cupon_value'], 'integer'],
            [['cupon_code', 'cupon_date_ini', 'cupon_date_end', 'cupon_hability'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cupons::find()->orderBy("cupon_id desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cupon_id' => $this->cupon_id,
            'cupon_type' => $this->cupon_type,
            'cupon_value' => $this->cupon_value,
            'cupon_date_ini' => $this->cupon_date_ini,
            'cupon_date_end' => $this->cupon_date_end,
        ]);

        $query->andFilterWhere(['like', 'cupon_code', $this->cupon_code])
            ->andFilterWhere(['like', 'cupon_hability', $this->cupon_hability]);

        return $dataProvider;
    }
}
