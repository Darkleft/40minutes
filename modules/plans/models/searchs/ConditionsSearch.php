<?php

namespace app\modules\plans\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\plans\models\Conditions;

/**
 * ConditionsSearch represents the model behind the search form of `app\modules\plans\models\Conditions`.
 */
class ConditionsSearch extends Conditions
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pc_id', 'pc_plan', 'pc_zone', 'pc_number_user', 'pc_price'], 'integer'],
            [['pc_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $plan_id)
    {
        $query = Conditions::find()->where("pc_plan IN ($plan_id)");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'pc_id' => $this->pc_id,
            'pc_plan' => $this->pc_plan,
            'pc_zone' => $this->pc_zone,
            'pc_number_user' => $this->pc_number_user,
            'pc_price' => $this->pc_price,
        ]);

        $query->andFilterWhere(['like', 'pc_hability', $this->pc_hability]);

        return $dataProvider;
    }
}
