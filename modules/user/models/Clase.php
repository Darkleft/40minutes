<?php

namespace app\modules\user\models;

use Yii;

/**
 * This is the model class for table "clase".
 *
 * @property int $id
 * @property string $fecha
 * @property int $id_usuario
 * @property int $id_entrenador
 * @property int $status
 * @property int $asistio
 * @property string $direccion
 * @property int $asistentes
 */
class Clase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'clase';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['id_usuario', 'id_entrenador', 'status', 'asistio', 'asistentes'], 'integer'],
            [['direccion'], 'string', 'max' => 245],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fecha' => 'Fecha',
            'id_usuario' => 'Id Usuario',
            'id_entrenador' => 'Id Entrenador',
            'status' => 'Status',
            'asistio' => 'Asistio',
            'direccion' => 'Direccion',
            'asistentes' => 'Asistentes',
        ];
    }
}
