<?php

use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use kartik\editable\Editable;
use app\modules\admin\models\Sublistas;
use yii\widgets\ActiveForm;
use app\modules\customers\models\Customersplans;
use app\modules\plans\models\Plans;
use app\modules\plans\models\Conditions;

/* @var $this yii\web\View */
/* @var $model app\modules\customers\models\Customers */

$this->title = $model->customer_name;
?>
<div class="customers-view">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Información', ['class' => 'btn btn-primary active btn_div btn_info', 'value' => 'info']) ?>
        <?= !empty($modelgroups) ? Html::button('Grupos', ['class' => 'btn btn-primary btn_div btn_group', 'value' => 'group']) : '' ?>
        <?= Html::button('Clases', ['class' => 'btn btn-primary btn_div btn_clases', 'value' => 'clases']) ?>
        <?= Html::button('Planes', ['class' => 'btn btn-primary btn_div btn_plan', 'value' => 'plan']) ?>
        <?= !empty($valoration) ? Html::button('Valoraciones', ['class' => 'btn btn-primary btn_div btn_valorations', 'value' => 'valorations']) : '' ?>
    </p>
    <div class="div_tb info">


        <div class="col-lg-5">
            <legend>Información</legend>
            <?=
            DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'customerUser.username',
                    'customer_identification',
                    'customer_name',
                    [
                        'label' => 'Género',
                        'attribute' => 'customerGender.sublist_name',
                    ],
                    'customer_address_one',
                    'customer_address_two',
                    'customer_celphone',
                    [
                        'label' => 'Plan Activo',
                        'attribute' => 'customerPlanActive.plan_name',
                    ],
//            'customer_avatar',
                ],
            ])
            ?>
        </div>
        <div class="col-lg-5">
            <?php
            if (!empty($valoration)) {
                ?>
                <legend>Última Valoración</legend>
                <?=
                DetailView::widget([
                    'model' => $valoration,
                    'attributes' => [
                        'valorationClass.class_date',
                        'valoration_weight',
                        'valoration_imc',
                        'valoration_grease',
                        'valoration_muscle',
                        'valoration_metabolism',
                        'valoration_cervical',
                        'valoration_age',
                    ],
                ]);
            }
            ?>

        </div>

    </div>
    <div class="clearfix"></div>
    <div class="div_tb group hide">

        <legend>Grupos</legend>
        <?php
        if (!empty($modelgroups)) {
            ?>
            <div class="col-lg-6">
                <h3><b> Administrador: <?= $modelgroups[1] == null ? 'Tu' : $modelgroups[1] ?></b></h3>
                <b>Integrantes:</b>
                <?=
                GridView::widget([
                    'dataProvider' => $modelgroups[0],
                    'pjax' => true,
                    'export' => false,
                    'columns' => [
                        'refereer_identification',
                        'refereer_name',
                        'refereer_email'
                    ],
                ]);
                ?> 
            </div>
            <?php
        }
        ?>
    </div>
    <div class="clearfix"></div>
    <div class="div_tb clases hide">
        <legend>Clases</legend>
        <p>
            <?= Html::button('Solicitadas', ['class' => 'btn btn-warning active btn_div_clases btn_sol', 'value' => 'sol']) ?>
            <?= Html::button('Asignadas', ['class' => 'btn btn-warning active btn_div_clases btn_asig', 'value' => 'asig']) ?>
            <?= Html::button('Iniciadas', ['class' => 'btn btn-warning active btn_div_clases btn_ini', 'value' => 'ini']) ?>
            <?= Html::button('Finalizadas', ['class' => 'btn btn-warning btn_div_clases btn_end', 'value' => 'end']) ?>
            <?= Html::button('Canceladas', ['class' => 'btn btn-warning btn_div_clases btn_cancel', 'value' => 'cancel']) ?>
        </p>
        <div class="div_clases sol table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $searchModel->searchClasesCustomers(Yii::$app->request->queryParams, 0, $model->customer_id),
                'filterModel' => $searchModel,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'header' => 'Acciones',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::toRoute(['cancelclass', 'id' => $key]);
                                $icono = '<span class="glyphicon glyphicon-trash"></span>';
                                $titulo = [
                                    'data' => [
                                        'confirm' => '¿Desea canelar la clase?',
                                        'method' => 'post',
                                    ],
                                    'title' => Yii::t('yii', 'Eliminar')];
                                return Yii::$app->user->can('admin') ? Html::a($icono, $url, $titulo) : '';
                            },
                        ]
                    ],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases asig table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $searchModel->searchClasesCustomers(Yii::$app->request->queryParams, 1, $model->customer_id),
                'filterModel' => $searchModel,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'coach',
                        'value' => 'classCoach.coach_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
                    ['class' => 'yii\grid\ActionColumn',
                        'template' => '{delete}',
                        'header' => 'Acciones',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                $url = \yii\helpers\Url::toRoute(['cancelclass', 'id' => $key]);
                                $icono = '<span class="glyphicon glyphicon-trash"></span>';
                                $titulo = [
                                    'data' => [
                                        'confirm' => '¿Desea canelar la clase?',
                                        'method' => 'post',
                                    ],
                                    'title' => Yii::t('yii', 'Eliminar')];
                                return Yii::$app->user->can('admin') ? Html::a($icono, $url, $titulo) : '';
                            },
                        ]
                    ],
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases ini table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $searchModel->searchClasesCustomers(Yii::$app->request->queryParams, 3, $model->customer_id),
                'filterModel' => $searchModel,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'coach',
                        'value' => 'classCoach.coach_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases end hide table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $searchModel->searchClasesCustomers(Yii::$app->request->queryParams, 5, $model->customer_id),
                'filterModel' => $searchModel,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'coach',
                        'value' => 'classCoach.coach_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
        <div class="div_clases cancel hide table-responsive">
            <?=
            GridView::widget([
                'dataProvider' => $searchModel->searchClasesCustomers(Yii::$app->request->queryParams, 2, $model->customer_id),
                'filterModel' => $searchModel,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//            'class_id',
                    [
                        'attribute' => 'class_type',
                        'value' => function($model) {
                            return $model->classType->sublist_name;
                        },
                        'filter' => Sublistas::getTypeclass()
                    ],
                    [
                        'attribute' => 'coach',
                        'value' => 'classCoach.coach_name'
                    ],
                    'class_date',
                    'class_address',
                    'class_assistants',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="div_tb plan hide">
        <br>
        <?= Html::button('Nuevo Plan', ['class' => 'btn btn-success', 'onclick' => '$(".formplan").removeClass("hide")']) ?>
        <div class="formplan">
            <br><br>
            <?php $form = ActiveForm::begin(); ?>
            <div class="col-lg-2 col-xs-6">
                <?= $form->field(new Customersplans(), 'cp_plan')->dropDownList(Plans::getList2(), ['onchange' => '
                            $.ajax({
                                type:"POST",
                                data:{
                                    ajaxplan:true,
                                    value:this.value,
                                },
                                success:function(data){
                                    $(".conditions").html(data);
                                }
                            })
                       ']) ?>
            </div>
            <div class="col-lg-2 col-xs-6">
                <?= $form->field(new Customersplans(), 'zone')->dropDownList(Sublistas::getZones(), ['onchange' => '
                            $.ajax({
                                type:"POST",
                                data:{
                                    ajaxzone:true,
                                    value:this.value,
                                },
                                success:function(data){
                                    $(".conditions").html(data)
                                }
                            })
                       '])->label('Zona') ?>
            </div>
            <div class="col-lg-2 col-xs-6">
                <?=
                        $form->field(new Customersplans(), 'numberuser')
                        ->dropDownList(['1' => 'Individual', '2' => 'Dos personas', '3' => 'Tres Personas', '4' => '4 Personas'], ['onchange' => '
                            $.ajax({
                                type:"POST",
                                data:{
                                    ajaxnumber:true,
                                    value:this.value,
                                },
                                success:function(data){
                                    $(".conditions").html(data)
                                }
                            })
                       '])
                        ->label('Usuarios')
                ?>
            </div>
            <div class="col-lg-2 col-xs-6 ">
                <label>Valor</label>
                <div class="conditions" style="margin-top: 2px">
                <?= $form->field(new Customersplans(), 'cp_plan_condition')
                    ->dropDownList(Conditions::getList(1))->label(false) ?>
                </div>

            </div>
            <div class="col-lg-2 col-xs-6">
                <?= $form->field(new Customersplans(), 'cp_customer')->hiddenInput(['value' => $model->customer_id])->label(false) ?>
            </div>
            <div class="clearfix"></div>
            <div class="form-group">
                <?= Html::submitButton('Activar', ['class' => 'btn btn-success']) ?>
                <?= Html::button('Cancelar', ['class' => 'btn btn-danger', 'onclick' => '$(".formplan").addClass("hide")']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            <br><br>
        </div>
        <legend>Planes Adquiridos</legend>
        <div class="table-responsive">

            <?=
            GridView::widget([
                'dataProvider' => $dataProviderplans,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    'cp_activate',
                    'cp_plan',
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'cp_expire',
                        'editableOptions' => function ($model, $key, $index) {
                            return [
                                'inputType' => Editable::INPUT_DATE,
                                'options' => [
                                    'convertFormat' => true,
                                    'pluginOptions' => [
                                        'autoclose' => true,
                                        'format' => 'yyyy-MM-dd',
                                        'minDate' => '0',
                                    ],
                                    'size' => 'md',
                                    'value' => function($model) {

                                        return date('d.m.y H:i', $model->date_time_game);
                                    },
                                ],
                                'formOptions' => ['action' => Url::to(['editablesplanes'])],
                            ];
                        },
                    ],
                    'cp_number_class',
                    'cp_value',
                    [
                        'attribute' => 'cp_hability',
                        'value' => function($model) {
                            return $model->cp_hability == 1 ? 'Activo' : 'Inactivo';
                        }
                    ],
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="div_tb valorations hide">
        <?php
        if (!empty($valoration)) {
            ?>
            <legend>Valoraciones</legend>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvidervaloratins,
                'filterModel' => $searchModelvalorations,
                'pjax' => true,
                'export' => false,
                'columns' => [
                    [
                        'attribute' => 'coach_name',
                        'value' => 'valorationCoach.coach_name'
                    ],
                    [
                        'attribute' => 'class_date',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a($model->valorationClass->class_date, ['/clases/clases/view', 'id' => $model->valoration_class]);
                        }
                    ],
                    'valoration_weight',
                    'valoration_imc',
                    'valoration_grease',
                    'valoration_muscle',
                    'valoration_metabolism',
                    'valoration_cervical',
                    'valoration_age',
                //'valoration_hability',
//            ['class' => 'yii\grid\ActionColumn'],
                ],
            ]);
        }
        ?>
    </div>
</div>
<?php
$this->registerJs("btndiv('" . Yii::$app->session->get('btn_div') . "');btndivclases('" . Yii::$app->session->get('btn_clases') . "')", View::POS_READY);
?>
