<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\customers\models\Customers */

$this->title = 'Registro de Clientes';
?>
<div class="customers-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
