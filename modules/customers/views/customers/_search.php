<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\customers\models\searchs\CustomersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'customer_user') ?>

    <?= $form->field($model, 'customer_identification') ?>

    <?= $form->field($model, 'customer_name') ?>

    <?= $form->field($model, 'customer_gender') ?>

    <?php // echo $form->field($model, 'customer_address_one') ?>

    <?php // echo $form->field($model, 'customer_address_two') ?>

    <?php // echo $form->field($model, 'customer_celphone') ?>

    <?php // echo $form->field($model, 'customer_plan_active') ?>

    <?php // echo $form->field($model, 'customer_avatar') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
