<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use kartik\editable\Editable;
use app\modules\admin\models\Sublistas;
use app\modules\plans\models\Plans;
use app\modules\admin\models\Usuarios;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customers\models\searchs\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clientes';
?>
<div class="customers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php Html::a('Nuevo Cliente', ['register'], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="table-responsive">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pjax' => true,
        'export' => false,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'username',
                'value' => 'customerUser.username',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'pass',
                'value' => function($model) {
                    return '*******';
                },
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
                'filter' => false
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'customer_name',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'customer_gender',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => Sublistas::getGender(),
                        'displayValueConfig' => Sublistas::getGender(),
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
                'filter' => Sublistas::getGender(),
            ],
//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'attribute' => 'customer_address_one',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
//            ],
//            [
//                'class' => 'kartik\grid\EditableColumn',
//                'attribute' => 'customer_address_two',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
//            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'customer_celphone',
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'formOptions' => ['action' => Url::to(['editables'])],
                    ];
                },
            ],
            [
//                'class' => 'kartik\grid\EditableColumn',
                'header' => 'Plan Activo',
                'attribute' => 'customerPlanActive.plan_name',
//                'editableOptions' => function ($model, $key, $index) {
//                    return [
//                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
//                        'data' => Plans::getList(),
//                        'displayValueConfig' => Plans::getListAll(),
//                        'formOptions' => ['action' => Url::to(['editables'])],
//                    ];
//                },
                'filter' => Plans::getList(),
            ],
//            [
//                'attribute' => 'customer_avatar',
//                'format' => 'raw',
//                'value' => function($model) {
//
//                    $gender = $model->customer_gender != 5 ? 'female' : 'male';
//                    $avatarempty = $model->customer_gender != 5 ? 'FC03 copy.gif' : 'H02 copy.gif';
//                    $avatar = empty($model->customer_avatar) ? $avatarempty : $model->customer_avatar;
//                    return Html::img(Yii::$app->request->baseUrl . "/images/avatars/$gender/$avatar", ['style' => 'width:30%']);
//                },
//                'filter' => false
//            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'hability',
                'value' => function ($model) {
                    switch ($model->customerUser->user_hability) {
                        case 0:
                            $estado = 'Registrado';
                            break;
                        case 1:
                            $estado = 'Activo';
                            break;
                        case 2:
                            $estado = 'Inactivo';
                            break;
                        default :
                            $estado = 'Error';
                            break;
                    }
                    return $estado;
                },
                'editableOptions' => function ($model, $key, $index) {
                    return [
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'data' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo'],
                        'displayValueConfig' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo'],
                        'formOptions' => ['action' => Url::to(['editableuser'])],
                    ];
                },
                'filter' => [0 => 'Registrado', 1 => 'Activo', 2 => 'Inactivo']
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'header' => 'Acciones',
            ],
        ],
    ]);
    ?>
    </div>
</div>
