<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\models\Sublistas;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\modules\customers\models\Customers */
/* @var $form yii\widgets\ActiveForm */
$alfabeto = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'customer_identification')->textInput(['maxlength' => 10, 'onkeypress' => 'return solonum(event)']) ?>

    <?= $form->field($model, 'customer_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_gender')->dropDownList(Sublistas::getGender(), ['prompt' => 'Seleccione un Género']) ?>

    <?= $form->field($model, 'customer_address_one')->textInput(['onclick' => '$("#modaldir").modal()']) ?>

    <?= $form->field($model, 'customer_address_two')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_celphone')->textInput(['maxlength' => 10]) ?>

    <?= $form->field($model, 'customer_plan_active')->textInput() ?>

    <?= $form->field($model, 'customer_avatar')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>



    <?php ActiveForm::end(); ?>

    <style>
        #modaldir{
            width: 65%;
        }
    </style>
    <?php Modal::begin(['id' => 'modaldir', 'header'=>'Crear Dirección']);?>
    <div class="">
        <i><strong>Dirección: </strong>Construya la dirección seleccionado los datos de los campos.</i>
        <table id ="tabledireccion" class="table-condensed "> 
            <tr>
                <td>CARRERA</td>
                <td>NUM</td>
                <td>A..Z</td>
                <td>BIS</td>
                <td>A..Z</td>
                <td>SUR</td>
                <td>#</td>
                <td>NUM</td>
                <td>A..Z</td>
                <td>-</td>
                <td>NUM</td>
                <td>SUR</td>
            </tr>
            <tr>
                <td style="width: 10px">
                    <select class="dir" id="camp1">
                        <option selected></option>
                        <option value="CALLE">CALLE</option>
                        <option value="CARRERA">CARRERA</option>
                        <option value="DIAGONAL">DIAGONAL</option>                    
                        <option value="TRANSVERSAL">TRANSVERSAL</option>
                        <option value="AVENIDA CALLE">AVENIDA CALLE</option>
                        <option value="AVENIDA CARRERA">AVENIDA CARRERA</option>               
                    </select>
                </td>
                <td>
                    <input type="text" class="dir num" id="camp2" pattern="[0-9]*" maxlength="3">
                </td>
                <td>              
                    <select class="dir" id="camp3">
                        <option selected></option>
                        <?php
                        foreach ($alfabeto as $value) {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <select class="dir" id="camp4">
                        <option selected></option>
                        <option value="BIS">BIS</option>                    
                    </select>
                </td>
                <td>
                    <select class="dir" id="camp5">
                        <option selected></option>
                        <?php
                        foreach ($alfabeto as $value) {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <select class="dir" id="camp6">
                        <option selected></option>
                        <option value="SUR">SUR</option>
                        <option value="ESTE">ESTE</option>
                    </select>
                </td>
                <td>
                    <select class="dir" id="camp7">
                        <option value="#" selected>#</option>
                    </select>
                </td>
                <td>
                    <input type="text" class="dir num" id="camp8" pattern="[0-9]*" maxlength="3">
                </td>
                <td>
                    <select class="dir" id="camp9">
                        <option selected></option>
                        <?php
                        foreach ($alfabeto as $value) {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>
                    <select class="dir" id="camp10">
                        <option value="-" selected>-</option>
                    </select>
                </td>
                <td>
                    <input type="text" class="dir num" id="camp11" pattern="[0-9]*" maxlength="3">
                </td>
                <td>
                    <select class="dir" id="camp12">
                        <option selected></option>
                        <option value="SUR">SUR</option>
                        <option value="ESTE">ESTE</option>
                    </select>
                </td>
            </tr>
        </table>
        <fieldset>
            <table>
                <tr>
                    <td colspan="3">
                        <i><strong>Complemento: </strong>Seleccione el tipo de complemento en la lista desplegable, escriba en el recuadro el detalle y pulse el botón "Adicionar otro complemento". Realice este proceso hasta tener toda la parte complementaria de la dirección.</i>
                    </td>            
                </tr>        
                <tr>
                    <td>
                        <select size="1" name="componenteComplemento" id="listaComplemento" class="formlista">
                            <option value="">Escoja una opción</option>
                            <option value="AP">Apartamento</option>
                            <option value="AG">Agrupación</option>
                            <option value="BL">Bloque</option>
                            <option value="BG">Bodega</option>
                            <option value="CN">Camino</option>
                            <option value="CT ">Carretera</option>
                            <option value="CEL">Célula</option>
                            <option value="CA">Casa</option>
                            <option value="CONJ">Conjunto</option>
                            <option value="CS ">Consultorio</option>
                            <option value="DP">Depésito</option>
                            <option value="ED ">Edificio</option>
                            <option value="EN">Entrada</option>
                            <option value="ESQ">Esquina</option>
                            <option value="ET">Etapa</option>
                            <option value="GJ">Garaje</option>
                            <option value="IN">Interior</option>
                            <option value="KM">Kilómetro</option>
                            <option value="LC">Local</option>
                            <option value="LT">Lote</option>
                            <option value="MZ">Manzana</option>
                            <option value="MN">Mezanine</option>
                            <option value="MD">Módulo</option>
                            <option value="OF">Oficina</option>
                            <option value="PS">Paseo</option>
                            <option value="PA">Parcela</option>
                            <option value="PH">Penthouse</option>
                            <option value="PI">Piso</option>
                            <option value="PN">Puente</option>
                            <option value="PD">Predio</option>
                            <option value="SC">Salón comunal</option>
                            <option value="SR">Sector</option>
                            <option value="SL">Solar</option>
                            <option value="SS">Semisótano</option>
                            <option value="SM">Super manzana</option>
                            <option value="TO">Torre</option>
                            <option value="UN">Unidad</option>
                            <option value="UR">Unidad residencial</option>
                            <option value="URB">Urbanización</option>
                            <option value="ZN">Zona</option>
                        </select>
                    </td>
                    <td>
                        <input type="text" name="valorComplemento" size="10" id="valorComplemento" class="formcampoyarea" title="Acepta n&uacute;meros, espacios y letras may&uacute;sculas."/> 
                    </td>
                    <td>
                        <input type="button" id="btnAgregarComplemento"  title="Agregar a Complemento"  value="Adicionar otro complemento"  onclick="editarComplemento();" class="btn-primary btn"/> 
                    </td>
                </tr>        
            </table>
        </fieldset>  
        <fieldset>  
            <legend>Dirección Completa</legend>
            <table>
                <tr>
                    <td>
                        <input type="text" class="span5" id="dir" readonly="readonly">
                    </td>
                    <td>
                        <input type="button" id="enviar" class="btn-primary btn" value="Enviar Dirección">
                    </td>
                </tr>
            </table>
        </fieldset>
        <script type="text/javascript">
            /*<![CDATA[*/
            function editarComplemento() {
                var dir = $("#dir").val();
                var comple = $("#listaComplemento").val();
                var valcomple = $("#valorComplemento").val();
                var dirfinal = "";

                if (dir != "" && comple != "" && valcomple != "") {
                    dirfinal = dir + " " + comple + " " + valcomple;
                    $("#dir").val(dirfinal);
                    $("#listaComplemento").val("");
                    $("#valorComplemento").val("");
                }
            }
            /*]]>*/
        </script>
    </div>
    <?php
    Modal::end();
    ?>
</div>
