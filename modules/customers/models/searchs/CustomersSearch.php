<?php

namespace app\modules\customers\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\customers\models\Customers;

/**
 * CustomersSearch represents the model behind the search form of `app\modules\customers\models\Customers`.
 */
class CustomersSearch extends Customers {

    public $username;
    public $pass;
    public $hability;

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_id', 'customer_user', 'customer_identification', 'customer_gender', 'customer_plan_active'], 'integer'],
            [['hability', 'pass', 'username', 'customer_name', 'customer_address_one', 'customer_address_two', 'customer_celphone', 'customer_avatar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Customers::find();
        $query->joinWith('customerUser');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'customer_id' => $this->customer_id,
            'customer_user' => $this->customer_user,
            'customer_identification' => $this->customer_identification,
            'customer_gender' => $this->customer_gender,
            'customer_plan_active' => $this->customer_plan_active,
            'user_hability' => $this->hability,
        ]);

        $query->andFilterWhere(['like', 'customer_name', $this->customer_name])
                ->andFilterWhere(['like', 'customer_address_one', $this->customer_address_one])
                ->andFilterWhere(['like', 'customer_address_two', $this->customer_address_two])
                ->andFilterWhere(['like', 'customer_celphone', $this->customer_celphone])
                ->andFilterWhere(['like', 'customer_avatar', $this->customer_avatar])
                ->andFilterWhere(['like', 'username', $this->username]);

        return $dataProvider;
    }

}
