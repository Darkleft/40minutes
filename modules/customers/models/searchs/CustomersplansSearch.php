<?php

namespace app\modules\customers\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\customers\models\Customersplans;

/**
 * CustomersplansSearch represents the model behind the search form of `app\modules\customers\models\Customersplans`.
 */
class CustomersplansSearch extends Customersplans
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cp_id', 'cp_plan', 'cp_customer', 'cp_number_classs'], 'integer'],
            [['cp_activate', 'cp_expire', 'cp_hability'], 'safe'],
            [['cp_value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customersplans::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cp_id' => $this->cp_id,
            'cp_activate' => $this->cp_activate,
            'cp_plan' => $this->cp_plan,
            'cp_customer' => $this->cp_customer,
            'cp_expire' => $this->cp_expire,
            'cp_number_classs' => $this->cp_number_classs,
            'cp_value' => $this->cp_value,
        ]);

        $query->andFilterWhere(['like', 'cp_hability', $this->cp_hability]);

        return $dataProvider;
    }
    
    public function searchOne($id)
    {
        $query = Customersplans::find()->where("cp_customer = $id")->orderBy("cp_id desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        

        return $dataProvider;
    }
}
