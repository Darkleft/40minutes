<?php

namespace app\modules\customers\models;

use Yii;
use app\modules\plans\models\Plans;
use app\modules\plans\models\Conditions;

/**
 * This is the model class for table "customers_plans".
 *
 * @property int $cp_id
 * @property string $cp_activate
 * @property int $cp_plan
 * @property int $cp_plan_condition
 * @property int $cp_customer
 * @property string $cp_expire
 * @property int $cp_number_class
 * @property string $cp_value
 * @property int $cp_valorations
 * @property string $cp_hability
 *
 * @property Customers $cpCustomer
 * @property PlansConditions $cpPlanCondition
 * @property Plans $cpPlan
 */
class Customersplans extends \yii\db\ActiveRecord
{
    
    public $zone;
    public $numberuser;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers_plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'cp_plan', 'cp_plan_condition', 'cp_customer', 'cp_expire', 'cp_number_class'], 'required'],
            [['cp_activate', 'cp_expire'], 'safe'],
            [['cp_plan', 'cp_plan_condition', 'cp_customer', 'cp_number_class', 'cp_valorations'], 'integer'],
            [['cp_value'], 'number'],
            [['cp_hability'], 'string', 'max' => 1],
            [['cp_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['cp_customer' => 'customer_id']],
            [['cp_plan_condition'], 'exist', 'skipOnError' => true, 'targetClass' => Conditions::className(), 'targetAttribute' => ['cp_plan_condition' => 'pc_id']],
            [['cp_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['cp_plan' => 'plan_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cp_id' => 'ID',
            'cp_activate' => 'Fecha de Activación',
            'cp_plan' => 'Plan',
            'cp_plan_condition' => 'Condición',
            'cp_customer' => 'Cliente',
            'cp_expire' => 'Fecha de caduidad',
            'cp_number_class' => 'Número de clases finalizadas',
            'cp_value' => 'Valor pagado',
            'cp_valorations'=>'Valoraciones a tomar',
            'cp_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'cp_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpPlanCondition()
    {
        return $this->hasOne(Conditions::className(), ['pc_id' => 'cp_plan_condition']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCpPlan()
    {
        return $this->hasOne(Plans::className(), ['plan_id' => 'cp_plan']);
    }
}
