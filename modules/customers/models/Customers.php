<?php

namespace app\modules\customers\models;

use Yii;
use app\modules\admin\models\Sublistas;
use app\modules\plans\models\Plans;
use app\modules\admin\models\Usuarios;

/**
 * This is the model class for table "customers".
 *
 * @property int $customer_id
 * @property int $customer_user
 * @property string $customer_identification
 * @property string $customer_name
 * @property int $customer_gender
 * @property string $customer_address_one
 * @property string $customer_address_two
 * @property string $customer_celphone
 * @property int $customer_plan_active
 * @property string $customer_avatar
 * @property string $long_one
 * @property string $lat_one
 * @property string $long_two
 * @property string $lat_two
 *
 * @property Sublists $customerGender
 * @property Plans $customerPlanActive
 * @property Users $customerUser
 */
class Customers extends \yii\db\ActiveRecord {

    public $username;
    public $pass;
    public $hability;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['customer_user', 'customer_name', 'customer_gender', 'customer_address_one', 'customer_celphone', 'customer_plan_active', 'customer_avatar'], 'required'],
            [['customer_user', 'customer_identification', 'customer_gender', 'customer_plan_active'], 'integer'],
            [['customer_name'], 'string', 'max' => 100],
            [['customer_address_one', 'customer_address_two', 'long_one', 'lat_one', 'long_two', 'lat_two'], 'string', 'max' => 45],
            [['customer_celphone'], 'string', 'max' => 10],
            [['customer_avatar'], 'string', 'max' => 20],
            [['customer_gender'], 'exist', 'skipOnError' => true, 'targetClass' => Sublistas::className(), 'targetAttribute' => ['customer_gender' => 'sublist_id']],
            [['customer_plan_active'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['customer_plan_active' => 'plan_id']],
            [['customer_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['customer_user' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'customer_id' => 'Id',
            'customer_user' => 'Usuario',
            'customer_identification' => 'Identificación',
            'customer_name' => 'Nombre',
            'customer_gender' => 'Género',
            'customer_address_one' => 'Dirección 1',
            'customer_address_two' => 'Dirección 2',
            'customer_celphone' => 'Número de Celular',
            'customer_plan_active' => 'Plan Activo',
            'customer_avatar' => 'Avatar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerGender() {
        return $this->hasOne(Sublistas::className(), ['sublist_id' => 'customer_gender']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerPlanActive() {
        return $this->hasOne(Plans::className(), ['plan_id' => 'customer_plan_active']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerUser() {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'customer_user']);
    }

    public function getName($user) {
        return Customers::find()->where(['customer_user' => $user])->one()->customer_name;
    }

    public function getNamecustomer($user) {
        return Customers::findOne($user)->customer_name;
    }

    public function getAddress($idcustomer) {
        $model = Customers::findOne($idcustomer);
        if (!empty($model)) {
            $array[$model->customer_address_one] = $model->customer_address_one;
            if (!empty($model->customer_address_two)) {
                $array[$model->customer_address_two] = $model->customer_address_two;
            }

            return $array;
        }
    }

}
