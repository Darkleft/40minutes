<?php

namespace app\modules\customers\controllers;

use Yii;
use app\modules\customers\models\Customers;
use app\modules\customers\models\searchs\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\admin\models\Usuarios;
use app\models\User;
use yii\filters\AccessControl;
use app\modules\groups\models\Groups;
use app\modules\clases\models\Clases;
use app\modules\clases\models\searchs\ClasesSearch;
use app\modules\customers\models\searchs\CustomersplansSearch;
use app\modules\groups\models\searchs\RefereersSearch;
use app\modules\valorations\models\Valorations;
use app\modules\valorations\models\searchs\ValorationsSearch;
use app\modules\groups\models\Refereers;
use app\modules\customers\models\Customersplans;
use app\modules\plans\models\Conditions;
use app\modules\plans\models\Plans;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'register', 'editables', 'editableuser', 'editablesplanes', 'view', 'cancelclass', 'finalizaclass'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        Yii::$app->session->set('btn_div', 'info');
        Yii::$app->session->set('btn_clases', 'asig');
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        $modelcustomeplan = new Customersplans();
        $model = $this->findModel($id);
        $modelgroup = '';
        $searchModel = new ClasesSearch();
        $searchModelgroup = new RefereersSearch();
        $modelgroups = Groups::find()->where("group_customer = $id")->andWhere("group_hability NOT IN ('0')")->one();
        if (!empty($modelgroups)) {
            $modelgroup = [$searchModelgroup->searchCustomers($modelgroups->group_id), $model->customer_name];
        } else {
            if (empty($model->customer_identification)) {
                Yii::$app->getSession()->setFlash('warning', 'Se deben actualizar los datos del cliente');
            } else {
                $referer = Refereers::find()->where("refereer_identification = $model->customer_identification")->andWhere("refereer_hability NOT IN (0)")->one();
                if (!empty($referer)) {
                    $modelreferer = Groups::findOne($referer->refereer_group);
                    $damin = $this->findModel($modelreferer->group_customer)->customer_name;
                    $modelgroup = [$searchModelgroup->searchCustomers($referer->refereer_group), $damin];
                }
            }
        }
        $searchModelplans = new CustomersplansSearch();
        $dataProviderplans = $searchModelplans->searchOne($id);
        $valoration = Valorations::find()->where("valoration_user = $id")->orderBy("valoration_class desc")->limit(1)->one();
        $searchModelvalorations = new ValorationsSearch();
        $dataProvidervalorations = $searchModelvalorations->searchCustomers(Yii::$app->request->queryParams, $id);

        if ($modelcustomeplan->load(Yii::$app->request->post())) {
            $validate = Customersplans::find()->where("cp_customer = $id")->andWhere("cp_hability NOT IN ('0')")->all();
            if (empty($validate)) {
                $modelcustomeplan->cp_plan_condition = Yii::$app->request->post('condition');
                $modelcustomeplan->cp_number_class = 0;
                $modelcustomeplan->cp_customer = $model->customer_id;
                $typeplan = Plans::findOne($modelcustomeplan->cp_plan)->plan_type;
                if ($typeplan == 13) {
                    $expire = date_create(date('Y-m-d'));
                    date_add($expire, date_interval_create_from_date_string("3 MONTH"));
                } else
                if ($typeplan == 14) {
                    $expire = date_create(date('Y-m-d'));
                    date_add($expire, date_interval_create_from_date_string("1 MONTH"));
                }
                $valueplan = Conditions::findOne($modelcustomeplan->cp_plan_condition);
                $modelcustomeplan->cp_value = $valueplan->pc_price;
                $modelcustomeplan->cp_expire = date_format($expire, "Y-m-d");
                if (!$modelcustomeplan->save()) {
                    Yii::$app->getSession()->setFlash('error', json_encode($modelcustomeplan->errors));
                }else{
                    return $this->redirect(['view', 'id'=>$id]);
                }
            } else {
                 Yii::$app->getSession()->setFlash('error', 'El usuario aún tiene un plan activo.');
            }
        }
        if (Yii::$app->request->post('ajaxplan')) {
            Yii::$app->session->set('plan', Yii::$app->request->post('value'));
            return \yii\helpers\Html::dropDownList('condition', '', Conditions::getList(
                                    Yii::$app->session->get('plan'), Yii::$app->session->get('zone'), Yii::$app->session->get('number')), ['class' => 'form-control']);
        }
        if (Yii::$app->request->post('ajaxzone')) {
            Yii::$app->session->set('zone', Yii::$app->request->post('value'));
            return \yii\helpers\Html::dropDownList('condition', '', Conditions::getList(
                                    Yii::$app->session->get('plan'), Yii::$app->session->get('zone'), Yii::$app->session->get('number')), ['class' => 'form-control']);
        }
        if (Yii::$app->request->post('ajaxnumber')) {
            Yii::$app->session->set('number', Yii::$app->request->post('value'));
            return \yii\helpers\Html::dropDownList('condition', '', Conditions::getList(
                                    Yii::$app->session->get('plan'), Yii::$app->session->get('zone'), Yii::$app->session->get('number')), ['class' => 'form-control']);
        }


        return $this->render('view', [
                    'model' => $model,
                    'modelgroups' => $modelgroup,
                    'searchModel' => $searchModel,
                    'dataProviderplans' => $dataProviderplans,
                    'valoration' => $valoration,
                    'dataProvidervaloratins' => $dataProvidervalorations,
                    'searchModelvalorations' => $searchModelvalorations
        ]);
    }

    /**
     * Creates a new Customers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
//    public function actionRegister() {
//        $model = new Customers();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['index']);
//        }
//
//        return $this->render('create', [
//                    'model' => $model,
//        ]);
//    }

    /**
     * Updates an existing Customers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCancelclass($id) {
        $model = Clases::findOne($id);
        $modeluser = new User();
        $model->class_hability = '2';
        if ($model->save()) {
            Yii::$app->getSession()->setFlash('Success', 'Clase Cancelada.');
            $modeluser->mail("40minutes", "Se ha cancelado la Clase para " . $model->classCustmer->customer_name . " del día $model->class_date", $model->classCoach->coachUser->username, "Cancelación de clase");
            $modeluser->mail("40minutes", "Se ha cancelado su Clase del día $model->class_date", $model->classCustmer->customerUser->username, "Cancelación de clase");
        } else {
            Yii::$app->getSession()->setFlash('error', json_encode($model->errors));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    public function actionFinalizaclass($id) {
        $model = Clases::findOne($id);
        $modelcoach = Coachs::findOne($model->class_coach);
        $modelcustplan = Customersplans::find()
                        ->where("cp_customer = $model->class_custmer")
                        ->andWhere("cp_hability IN ('1')")->one();
        $model->class_hability = '4';
        $model->class_end = date('H:i:s');
        $modelcoach->coah_hability = "1";
        $modelcustplan->cp_number_class = $modelcustplan->cp_number_class + 1;
        if ($model->save() && $modelcoach->save() && $modelcustplan->save()) {
            Yii::$app->getSession()->setFlash('Success', 'Clase Finalizada.');
        } else {
            Yii::$app->getSession()->setFlash('error', json_encode($model->errors));
        }
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Customers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
//    public function actionDelete($id) {
//        $this->findModel($id);
//
//        return $this->redirect(['index']);
//    }



    public function actionEditables() {
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $model = Customers::findOne($id_editable);
            $attr = Yii::$app->request->post('editableAttribute');
            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Customers']);
            $post['Customers'] = $posted;
            $save = true;
            if ($attr == 'customer_identification') {
                $validate = $model->find()->where("customer_identification = '" . $post['Customers']['customer_identification'] . "'")->one();
                if (!empty($validate)) {
                    $save = false;
                    $out = json_encode(['output' => '0', 'message' => 'El número de identificación ya esta registrado.']);
                }
            }
            if ($model->load($post) && $save) {
                if (!$model->save()) {
                    $out = json_encode(['output' => '0', 'message' => $model->errors]);
                }
            }
            echo $out;
            return;
        }
    }

    public function actionEditablesplanes() {
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $model = Customersplans::findOne($id_editable);
            $attr = Yii::$app->request->post('editableAttribute');
            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Customersplans']);
            $post['Customersplans'] = $posted;
            $save = true;

            if ($model->load($post) && $save) {
                if (!$model->save()) {
                    $out = json_encode(['output' => '0', 'message' => $model->errors]);
                }
            }
            echo $out;
            return;
        }
    }

    public function actionEditableuser() {
        date_default_timezone_set("America/Bogota");
        if (Yii::$app->request->post('hasEditable')) {
            $id_editable = Yii::$app->request->post('editableKey');
            $user = new User();
            $model = Customers::findOne($id_editable);
            $modelUser = Usuarios::findOne($model->customer_user);
            $attr = Yii::$app->request->post('editableAttribute');

            $out = json_encode(['output' => '', 'message' => '']);
            $post = [];
            $posted = current($_POST['Customers']);
            $post['Customers'] = $posted;
            $save = true;
            if ($attr == 'pass') {
                $modelUser->password = $user->encriptarPass($post['Customers']['pass']);
            }
            if ($attr == 'username') {
                $validate = $modelUser->find()->where("username = '" . $post['Customers']['username'] . "'")->one();
                if (!empty($validate)) {
                    $save = false;
                    $out = json_encode(['output' => '0', 'message' => 'El correo ya esta registrado.']);
                } else {
                    $modelUser->username = $post['Customers']['username'];
//                    $content = "Hola $model->customer_name,<br><br> Bienvenido a 40minutes <br><br> Para activiar tu usuario por favor da click en el siguiente enlace:<br>";
//                    $usuario = $user->encriptarPass($modelUser->user_id);
//                    $value = $user->encriptarPass(strtotime(date('Y-m-d H:i:s')));
//                    $content .= \yii\helpers\Html::a("Activación de usuario", "http://localhost/40minutes/activations/activate?key=$usuario&validate=$value", ['class' => 'btn btn-primary']);
//                    $user->mail("40minutes", $content, Yii::$app->request->post('email'), "Registro 40 Minutes");
                }
            }
            if ($save) {
                if (!$modelUser->save()) {
                    $out = json_encode(['output' => '0', 'message' => $modelUser->errors]);
                }
            }
//            print_r();

            echo $out;
            return;
        }
    }

    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
