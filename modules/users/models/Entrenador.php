<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "entrenador".
 *
 * @property int $id
 * @property string $nombre
 * @property string $email
 * @property string $password
 * @property int $status
 * @property string $imagen
 * @property int $costo_uno
 * @property int $costo_dos
 * @property int $costo_tres
 * @property int $costo_cuatro
 * @property int $costo_cinco
 * @property int $jornada
 */
class Entrenador extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entrenador';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'costo_uno', 'costo_dos', 'costo_tres', 'costo_cuatro', 'costo_cinco', 'jornada'], 'integer'],
            [['imagen'], 'string'],
            [['nombre', 'email', 'password'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'email' => 'Email',
            'password' => 'Password',
            'status' => 'Status',
            'imagen' => 'Imagen',
            'costo_uno' => 'Costo Uno',
            'costo_dos' => 'Costo Dos',
            'costo_tres' => 'Costo Tres',
            'costo_cuatro' => 'Costo Cuatro',
            'costo_cinco' => 'Costo Cinco',
            'jornada' => 'Jornada',
        ];
    }
}
