<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "calificaciones".
 *
 * @property int $id
 * @property int $id_clase
 * @property int $puntualidad
 * @property int $empatia
 * @property int $presentacion
 * @property string $observaciones
 * @property string $created_at
 */
class Calificaciones extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'calificaciones';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_clase', 'puntualidad', 'empatia', 'presentacion'], 'integer'],
            [['observaciones'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_clase' => 'Id Clase',
            'puntualidad' => 'Puntualidad',
            'empatia' => 'Empatia',
            'presentacion' => 'Presentacion',
            'observaciones' => 'Observaciones',
            'created_at' => 'Created At',
        ];
    }
}
