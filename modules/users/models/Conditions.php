<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "plans_conditions".
 *
 * @property int $pc_id
 * @property int $pc_plan
 * @property int $pc_zone
 * @property int $pc_number_user
 * @property int $pc_price
 * @property string $pc_hability
 *
 * @property Plans $pcPlan
 * @property Sublists $pcZone
 */
class Conditions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans_conditions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pc_plan', 'pc_zone', 'pc_number_user', 'pc_price'], 'required'],
            [['pc_plan', 'pc_zone', 'pc_number_user', 'pc_price'], 'integer'],
            [['pc_hability'], 'string', 'max' => 1],
            [['pc_plan'], 'exist', 'skipOnError' => true, 'targetClass' => Plans::className(), 'targetAttribute' => ['pc_plan' => 'plan_id']],
            [['pc_zone'], 'exist', 'skipOnError' => true, 'targetClass' => Sublists::className(), 'targetAttribute' => ['pc_zone' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pc_id' => 'Pc ID',
            'pc_plan' => 'Pc Plan',
            'pc_zone' => 'Pc Zone',
            'pc_number_user' => 'Pc Number User',
            'pc_price' => 'Pc Price',
            'pc_hability' => 'Pc Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcPlan()
    {
        return $this->hasOne(Plans::className(), ['plan_id' => 'pc_plan']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPcZone()
    {
        return $this->hasOne(Sublists::className(), ['sublist_id' => 'pc_zone']);
    }
}
