<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $user_id
 * @property int $type_user
 * @property string $username
 * @property string $password
 * @property string $create_at
 * @property string $last_login
 * @property string $ip_loged
 * @property string $user_hability
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_user', 'username', 'password'], 'required'],
            [['type_user'], 'integer'],
            [['create_at', 'last_login'], 'safe'],
            [['username', 'password'], 'string', 'max' => 100],
            [['ip_loged'], 'string', 'max' => 45],
            [['user_hability'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'type_user' => 'Type User',
            'username' => 'Username',
            'password' => 'Password',
            'create_at' => 'Create At',
            'last_login' => 'Last Login',
            'ip_loged' => 'Ip Loged',
            'user_hability' => 'User Hability',
        ];
    }
}
