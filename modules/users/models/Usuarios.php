<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $id
 * @property string $nombre
 * @property string $password
 * @property string $email
 * @property string $updated_at
 * @property string $avatar
 * @property string $direccion1
 * @property string $direccion2
 * @property string $telefono
 * @property int $max_clases_mes
 * @property int $plan
 * @property int $tipo
 * @property int $tipo_grupo
 * @property int $valor_pagado
 * @property string $fecha_vencimiento
 * @property int $clases_tomadas
 *
 * @property Membresia[] $membresias
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['updated_at', 'fecha_vencimiento'], 'safe'],
            [['avatar'], 'string'],
            [['max_clases_mes', 'plan', 'tipo', 'tipo_grupo', 'valor_pagado', 'clases_tomadas'], 'integer'],
            [['nombre', 'password', 'email', 'direccion1', 'direccion2', 'telefono'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'password' => 'Password',
            'email' => 'Email',
            'updated_at' => 'Updated At',
            'avatar' => 'Avatar',
            'direccion1' => 'Direccion1',
            'direccion2' => 'Direccion2',
            'telefono' => 'Telefono',
            'max_clases_mes' => 'Max Clases Mes',
            'plan' => 'Plan',
            'tipo' => 'Tipo',
            'tipo_grupo' => 'Tipo Grupo',
            'valor_pagado' => 'Valor Pagado',
            'fecha_vencimiento' => 'Fecha Vencimiento',
            'clases_tomadas' => 'Clases Tomadas',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMembresias()
    {
        return $this->hasMany(Membresia::className(), ['id_usuario' => 'id']);
    }
}
