<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "plans".
 *
 * @property int $plan_id
 * @property int $plan_type
 * @property string $plan_name
 * @property string $plan_number_class
 * @property string $plan_description
 * @property string $plan_hability
 *
 * @property Customers[] $customers
 * @property CustomersPlans[] $customersPlans
 * @property Sublists $planType
 * @property PlansConditions[] $plansConditions
 */
class Plans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'plans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_type'], 'integer'],
            [['plan_name', 'plan_number_class', 'plan_description'], 'required'],
            [['plan_description'], 'string'],
            [['plan_name', 'plan_number_class'], 'string', 'max' => 45],
            [['plan_hability'], 'string', 'max' => 1],
            [['plan_type'], 'exist', 'skipOnError' => true, 'targetClass' => Sublists::className(), 'targetAttribute' => ['plan_type' => 'sublist_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'plan_id' => 'Plan ID',
            'plan_type' => 'Plan Type',
            'plan_name' => 'Plan Name',
            'plan_number_class' => 'Plan Number Class',
            'plan_description' => 'Plan Description',
            'plan_hability' => 'Plan Hability',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['customer_plan_active' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomersPlans()
    {
        return $this->hasMany(CustomersPlans::className(), ['cp_plan' => 'plan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanType()
    {
        return $this->hasOne(Sublists::className(), ['sublist_id' => 'plan_type']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlansConditions()
    {
        return $this->hasMany(PlansConditions::className(), ['pc_plan' => 'plan_id']);
    }
}
