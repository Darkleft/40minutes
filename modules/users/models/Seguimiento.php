<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "seguimiento".
 *
 * @property int $id
 * @property int $id_usuario
 * @property int $id_clase
 * @property string $ejercicio
 * @property string $serie
 * @property string $repeticiones
 * @property int $tipo
 * @property string $peso
 * @property string $rondas
 * @property string $medio
 * @property string $usuario
 * @property string $circuito
 */
class Seguimiento extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'seguimiento';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_usuario', 'id_clase', 'tipo'], 'integer'],
            [['ejercicio', 'serie', 'repeticiones', 'peso', 'rondas', 'medio', 'usuario', 'circuito'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'id_clase' => 'Id Clase',
            'ejercicio' => 'Ejercicio',
            'serie' => 'Serie',
            'repeticiones' => 'Repeticiones',
            'tipo' => 'Tipo',
            'peso' => 'Peso',
            'rondas' => 'Rondas',
            'medio' => 'Medio',
            'usuario' => 'Usuario',
            'circuito' => 'Circuito',
        ];
    }
}
