<?php

namespace app\modules\users\models;

use Yii;

/**
 * This is the model class for table "valoracion".
 *
 * @property int $id
 * @property int $id_usuario
 * @property string $peso
 * @property string $imc
 * @property string $grasa
 * @property string $musculo
 * @property string $metabolismo
 * @property string $grasavisceral
 * @property string $created_at
 * @property string $edad_del_cuerpo
 */
class Valoracion extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'valoracion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_usuario'], 'integer'],
            [['created_at'], 'safe'],
            [['peso', 'imc', 'grasa', 'musculo', 'metabolismo', 'grasavisceral', 'edad_del_cuerpo'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_usuario' => 'Id Usuario',
            'peso' => 'Peso',
            'imc' => 'Imc',
            'grasa' => 'Grasa',
            'musculo' => 'Musculo',
            'metabolismo' => 'Metabolismo',
            'grasavisceral' => 'Grasavisceral',
            'created_at' => 'Created At',
            'edad_del_cuerpo' => 'Edad Del Cuerpo',
        ];
    }
}
