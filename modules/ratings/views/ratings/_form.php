<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\ratings\models\Ratings */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ratings-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rating_date')->textInput() ?>

    <?= $form->field($model, 'rating_class')->textInput() ?>

    <?= $form->field($model, 'rating_puntuality')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_empathy')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_presentation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'rating_observations')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
