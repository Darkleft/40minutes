<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\ratings\models\searchs\RatingsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ratings-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'rating_id') ?>

    <?= $form->field($model, 'rating_date') ?>

    <?= $form->field($model, 'rating_class') ?>

    <?= $form->field($model, 'rating_puntuality') ?>

    <?= $form->field($model, 'rating_empathy') ?>

    <?php // echo $form->field($model, 'rating_presentation') ?>

    <?php // echo $form->field($model, 'rating_observations') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
