<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\ratings\models\searchs\RatingsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calificaciones';
?>
<div class="ratings-index">
    <?= Html::a('Atras', ['/site/index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= Html::button('Ranking', ['class' => 'btn btn-primary', 'value' => 'ranking']) ?>
    <?= Html::button('All', ['class' => 'btn btn-primary', 'value' => 'all']) ?>
    <br><br>
    <div class="ranking div_rk table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $searchModel->searchRanking(),
//            'filterModel' => $searchModel,
            'columns' => [
                'coach_name',
                [
                    'attribute' => 'rating_puntuality',
                    'value' => function($model) {
                        return $model->rating_puntuality . ' Pts.';
                    }
                ],
                [
                    'attribute' => 'rating_empathy',
                    'value' => function($model) {
                        return $model->rating_empathy . ' Pts.';
                    }
                ],
                [
                    'attribute' => 'rating_presentation',
                    'value' => function($model) {
                        return $model->rating_presentation . ' Pts.';
                    }
                ],
                [
                    'header' => 'Total',
                    'attribute' => 'rating_observations',
                    'value' => function ($model) {
                        return $model->rating_observations . ' Pts.';
                    }
                ]
            ],
        ]);
        ?>
    </div>
    <div class="hide all div_rk table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                'rating_date',
                'rating_class',
                [
                    'header' => 'Entrenador',
                    'attribute' => 'ratingClass.classCoach.coach_name',
                ],
                'rating_puntuality',
                'rating_empathy',
                'rating_presentation',
                'rating_observations:ntext',
            ],
        ]);
        ?>
    </div>
</div>
<script>
    $(".btn-primary").on('click', function () {
        $('.div_rk').addClass('hide');
        $('.' + this.value).removeClass('hide')
    })
</script>

