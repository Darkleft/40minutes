<?php

namespace app\modules\ratings\models;

use Yii;
use app\modules\clases\models\Clases;

/**
 * This is the model class for table "ratings".
 *
 * @property int $rating_id
 * @property string $rating_date
 * @property int $rating_class
 * @property string $rating_puntuality
 * @property string $rating_empathy
 * @property string $rating_presentation
 * @property string $rating_observations
 *
 * @property Class $ratingClass
 */
class Ratings extends \yii\db\ActiveRecord {

    public $coach_name;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'ratings';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['coach_name', 'rating_date'], 'safe'],
            [['rating_class', 'rating_puntuality', 'rating_empathy', 'rating_presentation', 'rating_observations'], 'required'],
            [['rating_class'], 'integer'],
            [['rating_observations'], 'string'],
            [['rating_puntuality', 'rating_empathy', 'rating_presentation'], 'string', 'max' => 1],
            [['rating_class'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['rating_class' => 'class_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'rating_id' => 'Rating ID',
            'rating_date' => 'Fecha',
            'rating_class' => 'Clase',
            'rating_puntuality' => 'Puntualidad',
            'rating_empathy' => 'Empatía',
            'rating_presentation' => 'Presentación',
            'rating_observations' => 'Observaciones',
            'coach_name' => 'Entrenador'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRatingClass() {
        return $this->hasOne(Clases::className(), ['class_id' => 'rating_class']);
    }

}
