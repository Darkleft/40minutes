<?php

namespace app\modules\ratings\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\ratings\models\Ratings;
use app\modules\coachs\models\Coachs;

/**
 * RatingsSearch represents the model behind the search form of `app\modules\ratings\models\Ratings`.
 */
class RatingsSearch extends Ratings {

    public $coach_name;
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['rating_id', 'rating_class'], 'integer'],
            [['coach_name','rating_date', 'rating_puntuality', 'rating_empathy', 'rating_presentation', 'rating_observations'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Ratings::find()->orderBy("rating_date desc");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'rating_id' => $this->rating_id,
            'rating_date' => $this->rating_date,
            'rating_class' => $this->rating_class,
        ]);

        $query->andFilterWhere(['like', 'rating_puntuality', $this->rating_puntuality])
                ->andFilterWhere(['like', 'rating_empathy', $this->rating_empathy])
                ->andFilterWhere(['like', 'rating_presentation', $this->rating_presentation])
                ->andFilterWhere(['like', 'rating_observations', $this->rating_observations]);

        return $dataProvider;
    }

    public function searchRanking() {

        $query = Ratings::find()
                ->select([new \yii\db\Expression('coach_name,
                                FORMAT((sum(rating_puntuality)/count(rating_puntuality)),1) rating_puntuality , 
                                FORMAT((sum(rating_empathy)/count(rating_empathy)),1) rating_empathy, 
                                FORMAT((sum(rating_presentation)/count(rating_presentation)),1)  rating_presentation,
                                FORMAT((FORMAT((sum(rating_puntuality)/count(rating_puntuality)),1)+FORMAT((sum(rating_empathy)/count(rating_empathy)),1)+FORMAT((sum(rating_presentation)/count(rating_presentation)),1) )/3,1) rating_observations
')])
                ->join('join', 'class c', 'rating_class = class_id')
                ->join('join', 'coachs ch', 'class_coach = coach_id')
                ->andOnCondition('coah_hability NOT IN ("0")')
                ->groupBy('coach_id')->orderBy('rating_observations desc');
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

}
