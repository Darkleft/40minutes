<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\admin\models\Usuarios;

/* @var $this yii\web\View */
/* @var $model app\modules\chats\models\Chats */

$this->title = "Chat N°" . $model->chat_id;
?>
<div class="chats-view">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    <br><br>
    <legend><?= Html::encode($this->title) ?></legend>

    <div class="col-lg-6">

        <?=
        GridView::widget([
            'dataProvider' => $dataProvideruser,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'Item'],
                'chat_date_ini',
                [
                    'header' => 'Avatar',
                    'format'=>'raw',
                    'value' => function($model) {
                        return Html::img(Yii::$app->request->baseUrl."/images/avatars/".$model->chat_user."_t.png", ['style'=>"width:40px"]);
                    }
                ],
                [
                    'attribute' => 'chat_user',
                    'value' => function($model) {
                        return Usuarios::getName($model->chat_user);   
                    }
                ],
                'chat_message',
            ],
        ]);
        ?>
    </div>
</div>
