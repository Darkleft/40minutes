<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\chats\models\searchs\ChatsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Chats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="chats-index">

    <p>
        <?= Html::a('Atras', ['/site/index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => 'Item'],
            'chat_class',
            'chat_date_ini',
//            'chat_user',
//            'chat_message',
//            'chat_hability',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'header' => 'Acciones',
            ],
        ],
    ]);
    ?>
</div>
