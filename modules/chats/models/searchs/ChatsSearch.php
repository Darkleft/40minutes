<?php

namespace app\modules\chats\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\chats\models\Chats;
use app\modules\chats\models\Userschat;
use app\modules\chats\models\Messageschat;

/**
 * ChatsSearch represents the model behind the search form of `app\modules\chats\models\Chats`.
 */
class ChatsSearch extends Chats {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['chat_id'], 'integer'],
            [['chat_date_ini', 'chat_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Chats::find()->groupBy("chat_class");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'chat_id' => $this->chat_id,
            'chat_date_ini' => $this->chat_date_ini,
        ]);

        $query->andFilterWhere(['like', 'chat_hability', $this->chat_hability]);

        return $dataProvider;
    }

    public function searchall($clase) {
        $query = Chats::find()->where("chat_class = $clase");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}
