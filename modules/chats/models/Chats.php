<?php

namespace app\modules\chats\models;

use Yii;

/**
 * This is the model class for table "chats".
 *
 * @property int $chat_id
 * @property string $chat_date_ini
 * @property int $chat_class
 * @property int $chat_user
 * @property string $chat_message
 * @property string $chat_hability
 *
 * @property UsersChat[] $usersChats
 */
class Chats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chats';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['chat_date_ini'], 'safe'],
            [['chat_class', 'chat_user', 'chat_message'], 'required'],
            [['chat_class', 'chat_user'], 'integer'],
            [['chat_message'], 'string'],
            [['chat_hability'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'chat_id' => 'Id',
            'chat_date_ini' => 'Fecha y Hora',
            'chat_class' => 'Clase',
            'chat_user' => 'Usuario',
            'chat_message' => 'Mensaje',
            'chat_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsersChats()
    {
        return $this->hasMany(UsersChat::className(), ['uc_chat' => 'chat_id']);
    }
}
