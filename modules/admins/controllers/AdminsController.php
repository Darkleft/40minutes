<?php

namespace app\modules\admins\controllers;

use Yii;
use app\modules\admins\models\Admins;
use app\modules\admins\models\searchs\AdminsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\modules\admin\models\Usuarios;
use app\models\User;
use app\modules\auth\models\AuthAssignment;

/**
 * AdminsController implements the CRUD actions for Admins model.
 */
class AdminsController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'activar'],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = ['/site/index'];
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Admins models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new AdminsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admins model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Admins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Admins();
        $modeluser = new Usuarios();
        $modelauth = new AuthAssignment();

        if ($model->load(Yii::$app->request->post())) {
            $validate = $modeluser->find()->where("username = '$model->admin_user'")->one();
            //$validate2 = $modeluser->find()->where("username = '$model->admin_user'")->one();
            if (empty($validate)) {
                $modeluser->username = $model->admin_user;
                $modeluser->password = User::encriptarPass($model->admin_token);
                $modeluser->type_user = 2;
                if ($modeluser->save()) {
                    $model->admin_user = $modeluser->user_id;
                    $model->admin_key = '1';
                    if ($model->save()) {
                        $modelauth->user_id =$modeluser->user_id;
                        $modelauth->item_name = 'sysadmin';
                        $modelauth->created_at = date('YmdHis');
                        $modelauth->save();
                        return $this->redirect(['index']);
                    }
                } else {
                    Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($modeluser->errors));
                }
            } else {
                $model->addError('admin_user', 'Ya existe un usuario registrado con este correo.');
            }
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Admins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $modeluser = Usuarios::findOne($model->admin_user);

        if ($model->load(Yii::$app->request->post())) {
            if (!filter_var($model->admin_user, FILTER_VALIDATE_EMAIL)) {
                $message = 'Email no válido.';
                $model->addError('admin_user', $message);
            } else {
                $validate = $modeluser->find()->where("username = '$model->admin_user'")->andWhere("user_id NOT IN ($modeluser->user_id)")->one();
                //$validate2 = $modeluser->find()->where("username = '$model->admin_user'")->one();
                if (empty($validate)) {
                    $modeluser->username = $model->admin_user;
                    $modeluser->password = User::encriptarPass($model->admin_token);
                    if ($modeluser->save()) {
                        $model->admin_user = $modeluser->user_id;
                        empty($model->admin_key) ? $model->admin_key = '1' : '';
                        if ($model->save()) {
                            return $this->redirect(['index']);
                        } else {
                            Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($model->errors));
                        }
                    } else {
                        Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($modeluser->errors));
                    }
                } else {
                    $model->addError('admin_user', 'Ya existe un usuario registrado con este correo.');
                }
            }
        }
        $model->admin_user = $modeluser->username;
        $model->admin_token = User::desencriptarPass($modeluser->password);
        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admins model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->admin_key = '0';
        if ($model->save()) {
            
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($model->errors));
        }

        return $this->redirect(['index']);
    }

    public function actionActivar($id) {
        $model = $this->findModel($id);
        $model->admin_key = '1';
        if ($model->save()) {
            
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ocurrió un error durante el proceso. ' . json_encode($model->errors));
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Admins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Admins::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
