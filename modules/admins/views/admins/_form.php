<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admins\models\Admins */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admins-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-lg-2">
        <?= $form->field($model, 'admin_user')->textInput() ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'admin_identification')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-2">
        <?= $form->field($model, 'admin_name')->textInput(['maxlength' => true]) ?>
    </div>

    <div class="col-lg-2">
        <?= $form->field($model, 'admin_token')->textInput(['maxlength' => true]) ?>
    </div>
    <div class="col-lg-2">
        <label> </label><br>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
