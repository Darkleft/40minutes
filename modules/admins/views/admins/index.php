<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admins\models\searchs\AdminsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Administradores';
?>
<div class="admins-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Nuevo Administrador', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'admin_user',
                'value' => function ($model) {
                    return $model->adminUser->username;
                }
            ],
            'admin_identification',
            'admin_name',
            [
                'attribute' => 'admin_key',
                'value' => function($model) {
                    if ($model->admin_key == '0') {
                        $ret = 'Inactivo';
                    } else
                    if ($model->admin_key == '1') {
                        $ret = 'Activo';
                    }
                    return $ret;
                },
                'filter' => ['0' => 'Inactivo', '1' => 'Activo']
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete} {activar}',
                'header' => 'Acciones',
                'buttons' => [
                    'delete' =>
                    function ($url, $model) {
                        $icono = '<span class="glyphicon glyphicon-trash"></span>';
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea borrar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Eliminar')];
                        return Yii::$app->user->can('admin') && $model->admin_key == 1 ? Html::a($icono, $url, $titulo) : '';
                    },
                    'activar' => function ($url, $model, $key) {
                        $url = \yii\helpers\Url::toRoute(['activar', 'id' => $key]);
                        $titulo = [
                            'data' => [
                                'confirm' => '¿Desea Activar este elemento?',
                                'method' => 'post',
                            ],
                            'title' => Yii::t('yii', 'Activar')];
                        return Yii::$app->user->can('admin') && $model->admin_key == 0 ? Html::a('<span class="glyphicon glyphicon-upload"></span>', $url, $titulo) : '';
                    }
                ],
            ],
        ],
    ]);
    ?>
</div>
