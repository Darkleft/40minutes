<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admins\models\searchs\AdminsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="admins-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'admin_id') ?>

    <?= $form->field($model, 'admin_user') ?>

    <?= $form->field($model, 'admin_identification') ?>

    <?= $form->field($model, 'admin_name') ?>

    <?= $form->field($model, 'admin_key') ?>

    <?php // echo $form->field($model, 'admin_token') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
