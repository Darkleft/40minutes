<?php

namespace app\modules\admins\models;

use Yii;
use app\modules\admin\models\Usuarios;

/**
 * This is the model class for table "admin".
 *
 * @property int $admin_id
 * @property int $admin_user
 * @property string $admin_identification
 * @property string $admin_name
 * @property string $admin_key
 * @property string $admin_token
 *
 * @property Users $adminUser
 */
class Admins extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'admin';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['admin_user', 'admin_identification', 'admin_name', 'admin_key', 'admin_token'], 'required'],
            [['admin_identification'], 'integer'],
            [['admin_name'], 'string', 'max' => 100],
            [['admin_key', 'admin_token'], 'string', 'max' => 255],
            [['admin_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['admin_user' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'admin_id' => 'Id',
            'admin_user' => 'Email',
            'admin_identification' => 'Identificación',
            'admin_name' => 'Nombre',
            'admin_key' => 'Estado',
            'admin_token' => 'Token/Password',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdminUser() {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'admin_user']);
    }

    public function getName($user) {
        return Admins::find()->where(['admin_user' => $user])->one()->admin_name;
    }

}
