<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\groups\models\searchs\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Grupos';
?>
<div class="groups-index">
    <?= Html::a('Atras', ['/site/index'], ['class' => 'btn btn-success']) ?>
    <div class="col-md-12">
        <h1><?= Html::encode($this->title) ?></h1>
        <div class="table-responsive">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'group_id',
                [
                    'header' => 'Administrador',
                    'value' => 'groupCustomer.customer_name',
                ],
                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'header' => 'Acciones',
                ],
            ],
        ]);
        ?>
        </div>
    </div>
</div>
