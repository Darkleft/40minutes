<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\groups\models\Groups */

$this->title = "Grupo N° $model->group_id";
?>
<div class="groups-view">
    <?= Html::a('Atras', ['index'], ['class' => 'btn btn-success']) ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <h3><b>Administrador: <?= $model->groupCustomer->customer_name; ?></b></h3>

    <br>
    
    <b>Integrantes</b>
    <div class="col-md-12 table-responsive">        
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'refereer_identification',
                'refereer_name',
                'refereer_email'
            ],
        ]);
        ?> 
    </div>

</div>
