<?php

namespace app\modules\groups\models;

use Yii;
use app\modules\admin\models\Usuarios;

/**
 * This is the model class for table "refereers".
 *
 * @property int $refereer_id
 * @property int $refereer_user
 * @property int $refereer_group
 * @property string $refereer_identification
 * @property string $refereer_name
 * @property string $refereer_email
 * @property string $refereer_hability
 *
 * @property Users $refereerUser
 * @property Groups $refereerGroup
 */
class Refereers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'refereers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['refereer_user', 'refereer_group', 'refereer_identification', 'refereer_name', 'refereer_email'], 'required'],
            [['refereer_user', 'refereer_group', 'refereer_identification'], 'integer'],
            [['refereer_name', 'refereer_email'], 'string', 'max' => 100],
            [['refereer_hability'], 'string', 'max' => 1],
            [['refereer_identification'], 'unique'],
            [['refereer_email'], 'unique'],
            [['refereer_email'], 'email'],
            [['refereer_user'], 'exist', 'skipOnError' => true, 'targetClass' => Usuarios::className(), 'targetAttribute' => ['refereer_user' => 'user_id']],
            [['refereer_group'], 'exist', 'skipOnError' => true, 'targetClass' => Groups::className(), 'targetAttribute' => ['refereer_group' => 'group_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'refereer_id' => 'ID',
            'refereer_user' => 'Usuario',
            'refereer_group' => 'Grupo',
            'refereer_identification' => 'Integrante Cedula',
            'refereer_name' => 'Integrante Nombre',
            'refereer_email' => 'Correo',
            'refereer_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefereerUser()
    {
        return $this->hasOne(Usuarios::className(), ['user_id' => 'refereer_user']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefereerGroup()
    {
        return $this->hasOne(Groups::className(), ['group_id' => 'refereer_group']);
    }
}
