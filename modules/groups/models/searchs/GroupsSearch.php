<?php

namespace app\modules\groups\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\groups\models\Groups;

/**
 * GroupsSearch represents the model behind the search form of `app\modules\groups\models\Groups`.
 */
class GroupsSearch extends Groups
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'group_customer'], 'integer'],
            [['group_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Groups::find()->where("group_hability NOT IN ('0')");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'group_customer' => $this->group_customer,
        ]);

        $query->andFilterWhere(['like', 'group_hability', $this->group_hability]);

        return $dataProvider;
    }
    public function searchCustomers($params, $id)
    {
        $query = Groups::find()
                ->join('join', 'refereers', 'group_id = refereer_group')
                ->andOnCondition("group_customer = $id")
                ->andOnCondition("group_hability NOT IN ('0')")
                ->andOnCondition("refereer_hability NOT IN ('0')");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'group_customer' => $this->group_customer,
        ]);

        $query->andFilterWhere(['like', 'group_hability', $this->group_hability]);

        return $dataProvider;
    }
}
