<?php

namespace app\modules\groups\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\groups\models\Refereers;

/**
 * RefereersSearch represents the model behind the search form of `app\modules\groups\models\Refereers`.
 */
class RefereersSearch extends Refereers {

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['refereer_id', 'refereer_group', 'refereer_identification'], 'integer'],
            [['refereer_name', 'refereer_hability'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id) {
        $query = Refereers::find()->where("refereer_group = $id");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'refereer_id' => $this->refereer_id,
            'refereer_group' => $this->refereer_group,
            'refereer_identification' => $this->refereer_identification,
        ]);

        $query->andFilterWhere(['like', 'refereer_name', $this->refereer_name])
                ->andFilterWhere(['like', 'refereer_hability', $this->refereer_hability]);

        return $dataProvider;
    }

    public function searchCustomers($id_group) {
        $query = Refereers::find()->join('join', 'groups', 'group_id = refereer_group')
                ->andOnCondition("refereer_group = $id_group")
                ->andOnCondition("refereer_hability NOT IN ('0')")
                ->andOnCondition("group_hability NOT IN ('0')");

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $dataProvider;
    }

}
