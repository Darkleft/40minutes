<?php

namespace app\modules\groups\models;

use Yii;
use app\modules\clases\models\Clases;
use app\modules\customers\models\Customers;

/**
 * This is the model class for table "groups".
 *
 * @property int $group_id
 * @property int $group_customer
 * @property string $group_hability
 *
 * @property Class[] $classes
 * @property Customers $groupCustomer
 * @property Refereers[] $refereers
 */
class Groups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'groups';
    }
    
    public function transactions() {
        return [
            self::SCENARIO_DEFAULT => self::OP_INSERT,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_customer'], 'required'],
            [['group_customer'], 'integer'],
            [['group_hability'], 'string', 'max' => 1],
            [['group_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['group_customer' => 'customer_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'ID',
            'group_customer' => 'Creador',
            'group_hability' => 'Estado',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClasses()
    {
        return $this->hasMany(Clases::className(), ['class_group' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroupCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'group_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRefereers()
    {
        return $this->hasMany(Refereers::className(), ['refereer_group' => 'group_id']);
    }
}
