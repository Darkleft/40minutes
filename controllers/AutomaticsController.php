<?php

namespace app\controllers;

use app\modules\customers\models\Customers;
use app\modules\customers\models\Customersplans;
use app\models\User;
use Yii;
use app\modules\plans\models\Cupons;

class AutomaticsController extends \yii\console\Controller {

    public function actionInactiveplancustomer($validate) {
        if ($validate == '0') {
            $modelcustomer = new Customers();
            $modelcustomerplan = new Customersplans();
            $modeluser = new User();

            $plans = $modelcustomerplan->find()->where("cp_expire < CURRENT_DATE()")->andWhere("cp_hability NOT IN ('0')")->all();
            if (!empty($plans)) {
                foreach ($plans as $plan) {
                    $modelplan = $modelcustomerplan->findOne($plan->cp_id);

                    $model = $modelcustomer->find()->where("customer_id = $modelplan->cp_customer")->one();
                    print_r($modelplan);
                    $modelplan->cp_hability = '0';
                    $model->customer_plan_active = '0';
                    if ($modelplan->save() && $model->save()) {
                        echo 'Guardo';
                        try {
                            $modeluser->mail("40minutes", "Apreciado Cliente $model->customer_name, a partir de la fecha su plan queda inactivo.", $model->customerUser->username, "Clases 40 Minutes");
                        } catch (Exception $ex) {
                            echo $ex->error;
                        }
                    }
                }
            }
        }
    }

    public function actionInactivecupon($validate) {
        if ($validate == '0') {
            $modelcupons = new Cupons();
            $cupons = $modelcupons->find()->where("cupon_date_end < CURRENT_DATE()")->andWhere("cupon_hability NOT IN ('0')")->all();
            if (!empty($cupons)) {
                foreach ($cupons as $cupon) {
                    $model = $modelcupons->findOne($cupon->cupon_id);
                    $model->cupon_hability = '0';
                    $model->save();
                }
            }
        }
    }

}
