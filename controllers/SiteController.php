<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\User;
use app\modules\users\models\Users;
use app\modules\users\models\Usuarios;
use Pusher\Pusher;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'globals'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index', 'globals'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function ( $rule, $action ) {

                    if (Yii::$app->user->isGuest) {
                        $msn = 'Por favor inicie sesión.';
                        $redirect = ['/site/login'];
                    } else {
                        $msn = 'No tiene permitido el acceso.';
                        $redirect = Yii::$app->request->referrer;
                    }
                    Yii::$app->getSession()->setFlash('error', $msn);
                    $this->redirect($redirect);
                },
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionErrorint($name, $message) {

        return $this->render('error', ['name' => $name, 'message' => $message]);
    }

    public function actionAbout() {
        return $this->render('about');
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->identity->typeuser == 2) {
            return $this->render('index');
        } else {
            Yii::$app->user->logout();
            Yii::$app->getSession()->setFlash('error', 'Su usuario no esta activo para la Web, por favor ingrese por la app.');
            return $this->redirect(['site/login']);
        }
    }

    public function actionIndex2() {
        set_time_limit(0);
        $modelusuarios = new Usuarios();
        $modelentrenador = new \app\modules\users\models\Entrenador();
        $modelclases = new \app\modules\users\models\Clase();
        $modelcalificaciones = new \app\modules\users\models\Calificaciones();
        $modelvaloracion = new \app\modules\users\models\Valoracion();
        $modeluser = new User();

        //      Script para clientes
        $usuarios = $modelusuarios->find()->orderBy("id")->all();
        foreach ($usuarios as $usuario) {
//             echo $usuario->email;
            $users = new \app\modules\admin\models\Usuarios();
            $modelcustomers = new \app\modules\customers\models\Customers();
            $users->type_user = 4;
            $users->username = $usuario->email;
            $users->password = $modeluser->encriptarPass($usuario->password);
            $modelcustomers->customer_id = $usuario->id;
            $modelcustomers->customer_name = ucwords(strtolower($usuario->nombre));
            $modelcustomers->customer_plan_active = 1;
            $modelcustomers->customer_celphone = $usuario->telefono == '' ? 0 : substr($usuario->telefono, 0, 10);
            $modelcustomers->customer_address_one = $usuario->direccion1;
            $modelcustomers->customer_address_two = $usuario->direccion2;
            $modelcustomers->customer_avatar = 'F01.png';
            $modelcustomers->customer_gender = 5;
            $modelcustomers->customer_plan_active = $usuario->plan;
            if ($users->save()) {
                $modelcustomers->customer_user = $users->user_id;
                if ($modelcustomers->save()) {

                    echo "Guardo<br>";
                } else {
                    print_r($modelcustomers->errors);
                    $users->findOne($users->user_id)->delete();
                }
            } else {
                print_r($users->errors);
            }
        }
        //     Script para entrenadores
        $entrenadores = $modelentrenador->find()->all();
        foreach ($entrenadores as $entrenador) {
            $users = new \app\modules\admin\models\Usuarios();
            $modelcoachs = new \app\modules\coachs\models\Coachs();
            $modelclases = new \app\modules\users\models\Clase();
            $users->type_user = 3;
            $users->username = $entrenador->email;
            $users->password = $modeluser->encriptarPass($entrenador->password);
            $modelcoachs->coach_id = $entrenador->id;
            $modelcoachs->coach_name = $entrenador->nombre;
//
            switch ($entrenador->jornada) {
                case 2:
                    $modelcoachs->coach_working_day = 16;
                    break;
                case 3:
                    $modelcoachs->coach_working_day = 17;
                    break;
                default :
                    $modelcoachs->coach_working_day = 15;
                    break;
            }

            if ($users->save()) {
                $modelcoachs->coach_user = $users->user_id;
                if ($modelcoachs->save()) {
                    echo "Guardo cocach <br>";
                } else {
                    print_r($modelcoachs->errors);
                    $users->findOne($users->user_id)->delete();
                }
            } else {
                print_r($users->errors);
            }
        }
        //         Script para clases
        $clases = $modelclases->find()->all();
        foreach ($clases as $clase) {
            $modelclass = new \app\modules\clases\models\Clases();
            $modelclass->class_id = $clase->id;
            $modelclass->class_type = 1;
            $modelclass->class_custmer = $clase->id_usuario;
            $modelclass->class_coach = $clase->id_entrenador;
            $modelclass->class_date = $clase->fecha;
            $modelclass->class_address = $clase->direccion;
            $modelclass->class_assistants = $clase->asistentes;
            $modelclass->class_hability = $clase->asistio == 0 ? "2" : "5";
//
            $vaidate = \app\modules\customers\models\Customers::findOne($clase->id_usuario);
            if (!empty($vaidate)) {
                if ($modelclass->save()) {
//                     print_r("Guardo clase");
                } else {
                    echo "clase $clase->id <br>";
                    print_r($modelclass->errors);
                    echo "<br>";
                }
            }
        }
        //script calificaciones
        $calificaciones = $modelcalificaciones->find()->all();
        foreach ($calificaciones as $calificacion) {
            $modelratings = new \app\modules\ratings\models\Ratings();
            $modelratings->rating_date = $calificacion->created_at;
            $modelratings->rating_class = $calificacion->id_clase;
            $modelratings->rating_puntuality = "$calificacion->puntualidad";
            $modelratings->rating_empathy = "$calificacion->empatia";
            $modelratings->rating_presentation = "$calificacion->presentacion";
            $modelratings->rating_observations = $calificacion->observaciones;
            $validate = $modelclass->findOne($calificacion->id_clase);
            if (!empty($validate)) {
                if ($modelratings->save()) {
                    print_r("Guardo clase");
                } else {
                    echo "calificación $calificacion->id <br>";
                    print_r($modelratings->errors);
                    echo "<br>";
                }
            }
        }
        //     script Valoraciones
        $valoraciones = $modelvaloracion->find()->all();
        foreach ($valoraciones as $valoracion) {
            $modelvaloration = new \app\modules\valorations\models\Valorations();
//
            $fecha = date('Y-m', strtotime($valoracion->created_at));
//
            $validate = $modelclases->find()->where("id_usuario = $valoracion->id_usuario")->andWhere("fecha LIKE '$fecha%'")->limit(1)->one();
            if (!empty($validate)) {
                $modelvaloration->valoration_id = $valoracion->id;
                $modelvaloration->valoration_user = \app\modules\customers\models\Customers::findOne($valoracion->id_usuario)->customer_user;
                $modelvaloration->valoration_class = $validate->id;
                $modelvaloration->valoration_coach = $validate->id_entrenador;
                $modelvaloration->valoration_weight = $valoracion->peso == null || empty($valoracion->peso) ? 0 : str_replace(',', '.', $valoracion->peso);
                $modelvaloration->valoration_imc = $valoracion->imc == null || empty($valoracion->imc) ? 0 : str_replace(',', '.', $valoracion->imc);
                $modelvaloration->valoration_metabolism = $valoracion->metabolismo == null || empty($valoracion->metabolismo) ? 0 : str_replace(',', '.', $valoracion->metabolismo);
                $modelvaloration->valoration_grease = $valoracion->grasa == null || empty($valoracion->grasa) ? 0 : str_replace(',', '.', $valoracion->grasa);
                $modelvaloration->valoration_muscle = $valoracion->musculo == null || empty($valoracion->musculo) ? 0 : str_replace(',', '.', $valoracion->musculo);
                $modelvaloration->valoration_cervical = $valoracion->grasavisceral == null || empty($valoracion->grasavisceral) ? 0 : str_replace(',', '.', $valoracion->grasavisceral);
                $modelvaloration->valoration_age = $valoracion->edad_del_cuerpo == null || empty($valoracion->edad_del_cuerpo) ? 0 : str_replace(',', '.', $valoracion->edad_del_cuerpo);
                $modelvaloration->valoration_hability = "1";
                print_r($modelvaloration->attributes);
                $validate2 = \app\modules\valorations\models\Valorations::find()
                                ->where("valoration_user = $modelvaloration->valoration_user")
                                ->andWhere("valoration_class = $modelvaloration->valoration_class")->one();
                if (empty($validate2)) {
                    $modelvaloration->save();
                }
            }
        }
        //seguimientos 
        $modelseguimiento = new \app\modules\users\models\Seguimiento();
        $seguimientos = $modelseguimiento->find()->all();
        foreach ($seguimientos as $seguimiento) {
            $validate = $modelclass->findOne($seguimiento->id_clase);
            if (!empty($validate)) {
                $modelexcerciseclass = new \app\modules\clases\models\Excerciseclass();
                $modelexcerciseclass->ec_id = $seguimiento->id;
                $modelexcerciseclass->ec_exercise = 1;
                $modelexcerciseclass->ec_class = $seguimiento->id_clase;
                $modelexcerciseclass->ec_old = $seguimiento->ejercicio;
                $modelexcerciseclass->ec_weight = $seguimiento->peso;
                $modelexcerciseclass->ec_medium = $seguimiento->medio;
                $modelexcerciseclass->ec_repeat = $seguimiento->repeticiones;
                $modelexcerciseclass->ec_round = $seguimiento->rondas;
                $modelexcerciseclass->ec_serie = $seguimiento->serie;
                $modelexcerciseclass->ec_circuit = $seguimiento->circuito;
                if (!$modelexcerciseclass->save()) {
                    print_r($modelexcerciseclass->errors);
                }
            }
        }



        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin() {


        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['index']);
        } else
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['index']);
        } else {
            $model->password = '';
            return $this->render('login', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact() {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
//
            return $this->refresh();
        }
        return $this->render('contact', [
                    'model' => $model,
        ]);
    }

    public function actionGlobals() {
        if (Yii::$app->request->post('ajaxbtndiv')) {
            Yii::$app->session->set('btn_div', Yii::$app->request->post('value'));
        }
        if (Yii::$app->request->post('ajaxbtnclases')) {
            Yii::$app->session->set('btn_clases', Yii::$app->request->post('value'));
        }
    }

    public function actionIndex3() {

        $options = array(
            'cluster' => 'us2',
            'useTLS' => true
        );
        $pusher = new Pusher(
                '71e7c1f684b58f0b9b65', '2602be0e55eef10b1f80', '632578', $options
        );

        $data['message'] = 'hello world';
        $pusher->trigger('my-channel', 'my-event', $data);
    }

}
