<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\web\Controller;
use app\modules\admin\models\Usuarios;
use app\modules\groups\models\Refereers;

class ActivationsController extends Controller {

    public function actionActivate($key, $value) {
        date_default_timezone_set("America/Bogota");
        $modeluser = new User();
        $modelusuarios = new Usuarios();
        $key = str_replace(' ', '+', $key);
        $id = $modeluser->desencriptarPass($key);
        $fecha = $modeluser->desencriptarPass($value);
        $date = date_create($fecha);
        date_add($date, date_interval_create_from_date_string("3 days"));
        $caduce = date_format($date, "Y-m-d H:i:s");
        $date1 = date_create(date('Y-m-d H:i:s'));
        $date2 = date_create($caduce);
        $diff = $date1 > $date2;
        if (!$diff) {
            $model = $modelusuarios->findOne($id);
            if (!empty($model)) {
                $model->user_hability = "1";
                if ($model->save()) {
                    Yii::$app->getSession()->setFlash('success', 'Registro completado.');
                    return $this->redirect(['/site/about']);
                } else {
                    $name = 'Error Interno.';
                    $message = json_encode($model->errors);

                    return $this->redirect(['/site/error', 'name' => $name, 'message' => $message]);
                }
            } else {
                return $this->redirect(['/site/errorint', 'name' => 'Error', 'message' => 'Intento de Activación para un usuario no válido.']);
            }
        }else{
            return $this->redirect(['/site/errorint', 'name'=>'Link Caducado', 'message'=>'El tiempo de espera para la activación de su usuario a vencido, por favor comunicarse con el administrador del sistema']);
        }
    }

}
