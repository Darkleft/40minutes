<?php

namespace app\models;

use app\modules\users\models\Users;
use app\modules\admins\models\Admins;
use app\modules\coachs\models\Coachs;
use app\modules\customers\models\Customers;

class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface {

    public $user_id;
    public $username;
    public $password;
    public $type_user;
    public $create_at;
    public $last_login;
    public $ip_loged;
    public $user_hability;
    public $authKey;
    public $accessToken;

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id) {
        //return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
        $users = Users::findOne($id);
        if (!count($users)) {

            return null;
        } else {
//            $this->users_password=$users->getAttributes('users_password');
            return new static($users);
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = NULL) {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username) {
        $users = Users::find()->where(['username' => $username])->one();
        if (!count($users)) {
            return null;
        } else {
            return new static($users);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getId() {
        return $this->user_id;
    }

    public function getUsername() {
        return $this->username;
    }

    public function getName() {
        switch ($this->type_user) {
            case 2:
                $name = Admins::getName($this->user_id);
                break;
            case 3:
                $name = Coachs::getName($this->user_id);
                break;
            case 4:
                $name = Customers::getName($this->user_id);
                break;
            default :
                $name = 'User Invalid';
                break;
        }
        return $name;
    }

    public function getTypeuser() {
        return $this->type_user;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey() {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey) {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password) {
        return $this->password === $this->encriptarPass($password);
    }

    function encriptarPass($pass) {
        $cryptKey = '40M1NUT3SW3B4pp';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $pass, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return( $qEncoded );
    }

    function desencriptarPass($pass) {
        $cryptKey = '40M1NUT3SW3B4pp';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($pass), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

    public function mail($layout, $content, $to, $subject) {
        $mail = \Yii::$app->mailer
                ->compose('layouts/' . $layout, ['content' => $content])
                ->setSubject($subject);
        if (\Yii::$app->params['mail']) {
            $mail->setTo($to);
            return $mail->send();
        } else {
            $mail->setTo(\Yii::$app->params['adminEmail']);
            if ($mail->send()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function get_fcontent($url, $javascript_loop = 0, $timeout = 5) {
        $url = str_replace("&amp;", "&", urldecode(trim($url)));

        //$cookie = tempnam(\Yii::$app->basePath."/tmp", "CURLCOOKIE");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
        curl_setopt($ch, CURLOPT_URL, $url);
        //curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_ENCODING, "");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);    # required for https urls
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
        $content = curl_exec($ch);
        $response = curl_getinfo($ch);
        curl_close($ch);

        if ($response['http_code'] == 301 || $response['http_code'] == 302) {
            ini_set("user_agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");

            if ($headers = get_headers($response['url'])) {
                foreach ($headers as $value) {
                    if (substr(strtolower($value), 0, 9) == "location:")
                        return get_url(trim(substr($value, 9, strlen($value))));
                }
            }
        }

        if (( preg_match("/>[[:space:]]+window\.location\.replace\('(.*)'\)/i", $content, $value) || preg_match("/>[[:space:]]+window\.location\=\"(.*)\"/i", $content, $value) ) && $javascript_loop < 5) {
            return get_url($value[1], $javascript_loop + 1);
        } else {
            return array($content, $response);
        }
    }

}
