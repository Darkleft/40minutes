<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => '40minutesweb-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
//    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
//        'errorHandler' => [
//            'errorAction' => 'site/error',
//        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => false,
            'messageConfig' => [
//                'from' => ['info@40minutes.com.co' => '40minutes'], // this is needed for sending emails
//                'charset' => 'UTF-8',
                'from' => ['no-reply@40minutes.com.co' => 'No Reply 40Minutes'], // this is needed for sending emails
                'charset' => 'UTF-8',
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'mail.40minutes.com.co',
                'username' => 'no-reply@40minutes.com.co',
                'password' => 'n0r3ply',
//             'host' => 'smtp.postmarkapp.com',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
//             'username' => 'd4f6c85b-5976-4e90-a35d-204c1045a8d4',
//             'password' => 'd4f6c85b-5976-4e90-a35d-204c1045a8d4',
                'port' => '26', // Port 25 is a very common port too
                'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '<orgname:\w+>/<orgid:\d+>/<controller:\w+>/<id:\d+>' => '<controller>/view/<id>',
                '<orgname:\w+>/<orgid:\d+>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
    ],
    'params' => $params,
    'modules' => [
        'gridview' => ['class' => 'kartik\grid\Module'],
        'users' => [
            'class' => 'app\modules\users\Module',
        ],
        'coachs' => [
            'class' => 'app\modules\coachs\Module',
        ],
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
        'admins' => [
            'class' => 'app\modules\admins\Module',
        ],
        'customers' => [
            'class' => 'app\modules\customers\Module',
        ],
        'services' => [
            'class' => 'app\modules\services\Module',
        ],
        'auth' => [
            'class' => 'app\modules\auth\Module',
        ],
        'clases' => [
            'class' => 'app\modules\clases\Module',
        ],
        'ratings' => [
            'class' => 'app\modules\ratings\Module',
        ],
        'plans' => [
            'class' => 'app\modules\plans\Module',
        ],
        'valorations' => [
            'class' => 'app\modules\valorations\Module',
        ],
        'groups' => [
            'class' => 'app\modules\groups\Module',
        ],
        'chats' => [
            'class' => 'app\modules\chats\Module',
        ],
    ],
        /*
          'controllerMap' => [
          'fixture' => [ // Fixture generation command line.
          'class' => 'yii\faker\FixtureController',
          ],
          ],
         */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
