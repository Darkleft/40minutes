<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = '40Minutes';
?>

<div class="site-index wrap text-center">
    <h1>Bienvenido <?= Yii::$app->user->identity->name?></h1>

    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/admins.png", ['style'=>'border-radius: 5px']), ['admin/site/index']) ?><br>
        <span><b>Administración</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/coachs.png", ['style'=>'border-radius: 5px']), ['coachs/coachs/index']) ?><br>
        <span><b>Entrenadores</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/customers.png", ['style'=>'border-radius: 5px']), ['customers/customers/index']) ?><br>
        <span><b>Gestión de Clientes</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/groups.png", ['style'=>'border-radius: 5px']), ['groups/groups/index']) ?><br>
        <span><b>Grupos</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/plans.png", ['style'=>'border-radius: 5px']), ['plans/plans/index']) ?><br>
        <span><b>Planes</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/class.png", ['style'=>'border-radius: 5px']), ['clases/clases/index']) ?><br>
        <span><b>Clases</b></span>
    </div><div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/valorations.png", ['style'=>'border-radius: 5px']), ['valorations/valorations/index']) ?><br>
        <span><b>Valoraciones</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/citas.png", ['style'=>'border-radius: 5px']), ['admin/notificaciones/create']) ?><br>
        <span><b>Notificaciones</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/califications.png", ['style'=>'border-radius: 5px']), ['ratings/ratings/index']) ?><br>
        <span><b>Calificaciones</b></span>
    </div><div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/ganancias.png", ['style'=>'border-radius: 5px'])) ?><br>
        <span><b>Reporte de Ganancias</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/pagos.png", ['style'=>'border-radius: 5px'])) ?><br>
        <span><b>Pago a Entrenadores</b></span>
    </div>
    <div class="col-lg-2 col-xs-6" style="padding-top: 2%">
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/btns/chats.png", ['style'=>'border-radius: 5px']), ['chats/chats/index']) ?><br>
        <span><b>Gestión de Chats</b></span>
    </div>
    

    
</div>
