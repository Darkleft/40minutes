<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
?>
<div class="container">
    <div class="site-login row mb-20 mt-20">
        <div class="col-md-6 col-md-offset-3">
            <?= Html::img(Yii::$app->request->baseUrl . "/images/titles/logo_big.png", ['class'=>'img-responsive']) ?>
        </div>
        <div class="col-md-6 col-md-offset-3 mt-20">
            <?php
            $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-offset-4 col-lg-6\">{error}</div>",
                            'labelOptions' => ['class' => 'col-lg-4 control-label'],
                        ],
            ]);
            ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?=
            $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"col-lg-offset-4 col-lg-3\">{input} Recordarme</div>\n<div class=\"col-lg-8\">{error}</div>",
            ])
            ?>

            <div class="form-group mb-20">
                <div class="col-lg-offset-4 col-lg-6 mb-20">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary mb-20', 'name' => 'login-button']) ?>
                </div>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
    </div>
</div>