<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Registro éxitoso';
?>
<div class="site-about" style="text-align: center">
    <h1><?= Html::encode($this->title) ?></h1>

    <div >
        <?= Html::a(Html::img(Yii::$app->request->baseUrl . "/images/titles/logo_big.png")) ?>
    </div>

    <code>Desde hoy empiezas un nuevo ciclo con 40 Minutes, aprovechalo al máximo.</code>
</div>
