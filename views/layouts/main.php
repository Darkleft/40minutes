<?php
/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;
use yii\bootstrap\Modal;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" style="display: none;">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<?= Yii::$app->request->baseUrl; ?>/css/site.css" rel="stylesheet">
        <link href="<?= Yii::$app->request->baseUrl; ?>/images/icons/logo.png" type="image/x-icon" rel="shortcut icon" />

        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/bower_components/font-awesome/css/font-awesome.min.css">
        <!--Ionicons--> 
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/bower_components/Ionicons/css/ionicons.min.css">
        <!--Theme style--> 
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/dist/css/AdminLTE.min.css">
        <!--           AdminLTE Skins. Choose a skin from the css/skins
                       folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/dist/css/skins/_all-skins.min.css">
        <!--           Morris chart -->
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/bower_components/morris.js/morris.css">
        <!--jvectormap--> 
        <link rel="stylesheet" href="<?= Yii::$app->request->baseUrl; ?>/bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <!--<link rel="stylesheet" href="<?php Yii::$app->request->baseUrl; ?>/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">-->
        <!-- Daterange picker -->
        <!--<link rel="stylesheet" href="<?php Yii::$app->request->baseUrl; ?>/bower_components/bootstrap-daterangepicker/daterangepicker.css">-->


        <script src="<?= Yii::$app->request->baseUrl; ?>/js/jquery-1.11.1.js" type="text/javascript"></script>
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <?php $this->beginBody() ?>
        <div class="wrap">
            <?php if (!Yii::$app->user->isGuest) { ?>
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="main-sidebar">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- sidebar menu: : style can be found in sidebar.less -->
                        <ul class="sidebar-menu" data-widget="tree">
                            <li class="header text-center">Menú</li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/admin/site/index">
                                    <i class="fa fa-dashboard"></i> <span>Administración</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/coachs/coachs/index">
                                    <i class="fa fa-user"></i> <span>Entrenadores</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/customers/customers/index">
                                    <i class="fa fa-user"></i> <span>Clientes</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/groups/groups/index">
                                    <i class="fa fa-users"></i> <span>Grupos</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/plans/plans/index">
                                    <i class="fa fa-calendar"></i> <span>Planes</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/clases/clases/index">
                                    <i class="fa fa-calendar"></i> <span>Clases</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/valorations/valorations/index">
                                    <i class="fa fa-heart"></i> <span>Valoraciones</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/admin/notificaciones/create">
                                    <i class="fa fa-mail-reply"></i> <span>Notificaciones</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/ratings/ratings/index">
                                    <i class="fa fa-star"></i> <span>Calificaciones</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>">
                                    <i class="fa fa-credit-card"></i> <span>Ganancias</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/">
                                    <i class="fa fa-money"></i> <span>Pagos</span>
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= Yii::$app->request->baseUrl; ?>/chats/chats/index">
                                    <i class="fa fa-comments"></i> <span>Chats</span>
                                </a>
                            </li>
                        </ul>
                    </section>
                    <!-- /.sidebar -->
                </aside>
            <?php } ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="<?php if (!Yii::$app->user->isGuest) {
                echo 'content-wrapper';
            } else {
                echo 'container-fluid';
            } ?>">
                <!-- Content Header (Page header) -->
                <div class="container-fluid">

                    <?php
                    if (!Yii::$app->user->isGuest) {
                        NavBar::begin([
                            'brandLabel' => '<img style="display:inline;margin-right:5px;background:#FFF" src="' . Yii::$app->request->baseUrl . '/images/icons/logo.png">' . '40Minutes',
                            'brandUrl' => Yii::$app->homeUrl,
                            'options' => [
                                'class' => 'navbar-inverse navbar-fixed-top',
                                'style' => 'background-color: #1e282c;border-color: #1e282c;'
                            ],
                        ]);
                        echo Nav::widget([
                            'options' => ['class' => 'navbar-nav navbar-right'],
                            'encodeLabels' => false,
                            'items' => [
                                ['label' => 'Inicio', 'url' => ['/site/index']],
                                [
                                    'label' => 'Módulos',
                                    'items' => [
                                        ['label' => 'Administración', 'url' => ['/admin/site/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Entrenadores', 'url' => ['/coachs/coachs/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Gestión de Clientes', 'url' => ['/customers/customers/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Grupos', 'url' => ['/groups/groups/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Planes', 'url' => ['/plans/plans/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Clases', 'url' => ['/clases/clases/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Valoraciones', 'url' => ['/valorations/valorations/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Calificaciones', 'url' => ['/ratings/ratings/index'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Reporte de Ganancias', 'url' => ['site/prensa', 'view' => 'fotos'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Pago a Entrenadores', 'url' => ['site/prensa', 'view' => 'videos'], 'options' => ['onclick' => 'loading()']],
                                        ['label' => 'Chats', 'url' => ['/chats/chats/index'], 'options' => ['onclick' => 'loading()']],
                                    //['label' => 'Gruos', 'url' => ['site/prensa', 'view' => 'audios'], 'options' => ['onclick' => 'loading()']],
                                    ],
                                ],
                                Yii::$app->user->isGuest ? (
                                        ['label' => 'Login', 'url' => ['/site/login']]
                                        ) : (
                                        '<li>'
                                        . Html::beginForm(['/site/logout'], 'post')
                                        . Html::submitButton(
                                                'Cerrar Sesión (' . Yii::$app->user->identity->name . ')', ['class' => 'btn btn-link logout']
                                        )
                                        . Html::endForm()
                                        . '</li>'
                                        )
                            ],
                        ]);
                        NavBar::end();
                    }
                    ?>

                    <div class="col-md-12" style="margin-top: 70px;">
<?= Alert::widget() ?>
            <?= $content ?>
                    </div>
                </div>

            </div>
<?php $this->endBody() ?>
    </body>
    <footer class="footer text-center" style="padding: 10px 0px; margin-top: 10px;">
        <p>
            &copy; 40Minutes <?= date('Y') ?>
            <br>Todos los derechos reservados.<br>
        </p>
        <div class="text-center"><span>Desarrollado por <a target="_blank" href="https://www.kloustr.com/"><img width="70" src="https://www.kloustr.com/wp-content/themes/kloustr/dist/images/logo-white.png"></a></span></div>
    </footer>

    <script src="<?= Yii::$app->request->baseUrl; ?>/js/site.js" type="text/javascript"></script>

<!--    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/jquery-ui/jquery-ui.min.js"></script>-->
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>


    <!-- Sparkline -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?= Yii::$app->request->baseUrl; ?>/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/moment/min/moment.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?= Yii::$app->request->baseUrl; ?>/dist/js/pages/dashboard.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.6/dist/loadingoverlay.min.js
    "></script>

    <script>
        $.LoadingOverlay("show", {
            background: "rgba(255, 255, 255, 0.99)",
            image: "",
            fontawesome: "fa fa-refresh fa-spin"
        });
        setTimeout(function () {
            $('html').fadeIn('slow');
        }, 1000);
        setTimeout(function () {
            $.LoadingOverlay("hide");
        }, 1500);
    </script>
    <?php
//Mensajes
    Modal::begin([
        'id' => 'modalmensajes',
        'closeButton' => ['id' => 'closebutton'],
        'header' => '<h1  id="modalheader">Mensaje</h1>',
        'headerOptions' => [
            'style' => "text-align: center"
        ],
        'clientOptions' => [
            'backdrop' => 'static', 'keyboard' => false],
    ]);
    ?>
    <div id="modalmensajebody"></div>
    <br>
    <div style="text-align: center" id="btn-ok-modal">
    <?= Html::button('OK', ['class' => 'btn btn-success', 'onclick' => '$("#modalmensajes").modal("hide")']) ?>
    </div>
<?php
Modal::end();
?>
</html>
<?php $this->endPage() ?>
